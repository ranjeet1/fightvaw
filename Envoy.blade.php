@servers(['staging' => 'fightvaw@custom.fightvaw.org', 'production-tpo' => 'username@tpo.fightvaw.org', 'production-lacc' => 'username@lacc.fightvaw.org','production-cwin' => 'username@cwin.fightvaw.org','production-saathi' => 'username@saathi.fightvaw.org'])

<!--Deploy Production-->
@task('deploy-production', ['on' => 'production-tpo'])
	cd public_html
        git init
        git remote add origin git@gitlab.yipl.com.np:web-apps/fightvaw_custom.git
        git pull origin master
        cp /path/to/config/file .env 
        composer install
        php artisan migrate
@endtask

<!--Deploy Staging-->
@task('deploy-staging', ['on' => 'staging'])
	cd subdomains/custom/
        mkdir {{ $org }}
        cd {{ $org }}
        git init
        git remote add origin git@gitlab.yipl.com.np:web-apps/fightvaw_custom.git
        git pull origin master
        cp /path/to/config/file .env
        composer install
        php artisan migrate
@endtask

<!--TPO-->
@task('deploy-production-tpo', ['on' => 'production-tpo'])
        cd public_html
	git pull origin master
	php artisan migrate
@endtask

@task('deploy-staging-tpo', ['on' => 'staging'])
    cd subdomains/custom/tpo/
    git pull origin master
    php artisan migrate
@endtask

<!--Lacc-->
@task('deploy-production-lacc', ['on' => 'production-lacc'])
        cd public_html
	git pull origin master
	php artisan migrate
@endtask

@task('deploy-staging-lacc', ['on' => 'staging'])
	cd subdomains/custom/lacc/
	git pull origin master
	php artisan migrate
@endtask

<!--CWIN-->
@task('deploy-production-cwin', ['on' => 'production-cwin'])
        cd public_html
	git pull origin master
	php artisan migrate
@endtask

@task('deploy-staging-cwin', ['on' => 'staging'])
	cd subdomains/custom/cwin/
	git pull origin master
	php artisan migrate
@endtask

<!--SAATHI-->
@task('deploy-production-saathi', ['on' => 'production-saathi'])
        cd public_html
	git pull origin master
	php artisan migrate
@endtask

@task('deploy-staging-saathi', ['on' => 'staging'])
	cd subdomains/custom/saathi/
	git pull origin master
	php artisan migrate
@endtask


