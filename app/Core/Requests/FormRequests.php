<?php

namespace App\Core\Requests;

use App\Core\Elements\ElementHandler;

class FormRequests
{
    protected $elementHandler;

    public function __construct($type)
    {
        $this->elementHandler = new ElementHandler($type);
    }

    public function getRules()
    {
        $elementsArray = $this->elementHandler->getElements();
        $this->elementHandler->filter($elementsArray, null, 'rules');
        $rules         = $this->elementHandler->extractData;

        return $rules;
    }

    public function getAttributesLabel()
    {
        $elementsArray   = $this->elementHandler->getElements();
        $this->elementHandler->filter($elementsArray, null, 'label');
        $attributesLabel = $this->elementHandler->extractData;

        return $attributesLabel;
    }
}
