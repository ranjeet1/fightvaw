<?php

namespace App\Core\Elements;

use App;

class ElementHandler
{
    public $extractData = [];
    protected $type;
    protected $parentInfo;

    public function __construct($type)
    {
        $this->organization     = App::make('App\Core\Organization\Organization');
        $this->organizationName = $this->organization->getName();
        $this->type             = $type;
    }

    public function findKey($data, $keySearch)
    {
        foreach ($data as $key => $item) {
            if ($key == $keySearch) {
                return $item;
            } else {
                if (isset($item[$keySearch])) {
                    return $item[$keySearch];
                } elseif (in_array($keySearch, $item)) {
                    $this->findKey($item, $keySearch);
                }
            }
        }
        throw new \Exception("$keySearch does not exist");
    }

    public function getKeyElements($data, $key)
    {
        $elements = $this->findKey($data, $key);
        if (!$this->parentInfo) {
            if (isset($elements['parent'])) {
                unset($elements['parent']);
            }
        }

        return $elements;
    }

    /**
     * $indexArray is array path
     * if $indexArray exist return value
     * else return whole array
     * @param  array $indexArray
     * @return array $elements
     */
    public function getElementsArray($indexArray)
    {
        $className   = $this->getElementClassName();
        $elementsObj = App::make($className);
        $elements    = $elementsObj->elements();
        if (count(array_filter($indexArray))) {
            foreach ($indexArray as $index) {
                if (str_contains($index, 'other-')) {
                    $elements = $elements['other-1'];
                } else {
                    $elements = $elements[$index];
                }
            }
        }

        return $elements;
    }

    public function getElements($key = false, $parentInfo = false)
    {
        $this->parentInfo = $parentInfo;
        $className        = $this->getElementClassName();
        $elementsObj      = App::make($className);
        $elements         = $elementsObj->elements();
        if ($key) {
            return $this->getKeyElements($elements, $key);
        }

        return $elementsObj->elements();
    }

    /**
     * Extract required data based on given index
     * @param array  $elementsArray
     * @param string $parentName
     * @param string $index
     */
    public function filter($elementsArray, $parentName, $index)
    {
        foreach ($elementsArray as $elementName => $element) {
            $keyName = $elementName;
            if ($parentName) {
                $keyName = $parentName.".".$elementName;
            }
            if (isset($element['input']['options'][$index])) {
                $this->extractData[$keyName] = $element['input']['options'][$index];
            } elseif (isset($element['parent'])) {
                unset($element['parent']);
                $this->filter($element, $keyName, $index);
            }
        }
    }

    /**
     * Generate classname as required
     * @return string $className
     */
    private function getElementClassName()
    {
        $className         = "App\Fightvaw\\$this->organizationName\Elements";
        $followUpClassName = "App\Elements\Services\FollowUp";
        if ($this->type == 'Cases') {
            $className .= '\Cases\Cases';
        } else {
            $className .= "\Services\\".ucfirst($this->type);
            if (!class_exists($className)) {
                return $followUpClassName;
            }
        }

        return $className;
    }

    public function getFilesElement()
    {
        $fileElementObj = App::make('App\Elements\Services\File');
        $fileElements   = $fileElementObj->elements();

        return $fileElements;
    }
}
