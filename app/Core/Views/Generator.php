<?php

namespace App\Core\Views;

class Generator
{

    protected function isDisplayTitleEnabled($options)
    {
        $boolean                 = $options['displayTitle'] = (isset($options['displayTitle'])
                    ? $options['displayTitle'] : true);

        return $boolean;
    }

    protected function setSubTitle($options)
    {
        if ($this->isDisplayTitleEnabled($options)) {
            $this->html .= '<div class="sub-title">'.$options['label'].'</div>';
        }
    }

    protected function setSubInnerTitle($options)
    {
        if ($this->isDisplayTitleEnabled($options) && $options['label'] != " ") {
            $this->html .= '<div class="sub-inner-title">'.$options['label'].'</div>';
        }
    }

    public function subSection($object, $elements)
    {
        $this->html .= '<div class="sub-inner-section">';
        $this->getData($object, $elements);
        $this->html .= '</div>';
    }

    public function getData($object, $elements)
    {
        if (isset($elements['parent'])) {
            $this->setSubInnerTitle($elements['parent']['options']);
            unset($elements['parent']);
        }
        foreach ($elements as $elementName => $element) {
            if (!isset($element['parent']) && isset($element['input'])) {
                $options = $element['input']['options'];
                $this->html .= '<li><label>'.$options['label'].'</label><span>';
                if (isset($object[$elementName])) {
                    $this->getInputValue($object[$elementName], $options);
                }
                $this->html .= '</span></li>';
            } else {
                if (!isset($element['parent'])) {
                    $element = $elements;
                }
                if (isset($object[$elementName])) {
                    $this->subSection($object[$elementName], $element);
                }
            }
        }
    }

    public function getInputValue($element, $options)
    {
        if (isset($options['otherOptions']) && $options['otherOptions']) {
            if (isset($element['Others']) && !$element) {
                $this->html .= $element['Others'];
            } else {
                $this->getInputValueBasedOnType($element, $options);
            }
        } else {
            $this->getInputValueBasedOnType($element, $options);
        }
    }

    public function getInputValueBasedOnType($element, $options)
    {
        if (is_array($element) || isset($options['multiple'])) {
            if(is_array($element))
                $this->html .= implode(", ", $element);
            else
                $this->html .= $element;
        } else {
            $this->html .= $element;
        }
    }
}
