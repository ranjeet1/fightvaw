<?php

namespace App\Core\Views;

use App\Core\Elements\ElementHandler;

class IntakeGenerator extends Generator
{
    protected $html;

    public function generate($case, $type, $key)
    {
        $caseInfo       = json_decode($case->$key, true);
        $elementhandler = new ElementHandler($type);
        $elements       = $elementhandler->getElements($key, true);
        if ($key=='contact')
        {
            if (isset($caseInfo['childContact']))
            {
                if($caseInfo['childContact']['PermanentAddress']['country']=="Nepal")
                {
                    unset($caseInfo['childContact']['PermanentAddress']['otherAddress']);
                }
                else
                    unset($caseInfo['childContact']['PermanentAddress']['nepalAddress']);
            }
            if (isset($caseInfo['PermanentAddress']))
            {
                if($caseInfo['PermanentAddress']['country']=="Nepal")
                {
                    unset($caseInfo['PermanentAddress']['otherAddress']);
                }
                else
                    unset($caseInfo['PermanentAddress']['nepalAddress']);
            
            }
        }
            
        
        
        $this->html     = '';
        $this->view($caseInfo, $elements);

        return $this->html;
    }

    public function View($caseInfo, $elements)
    {
        if (isset($elements['parent'])) {
            $this->setSubTitle($elements['parent']['options']);
            unset($elements['parent']);
        }
        $this->html .= '<ul>';
        $this->getData($caseInfo, $elements);
        $this->html .= '</ul>';
    }

    public function subSection($caseInfo, $elements)
    {
        $this->html .= '<div class="sub-inner-section">';
        $this->getData($caseInfo, $elements);
        $this->html .= '</div>';
    }
}
