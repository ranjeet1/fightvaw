<?php

namespace App\Core\Views;

use App\Core\Elements\ElementHandler;

class ServiceGenerator extends Generator
{
    protected $html;

    public function generate($services, $type)
    {
        $caseInfo       = json_decode($services->details, true);
        $elementHandler = new ElementHandler($type);
        $elements       = $elementHandler->getElements('details', true);
        $this->html     = '';
        $this->view($caseInfo, $elements);

        return $this->html;
    }

    public function View($caseInfo, $elements)
    {
        if (isset($elements['parent'])) {
            $this->setSubTitle($elements['parent']['options']);
            unset($elements['parent']);
        }
        $this->html .= '<ul class="service-list">';
        $this->getData($caseInfo, $elements);
        $this->html .= '</ul>';
    }

    public function getValue($object)
    {
        $otherArray = [];
        $html       = '';
        if (isset($object)) {
            foreach ((array) $object as $key => $value) {
                if (strpos($key, "other-") !== false) {
                    $otherArray[] = $key;
                }
            }
        }
        if (count($otherArray)) {
            foreach ($otherArray as $other) {
                if ($object->$other->value && $object->$other->label) {
                    $label = $object->$other->label;
                    $value = $object->$other->value;
                    $html .= '<li><label for="">'.$label.'</label><span>('.$value.')</span></li>';
                }
            }
        }

        return $html;
    }
}
