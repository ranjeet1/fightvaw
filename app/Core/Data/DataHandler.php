<?php

namespace App\Core\Data;

use App\Core\Organization\Organization;
use App;

class DataHandler extends Collection
{

    public function __construct(Organization $organizationObj)
    {
        $this->organizationObj = $organizationObj;
        $organizationName      = $this->organizationObj->getName();
        $className             = "App\Fightvaw\\$organizationName\\Data\Collection";
        $this->orgCollection   = App::make($className);
    }

    public function getData($name)
    {
        $data = $this->orgCollection->getData($name);
        if ($data) {
            return $this->mergeArrayValueToKey($data);
        } else {
            return $this->getDefaultData($name);
        }
    }

    public function getDefaultData($name)
    {
        $data = [];
        switch ($name) {
            case 'district':
                $data = $this->district();
                break;
            case 'ethnicity':
                $data = $this->ethinicity();
                break;
            case 'age':
                $data = $this->age();
                break;
            case 'typeOfViolence':
                $data = $this->typeOfViolence();
                break;
            case 'country':
                $data = $this->country();
                break;
            default:
                break;
        }

        return $this->mergeArrayValueToKey($data);
    }

    public function mergeArrayValueToKey($data)
    {
        $options = array_combine($data, $data);
        
        //asort($options);

        return $options;
    }
}
