<?php

namespace App\Core\Form;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\App;

class FormBuilder
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var FormHelper
     */
    private $formHelper;

    public function __construct()
    {
        $this->container  = App::make('Illuminate\Contracts\Container\Container');
        $this->formHelper = App::make('App\Core\Form\FormHelper');
    }

    /**
     * @param       $formClass
     * @param       $options
     * @param       $data
     * @return Form
     */
    public function create($formClass, array $options = [], array $data = [])
    {
        $form = $this->container
            ->make($formClass)
            ->setFormHelper($this->formHelper)
            ->setFormOptions($options)
            ->addData($data);

        $form->buildForm();

        return $form;
    }

    /**
     * Get instance of the empty form which can be modified
     *
     * @param  array $options
     * @param  array $data
     * @return Form
     */
    public function plain(array $options = [], array $data = [])
    {
        return $this->container
                ->make('Kris\LaravelFormBuilder\Form')
                ->setFormHelper($this->formHelper)
                ->setFormOptions($options)
                ->addData($data);
    }
}
