<?php

namespace App\Core\Form;

use App\Core\Elements\ElementHandler;

class BaseForm extends Form
{
    protected $inputName;
    protected $fieldsArray;
    protected $parent;
    protected $fieldName;

    /**
     * Get array of elements
     * @param  object $organizationObj
     * @param  string $parent
     * @return array  $elements
     */
    private function getTypeFields($organizationObj)
    {
        $type                 = $organizationObj->getSection();
        $elementHandler       = new ElementHandler($type);
        $pathToElementInArray = $this->formHelper->getFieldsNameInArray($this->parent);
        $elements             = $elementHandler->getElementsArray($pathToElementInArray);
        if ($type != 'Cases' && !$this->parent) {
            $fileElements = $elementHandler->getFilesElement();
            $elements     = array_merge($elements, $fileElements);
        }

        return $elements;
    }

    /**
     * Add other fields
     * @param array  $field
     * @param array  $fieldName
     * @param object $model
     */
    private function addOtherFields($field, $model)
    {
        $fieldType           = $field['parent']['fieldType'];
        $options             = $field['parent']['options'];
        $this->fieldsArray[] = [$this->fieldName, $fieldType, $options];
        if (isset($options['multiple_field'])) {
            $count = $this->getOtherFields($model);
            if ($count) {
                $i = 2;
                for ($i; $i <= $count; $i += 1) {
                    $options['wrapper']  = str_replace('other-form', '',
                        $options['wrapper']);
                    $this->fieldsArray[] = [$this->parent."[other-$i]",
                        $fieldType, $options, ];
                }
            }
        }
    }

    /**
     * Get the value of particular parent or field
     * @param  type  $model
     * @param  type  $elementName
     * @return array or string
     */
    private function getValue($model, $elementName)
    {
        $elements = $this->formHelper->getFieldsNameInArray($elementName);
        if ($model->exists) {
            foreach ($elements as $element) {
                if (isset($model->$element)) {
                    $model = $model->$element;
                }
            }
        }

        return $model;
    }

    /**
     * Get count for other fields
     * @return int $count
     */
    private function getOtherFields($model)
    {
        $children = $this->getValue($model, $this->parent);
        $count    = 0;
        foreach ((array) $children as $key => $value) {
            if (str_contains($key, "other-") !== false) {
                $count += 1;
            }
        }

        return $count;
    }

    /**
     * Generate text input for Others to specify, if Others is selected
     * @param string $inputType
     * @param array  $options
     * @param object $model
     */
    private function addOthersOption($inputType, $options, $model)
    {
        $value = $this->getValue($model, $this->fieldName);
        if (isset($value->Others)) {
            $options['selected'] = 'Others';
            $this->fieldsArray[] = [$this->fieldName, $inputType, $options];
            $this->fieldsArray[] = [$this->fieldName.'[Others]', 'text', [
                    'label_attr' => ['class' => 'hide-label'],
                    'attr' => ['placeholder' => 'Others(Specify)'],
            ]];
        } else {
            $this->fieldsArray[] = [$this->fieldName, $inputType, $options];
        }
    }

    /**
     * Generate array of input fields
     * @param array $fields
     */
    private function generateFields($fields, $model)
    {
        if (array_key_exists('parent', $fields)) {
            unset($fields['parent']);
        }
        foreach ($fields as $key => $field) {
            $this->fieldName = $this->getInputName($key);
            if (isset($field['input'])) {
                $inputType = $field['input']['fieldType'];
                $options   = isset($field['input']['options']) ? ($field['input']['options'])
                        : [];
                if (isset($options['otherOptions']) && $options['otherOptions']) {
                    $this->addOthersOption($inputType, $options, $model);
                } else {
                    $this->fieldsArray[] = [$this->fieldName, $inputType, $options];
                }
            } else {
                if (isset($field['parent'])) {
                    $this->addOtherFields($field, $model);
                } else {
                    $this->inputName .= '['.$key.']';
                    $this->generateFields($field);
                }
            }
        }
    }

    /**
     * Generate field name
     * @param  string $name
     * @return string
     */
    private function getInputName($name)
    {
        if ($this->parent) {
            $name = $this->parent.'['.$name.']';
        }

        return $name;
    }

    /**
     * Build up form
     */
    public function buildForm()
    {
        $organizationObj = $this->getData('organizationObj');
        $this->parent    = $this->getData('parent');
        $elements        = $this->getTypeFields($organizationObj);
        $this->generateFields($elements, $this->model);
        foreach ($this->fieldsArray as $field) {
            $this->add($field[0], $field[1], $field[2]);
        }
        if (!$this->parent) {
            $this->add('Save', 'submit',
                    [
                    'attr' => ['class' => 'submit-button submit-dash'],
                ])
                ->add('Cancel', 'button',
                    [
                    'attr' => ['class' => 'submit-button submit-dash', 'onclick' => "redirectFunction('".route('cases.show',
                            $this->getData('caseId'))."')"],
            ]);
        }
    }
}
