<?php

namespace App\Core\Form\Fields;

use App\Core\Form\Form;
use App\Core\Organization\Organization;

abstract class ParentType extends FormField
{
    /**
     * @var mixed
     */
    protected $children;

    abstract protected function createChildren();

    public function __construct($name, $type, Form $parent, array $options = [],
                                Organization $organizationObj)
    {
        parent::__construct($name, $type, $parent, $options, $organizationObj);
        $this->createChildren();
    }

    public function render(array $options = [], $showLabel = true,
                           $showField = true, $showError = true)
    {
        $options['children'] = $this->children;

        return parent::render($options, $showLabel, $showField, $showError);
    }

    /**
     * Get all children of the choice field
     *
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get a child of the choice field
     *
     * @return mixed
     */
    public function getChild($key)
    {
        return array_get($this->children, $key);
    }

    public function isRendered()
    {
        foreach ($this->children as $key => $child) {
            if ($child->isRendered()) {
                return true;
            }
        }

        return parent::isRendered();
    }
}
