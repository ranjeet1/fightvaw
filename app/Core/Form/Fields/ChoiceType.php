<?php

namespace App\Core\Form\Fields;

class ChoiceType extends ParentType
{
    /**
     * @var string
     */
    protected $choiceType = 'select';

    protected function getTemplate()
    {
        return 'choice';
    }

    /**
     * Determine which choice type to use
     *
     * @return string
     */
    protected function determineChoiceField()
    {
        $expanded = $this->options['expanded'];
        $multiple = $this->options['multiple'];

        if ($multiple) {
            $this->options['attr']['multiple'] = true;
        }

        if ($expanded && !$multiple) {
            return $this->choiceType = 'radio';
        }

        if ($expanded && $multiple) {
            return $this->choiceType = 'checkbox';
        }
    }

    protected function getDefaults()
    {
        return [
            'choices' => $this->getDataForChoices(),
            'selected' => $this->getValue(),
            'expanded' => false,
            'multiple' => false,
        ];
    }

    /**
     * Create children depending on choice type
     */
    protected function createChildren()
    {
        $this->determineChoiceField();

        $fieldMultiple = $this->options['multiple'] ? '[]' : '';
        $fieldType     = $this->formHelper->getFieldType($this->choiceType);
        switch ($this->choiceType) {
            case 'radio':
                $this->setRadioProperty();
            case 'checkbox':
                $this->buildCheckableChildren($fieldType, $fieldMultiple);
                break;
            default:
                $this->buildSelect($fieldType, $fieldMultiple);
                break;
        }
    }

    /**
     * Build checkable children fields from choice type
     *
     * @param string $fieldType
     * @param string $fieldMultiple
     */
    protected function buildCheckableChildren($fieldType, $fieldMultiple)
    {
        foreach ((array) $this->options['choices'] as $key => $choice) {
            $inputId               = $choice.'_'.$key;
            $this->children[] = new $fieldType(
                $this->name.$fieldMultiple, $this->choiceType, $this->parent,
                [
                'attr' => ['id' => $inputId],
                'label_attr' => ['for' => $inputId],
                'label' => $choice,
                'is_child' => true,
                'checked' => in_array($key, (array) $this->options['selected']),
                'default_value' => $key,
                ], $this->organizationObj
            );
        }
    }

    /**
     * Build select field from choice
     *
     * @param string $fieldType
     * @param string $fieldMultiple Append [] if multiple choice
     */
    protected function buildSelect($fieldType, $fieldMultiple)
    {
        $label = $this->getLabel();
        if (!isset($this->options['empty_value'])) {
            $this->options['empty_value'] = "Select $label";
        }

        if ($this->options['multiple']) {
            $this->setMultiSelectProperty();
        }
        
        $this->options['selected'] = array_filter((array)$this->options['selected']);

        $this->children[] = new $fieldType(
            $this->name.$fieldMultiple, $this->choiceType, $this->parent,
            $this->formHelper->mergeOptions($this->options, ['is_child' => true]),
            $this->organizationObj
        );
    }

    /**
     * Set properties for radio input
     */
    private function setRadioProperty()
    {
        $this->options['wrapper']['class'] .= ' input-radio';
    }

    /**
     * Set properties for multi-select input
     */
    private function setMultiSelectProperty()
    {
        $inputName                         = $this->getLabel();
        $this->options['attr']['multiple'] = true;
        $this->options['attr']['class']    = 'input-name multiple-input';
        if (!isset($this->options['attr']['placeholder'])) {
            $this->options['attr']['placeholder'] = "Select $inputName";
        }
    }

    /**
     * Fetch value of required field
     * @return type
     */
    private function getValue()
    {
        $elements = $this->formHelper->getFieldsNameInArray($this->name);
        $object   = $this->parent->getModel();
        if ($object->exists) {
            foreach ($elements as $element) {
                if (isset($object->$element)) {
                    $object = $object->$element;
                }
            }

            return $object;
        }
    }
}
