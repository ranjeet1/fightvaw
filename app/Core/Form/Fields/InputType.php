<?php

namespace App\Core\Form\Fields;

class InputType extends FormField
{

    protected function getTemplate()
    {
        return 'text';
    }
}
