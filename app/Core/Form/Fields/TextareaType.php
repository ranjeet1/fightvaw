<?php

namespace App\Core\Form\Fields;

class TextareaType extends FormField
{

    protected function getTemplate()
    {
        return 'textarea';
    }
}
