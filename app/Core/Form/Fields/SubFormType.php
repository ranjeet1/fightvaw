<?php

namespace App\Core\Form\Fields;

use App\Core\Form\Form;
use App\Core\Form\FormBuilder;

class SubFormType extends ParentType
{

    protected function getTemplate()
    {
        return 'sub_form';
    }

    protected function getDefaults()
    {
        return [
            'is_child' => true,
            'displayTitle' => true,
        ];
    }

    protected function createChildren()
    {
        $formPath       = $this->organizationObj->getFormPath();
        $formBuilder    = new FormBuilder();
        $inputName      = $this->getInputName($this->name);
        $form           = $formBuilder->create($formPath,
            [
            'model' => $this->parent->getModel(),
            'data' => ['organizationObj' => $this->organizationObj, 'element' => $inputName,
                'parent' => $this->name, ],
        ]);
        $this->children = preg_replace(["/<form[^>]+\>/i", "/<\/form>/"], "",
            form($form));
    }

    /**
     * @return Form
     * @throws \Exception
     */
    protected function getClassFromOptions()
    {
        $class = array_get($this->options, 'class');

        if ($class && $class instanceof Form) {
            return $class;
        }

        throw new \Exception('Please provide instance of Form class.');
    }
}
