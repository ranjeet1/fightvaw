<?php

namespace App\Core\Organization;

use App;

class Organization extends OrganizationDetails
{
    public $isFollowupEnable;

    public function __construct()
    {
        $organizationName = env('ORGANIZATION_NAME');
        $orgObject        = App::make("App\Fightvaw\\$organizationName\Organization\Organization");
        $this->bindOrgDetails($orgObject);
    }

    private function bindOrgDetails($orgObject)
    {
        $this->setName(env('ORGANIZATION_NAME'));
        $this->setRegistrationNumber($orgObject->registrationNumber());
        $this->setServices($orgObject->services());
        $this->setJsonCaseFields($orgObject->JsonCaseFields());
        $searchFields = $this->search($orgObject);
        $this->setSearchFields($searchFields);
        $addSearchFields = $this->addSearch($orgObject);
        $this->setAddSearchFields($addSearchFields);
        $this->isFollowupEnable = $orgObject->followup();
    }

    private function searchFields()
    {
        return [
            'clientName' => [
                'label' => 'Client Name',
                'type' => 'text',
                'field_type' => 'text',
                'attrs' => [
                ],
                'type' => 'json',
                'field' => 'profile',
                'path' => '{name}',
            ],
            'registrationNumber' => [
                'label' => 'Registration Number',
                'type' => 'text',
                'field_type' => 'text',
                'field' => 'reg_no',
                'attrs' => [
                ],
            ],
            'district' => [
                'label' => 'District',
                'field_type' => 'select',
                'select_options' => [
                    'choices' => [
                    ],
                ],
                'attrs' => [
                    'class' => 'multiple-input',
                    'multiple' => true,
                    'placeholder' => 'Select District',
                ],
                'type' => 'json',
                'field' => ['contact','incident'],
                'path' => ['{TemporaryAddress,district}','{PermanentAddress,nepalAddress,district}','{district}'] ,
            ],
        ];
    }
    private function addSearchFields()
    {
        return [
            'description' => [
                'type' => 'json',
                'operator' => 'like',
                'field' => 'incident',
                'path' => '{description}',
            ],
        ];
    }

    private function addSearch($orgObject)
    {
        $addSearchFields = $this->addSearchFields();
        if (method_exists($orgObject, 'addSearchFields')) {
            $fields = $orgObject->addSearchFields();
            $addSearchFields = array_merge($addSearchFields, $fields);
        }

        return $addSearchFields;
    }

    private function search($orgObject)
    {
        $searchFields = $this->searchFields();
        if (method_exists($orgObject, 'searchFields')) {
            $fields = $orgObject->searchFields();
            $searchFields = array_merge($searchFields, $fields);
        }

        return $searchFields;
    }

    public function getFormPath()
    {
        $organizationName = $this->getName();
        $type             = $this->getSection();
        $baseFormPath     = 'App\Core\Form\BaseForm';
        $formPath         = "App\Fightvaw\\$organizationName\\Forms";
        if ($type == 'Cases') {
            return $baseFormPath;
        } else {
            $formPath .= "\Services\\".ucfirst($type);
            if (class_exists($formPath)) {
                return $formPath;
            }
        }

        return $baseFormPath;
    }

    public function getServiceTitle()
    {
        $serviceType = $this->getSection();
        $serviceList = $this->getServices();
        if (isset($serviceList[$serviceType]['label'])) {
            return $serviceList[$serviceType]['label'];
        }
    }
}
