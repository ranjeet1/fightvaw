<?php

namespace App\Core\Organization;

class OrganizationDetails
{
    protected $name;
    protected $registrationNumber;
    protected $services = [];
    protected $section;
    protected $jsonCaseFields = [];
    protected $searchFields = [];
    protected $addSearchFields = [];

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    public function setServices($services)
    {
        $this->services = $services;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function setSection($section)
    {
        $this->section = $section;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function setJsonCaseFields($jsonCaseFields)
    {
        $this->jsonCaseFields = $jsonCaseFields;
    }

    public function getJsonCaseFields()
    {
        return $this->jsonCaseFields;
    }

    public function setSearchFields($searchFields)
    {
        $this->searchFields = $searchFields;
    }

    public function getSearchFields()
    {
        return $this->searchFields;
    }

    public function setAddSearchFields($addSearchFields)
    {
        $this->addSearchFields = $addSearchFields;
    }

    public function getAddSearchFields()
    {
        return $this->addSearchFields;
    }
}
