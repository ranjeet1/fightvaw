<?php

namespace App\Fightvaw\Lacc\Organization;

class Organization
{
    const REGISTRATION_NUMBER = '101';
    const IS_FOLLOW_UP_ENABLE = true;

    public function registrationNumber()
    {
        return self::REGISTRATION_NUMBER;
    }

    public function followup()
    {
        return self::IS_FOLLOW_UP_ENABLE;
    }

    public function JsonCaseFields()
    {
        return [
            'case_info', 'profile', 'contact', 'perpetrator', 'family_info', 'incident',
            'details',
        ];
    }

    public function services()
    {
        return [
            'Legal' => [
                'label' => 'Legal Service',
                'followUpTemplate' => false,
            ],
            'ParaLegal' => [
                'label' => 'ParaLegal Service',
                'followUpTemplate' => false,
            ],
            'Psychosocial' => [
                'label' => 'Psychosocial Service',
            ],
            'LaccMediation' => [
                'label' => 'Lacc Mediation Service',
            ],
            'CourtRepresentation' => [
                'label' => 'Court Representation Service',
            ],
            'FollowUp' => [
                'label' => 'Follow up',
            ],
        ];
    }
}
