<?php

namespace App\Fightvaw\Lacc\Data;


class Collection extends ServiceCollection
{

    public function getData($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        } else {
            return [];
        }
    }

    private function physicalMentalStatus()
    {
        return [
            'Normal', 'Physical Disable', 'Mental Disbable',
        ];
    }

    private function education()
    {
        return [
            'Illiterate', 'Literate', 'Under SLC', 'Higher Education', 'Graduate',
            'Master', 'PhD',
        ];
    }

    private function heardOfHelpLine()
    {
        $heardOfHelpLine = [
            'Radio', 'Newspaper', 'Outreach', 'Police', 'CDO', 'NGO', 'Friend', 'Old Clients',
            'Judicial Bodies', 'National Women Commission',
        ];
        asort($heardOfHelpLine);
        array_push($heardOfHelpLine, 'Others');
        return $heardOfHelpLine;
    }

    private function maritalStatus()
    {
        return [
            'Married', 'Unmarried', 'Widow', 'Single Women', 'Divorces', 'Remarriage',
        ];
    }

    private function typeOfMarriage()
    {
        $typeOfMarriage = [
            'Arranged', 'Love', 'Inter Caste', 'International', 'Child', 'Registered Marriage',
        ];
        asort($typeOfMarriage);
        return $typeOfMarriage;
    }

    private function legalProblem()
    {
        $legalProblem = [
            'Inheritance/Partition', 'Divorce', 'Maintenance', 'Dowry Return', 'Bigamy',
            'Battering', 'Marital Rape', 'Rape', 'Incest', 'Attempt to Rape', 'Sexual Harassment',
            'Recognition of Marriage', 'Narcotic Drugs', 'Citizenship', 'Human Trafficking',
            'Transactions', 'Coercion', 'Cheating', 'Writ', 'Verbal Abuse', 'Voidable Documents',
            'Forgery', 'Murder', 'Attempt to Murder', 'Land Disputes', 'Kidnap',
            'Witch Craft', 'Security',
        ];
        asort($legalProblem);
        array_push($legalProblem, 'Others');
        return $legalProblem;
    }

    private function hiddenCausesOfProblems()
    {
        $hiddenCausesOfProblems = [
            'Alcoholic', 'Ego Conflict', 'Battering', 'Gambling', 'Relationship with Other Women',
            'Bigamy', 'Given Birth to Daughter Only', 'Sexual Abuse', 'Casino', 'Habit of Visting Cabin Resturant',
            'Unemployment', 'Difference in Age', 'Difference in Cast', 'Drug Addict',
            'Not Provided Maintenance', 'Dowry', 'Physical Disable', 'Family Disputes',
            'Poverty', 'Rape', 'Attempt to Rape', 'Marital Rape', 'Health Problems',
            'Religion',
        ];
        asort($hiddenCausesOfProblems);
        array_push($hiddenCausesOfProblems, 'Others');
        return $hiddenCausesOfProblems;
    }

    private function counselling()
    {
        $counselling = [
            'Psychosocial Counselling', 'Legal Counselling', 'Court Representation',
            'Lacc Mediation', 'ParaLegal Service',
        ];
        asort($counselling);
        return $counselling;
    }
}
