<?php

namespace app\Fightvaw\Lacc\Data;

class ServiceCollection
{

    protected function civilFamilyCase()
    {
        $civilFamilyCase = [
            'Inheritance/Partition', 'Divorce', 'Maintenance', 'Dowry Return', 'Recognition of Marriage',
            'Transactions', 'Void Able Documents', 'Hiding Property', 'Land Disputes',
        ];
        asort($civilFamilyCase);
        return $civilFamilyCase;
    }

    protected function criminalCase()
    {
        $criminalCase = [
            'Bigmay', 'Battering', 'Marital Rape', 'Rape', 'Attempt to Rape', 'Sexual Harassment',
            'Narcotic Drugs', 'Human Trafficking', 'Coercion', 'Cheating', 'Verbal Abuse',
            'Forgery', 'Murder', 'Attempt to Murder', 'Kidnap', 'Witch Craft', 'Incest',
            'Corruption',
        ];
        asort($criminalCase);
        return $criminalCase;
    }

    protected function constitutionalCase()
    {
        $constitutionalCase = [
            'Writ of Habeas Corpus', 'Mandamus', 'Certiorari', 'Prohibition', 'Qua Warranto',
            'Injunction',
        ];
        asort($constitutionalCase);
        return $constitutionalCase;
    }

    protected function commercialCase()
    {
        $commercialCase = [
            'Contract', 'Arbitration', 'Company',
        ];
        asort($commercialCase);
        return $commercialCase;
    }

    protected function decisionName()
    {
        $decisionName = [
            'Court Mediation', 'Lacc Mediation', 'Court Decision', 'Dropout',
        ];
        asort($decisionName);
        return $decisionName;
    }

    protected function decisionResult()
    {
        return [
            'Won', 'Lose',
        ];
    }

    protected function paralegalServices()
    {
        $paralegalServices = [
            'Contact with family', 'Application to the CDO', 'Application to the police',
            'Application to the Land revenue office', 'Application to the VDC', 'Application to the Municipality',
            'Application to the DDC', 'Refer to the hospital', 'Refer to the shelter and other organization',
            'Document copied', 'Any kind of Application',
        ];
        asort($paralegalServices);
        return $paralegalServices;
    }
}
