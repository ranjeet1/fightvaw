<?php

namespace App\Fightvaw\Lacc\Elements\Cases;

class Cases
{

    public function elements()
    {
        return [
            'reg_no' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Number',
                        'rules' => 'required|unique:cases,reg_no',
                    ],
                ],
            ],
            'case_created' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'case_info' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Case',
                        'displayTitle' => false,
                    ],
                ],
                'clientType' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Client Type',
                            'choices' => ['Phone' => 'Phone', 'Walk' => 'Walk', 'Email' => 'Email'],
                            'expanded' => true,
                            'rules' => 'required',
                        ],
                    ],
                ],
                'counselorName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Counselor',
                            'rules' => 'required',
                        ],
                    ],
                ],
            ],
            'profile' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Client Profile',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Client Name',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other', ],
                            'expanded' => true,
                            'rules' => 'required',
                        ],
                    ],
                ],
                'ethnicity' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Ethnicity',
                        ],
                    ],
                ],
                'physicalMentalStatus' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Physical Mental Status',
                        ],
                    ],
                ],
                'education' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Education',
                        ],
                    ],
                ],
                'maritalStatus' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Marital Status',
                        ],
                    ],
                ],
                'typeOfMarriage' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Type of Marriage',
                        ],
                    ],
                ],
                'occupation' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Occupation',
                        ],
                    ],
                ],
                'monthlyIncome' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Monthly Income',
                        ],
                    ],
                ],
            ],
            'contact' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Client Contact Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'email' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Email',
                            'rules' => 'email',
                        ],
                    ],
                ],
                'phoneNumber' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Phone Number',
                            'rules' => 'numeric',
                        ],
                    ],
                ],
                'mobileNumber' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Mobile Number',
                            'rules' => 'numeric',
                        ],
                    ],
                ],
                'PermanentAddress' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Permanent Address',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'country' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'attr' => ['id' => 'per-country'],
                                'empty_value' => 'Select country',
                                'label' => 'Country',
                            ],
                        ],
                    ],
                    'nepalAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => " ",
                                    'wrapper' => ['class' => 'nepalAddress form-group'],
                                ],
                            ],    
                          'district' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'empty_value' => 'Select district',
                                        'label' => 'District',
                                    ],
                                ],
                            ],
                            'wardNo' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'Ward Number',
                                    ],
                                ],
                            ], 
                            'vdcMunicipality' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'VDC/Municipality',
                                    ],
                                ],
                            ],
                        ],                                   
                    'otherAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => ' ',
                                    'wrapper' => ['class' => 'otherAddress form-group'],
                                ],
                            ],   
                    
                        'description' => [
                            'input' => [
                                'fieldType' => 'textarea',
                                'options' => [
                                    'label' => 'Address',
                                ],
                            ],
                        ],  
                    ],
                ],
                'TemporaryAddress' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'displayTitle' => true,
                            'label' => 'Temporary Address',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'district' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'empty_value' => 'Select district',
                                'label' => 'District',
                            ],
                        ],
                    ],
                    'wardNo' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Ward Number',
                            ],
                        ],
                    ],
                    'vdcMunicipality' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'VDC/Municipality',
                            ],
                        ],
                    ],
                ],
            ],
            'family_info' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => true,
                        'label' => 'Family Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Name',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                        ],
                    ],
                ],
                'relation' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Relation',
                        ],
                    ],
                ],
                'contactNumber' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Contact Number',
                        ],
                    ],
                ],
                'address' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Address',
                        ],
                    ],
                ],
            ],
            'incident' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Incident',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'date' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Date',
                            'attr' => ['class' => 'input-name datetimepicker'],
                            'rules' => 'required',
                        ],
                    ],
                ],
                'typeOfViolence' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Type of Incident',
                            'rules' => 'required',
                            'multiple' => true,
                        ],
                    ],
                ],
                'legalProblem' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Legal Problem',
                            'multiple' => true,
                        ],
                    ],
                ],
                'hiddenCausesOfProblems' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Hidden Causes of Problems',
                            'multiple' => true,
                        ],
                    ],
                ],
                'description' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Description of Incident',
                            'rules' => 'required',
                        ],
                    ],
                ],
            ],
            'perpetrator' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Perpetrator Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Name',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other', ],
                            'expanded' => true,
                        ],
                    ],
                ],
                'occupation' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Occupation',
                        ],
                    ],
                ],
                'relationWithVictim' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Relation with Victim',
                        ],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Other Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'heardOfHelpLine' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Heard of Help Line',
                        ],
                    ],
                ],
                'counselling' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Required Services',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'yourAdvice' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Your Advice',
                        ],
                    ],
                ],
            ],
        ];
    }
}
