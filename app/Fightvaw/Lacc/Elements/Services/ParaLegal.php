<?php

namespace App\Fightvaw\Lacc\Elements\Services;

class ParaLegal
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "Name of Paralegal Service Provider's",
                            'rules' => 'required',
                        ],
                    ],
                ],
                'paralegalServices' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Services',
                            'multiple' => true,
                            'rules' => 'required',
                        ],
                    ],
                ],
                'other' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Other Services',
                        ],
                    ],
                ],
            ],
        ];
    }
}
