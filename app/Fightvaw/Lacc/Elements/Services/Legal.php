<?php

namespace App\Fightvaw\Lacc\Elements\Services;

class Legal
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'courtRegNo' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "Court Registration No",
                            'rules' => 'required',
                        ],
                    ],
                ],
                'courtCaseRegDate' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "Court Case Registration Date",
                            'attr' => ['class' => 'input-name datetimepicker'],
                            'rules' => 'required',
                        ],
                    ],
                ],
                'courtName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "Court Name",
                            'rules' => 'required',
                        ],
                    ],
                ],
                'lawyerName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "Lawyer Name",
                            'rules' => 'required',
                        ],
                    ],
                ],
                'cases' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Cases',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'civilFamilyCase' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'multiple' => true,
                                'label' => "Civil Family Case",
                            ],
                        ],
                    ],
                    'civilFamilyCaseOther' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => "Civil Family Case Other(Specify)",
                            ],
                        ],
                    ],
                    'criminalCase' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'multiple' => true,
                                'label' => "Criminal Case",
                            ],
                        ],
                    ],
                    'criminalCaseOther' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => "Criminal Case Other(Specify)",
                            ],
                        ],
                    ],
                    'constitutionalCase' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'multiple' => true,
                                'label' => "Constitutional Case",
                            ],
                        ],
                    ],
                    'commercialCase' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'multiple' => true,
                                'label' => "Commercial Case",
                            ],
                        ],
                    ],
                ],
                'decisions' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Court Decisions',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'decisionName' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'label' => "Decision",
                            ],
                        ],
                    ],
                    'date' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => "Date",
                                'attr' => ['class' => 'input-name datetimepicker'],
                            ],
                        ],
                    ],
                    'decisionResult' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'label' => "Result",
                            ],
                        ],
                    ],
                    'decisionRemarks' => [
                        'input' => [
                            'fieldType' => 'textarea',
                            'options' => [
                                'label' => "Remarks",
                            ],
                        ],
                    ],
                ],
                'implementation' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Implementation',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'date' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => "Date of Application of Implimentation",
                                'attr' => ['class' => 'input-name datetimepicker'],
                            ],
                        ],
                    ],
                    'remarks' => [
                        'input' => [
                            'fieldType' => 'textarea',
                            'options' => [
                                'label' => "Remarks",
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
