<?php

namespace App\Fightvaw\TPO\Organization;

class Organization
{
    const REGISTRATION_NUMBER = '100';
    const IS_FOLLOW_UP_ENABLE = false;

    public function registrationNumber()
    {
        return self::REGISTRATION_NUMBER;
    }

    public function followup()
    {
        return self::IS_FOLLOW_UP_ENABLE;
    }

    public function JsonCaseFields()
    {
        return [
            'profile', 'contact', 'perpetrator', 'family_info', 'incident', 'details',
        ];
    }

    public function services()
    {
        return [
            'Psychosocial' => [
                'label' => 'Psychosocial Service',
                'followup' => false,
                'followUpTemplate' => false,
            ],
        ];
    }

    public function searchFields()
    {
        return [
            'providerName' => [
                'label' => 'Psychosocial Provider Name',
                'field_type' => 'text',
                'type' => 'json',
                'field' => 'services.details',
                'path' => '{providerName}',
                'attrs' => [
                ],
            ],
            'providerDesignation' => [
                'label' => 'Psychosocial Provider Designation',
                'field_type' => 'select',
                'select_options' => [
                    'choices' => [
                    ],
                ],
                'attrs' => [
                    'class' => 'multiple-input',
                    'multiple' => true,
                    'placeholder' => 'Select Designation',
                ],
                'type' => 'json',
                'field' => 'services.details',
                'path' => '{providerDesignation}',
            ],
        ];
    }
}
