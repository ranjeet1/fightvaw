<?php

namespace App\Fightvaw\TPO\Data;

class Collection extends ServiceCollection
{

    public function getData($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        } else {
            return [];
        }
    }

    private function education()
    {
        return [
            'Illiterate',
            'Nonformal Education',
            'Grade 1', 'Grade 2', 'Grade 3', 'Grade 4', 'Grade 5',
            'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10',
            'Grade 11', 'Grade 12',
            'Bachelor',
            'Master & above',
        ];
    }

    private function occupation()
    {
        $occupation = [
            'Student', 'Business', 'Forming', 'Service',
        ];
        asort($occupation);
        array_push($occupation, 'No Occupation', 'Others');
        return $occupation;
    }

    private function incident()
    {
        $incident = [
            'Trafficking', 'Refugee', 'Domestic Violence', 'Child Abuse',
            'Torture', 'Sexual  Violence',
        ];
        asort($incident);
        array_push($incident, 'Others');
        return $incident;
    }
}
