<?php

namespace App\Fightvaw\TPO\Data;

class ServiceCollection
{
    protected function sessionNumber()
    {
        return range(1, 99);
    }

    protected function providerDesignation()
    {
        $providerDesignation = [
            'Community Based Psychosocial Worker', 'Psychosocial Counsellor', 'Psychologist',
            'Psychiatrist',
        ];
        asort($providerDesignation);
        return $providerDesignation;
    }

    protected function psychosocialIntervention()
    {
        $psychosocialIntervention = [
            'Supportive Counselling, Emotional Support',
            'Psycho-education (Inform about side effect of medicine, discussion on stress circle, education in different problems)',
            'Individual Psychosocial Counselling',
            'Group Intervention',
            'Information Giving',
            'Relaxation',
            'Strengthen Coping Strategies',
            'Normalization',
            'Family Intervention (Family Counselling, Couple Counselling)',
            'Problem Solving',
            'Healthy Activity Program (Based On Behaviour Activation)',
            'Counselling For Alcohol Problem',
            'Family Intervention For Psychotic Symptoms (FAM)',
            'Psychotherapy',
        ];
        asort($psychosocialIntervention);
        return $psychosocialIntervention;
    }

    protected function toolsTechniques()
    {
        $toolsTechniques = [
            'Brain storming' => 'Brain storming',
            'Using SUDS' => 'Using SUDS',
            'Check list' => 'Check list',
            'Observation' => 'Observation',
            'Communication Skills' => 'Communication Skills',
            'Relaxation' => 'Relaxation',
            'Me Map' => 'Me Map',
            'Discussion' => 'Discussion',
            'Diary Maintain' => 'Diary Maintain',
            'Emotional Support' => 'Emotional Support',
        ];
        asort($toolsTechniques);
        return $toolsTechniques;
    }

    protected function caseStatus()
    {
        return [
            'Ongoing', 'Terminated', 'Drop or No show up',
        ];
    }

    protected function serverityLevel()
    {
        return [
            'Mild', 'Moderate', 'High', 'Severe',
        ];
    }
}
