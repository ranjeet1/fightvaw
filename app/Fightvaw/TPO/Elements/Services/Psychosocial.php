<?php

namespace App\Fightvaw\TPO\Elements\Services;

use App\Fightvaw\TPO\Data\ServiceCollection;

class Psychosocial extends ServiceCollection
{

    public function __construct()
    {
        $serverityLevel = $this->serverityLevel();
        $this->data = array_combine($serverityLevel, $serverityLevel);
    }

    public function elements()
    {
        $service = [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
        ];
        $details = $this->details();
        $fields = array_merge($service, $details);

        return $fields;
    }

    private function details()
    {
        $details['details'] = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'displayTitle' => false,
                    'label' => 'Other information',
                ],
            ],
            'providerName' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => "Psychosocial Service Provider Name",
                    ],
                ],
            ],
            'providerDesignation' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'empty_value' => 'Select Designation',
                        'label' => ' Psychosocial Service Provider Designation',
                    ],
                ],
            ],
        ];
        $details['details']['assessment'] = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Assessment Form',
                    'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                ],
            ],
            'problems' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Observed/Reported Problems',
                        'wrapper' => ['class' => 'form-group subForm'],
                    ],
                ],
                'behaviouralProblem' => $this->behaviouralProblem(),
                'developmentProblem' => $this->developmentProblem(),
                'depressiveProblem' => $this->depressiveProblem(),
                'anxietyProblem' => $this->anxietyProblem(),
                'socialProblem' => $this->socialProblem(),
                'physicalProblem' => $this->physicalProblem(),
                'epilepsy' => $this->epilepsy(),
                'alcoholUsedDisorder' => $this->alcoholUsedDisorder(),
                'severeMentalHealthProblems' => $this->severeMentalHealthProblems(),
                'mentalHealthProblems' => $this->mentalHealthProblems(),
            ],
        ];
        $details['details']['followUp'] = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Follow up Form',
                    'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                ],
            ],
            'sessionNumber' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'label' => "Session Number",
                        'rules' => 'required',
                    ],
                ],
            ],
            'psychosocialIntervention' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'multiple' => true,
                        'label' => 'Psychosocial Intervention',
                    ],
                ],
            ],
            'toolsTechniques' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'multiple' => true,
                        'label' => 'Tools and Techniques',
                    ],
                ],
            ],
            'ImpactOfIntervention' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'multiple' => true,
                        'label' => 'Impact of Intervention',
                    ],
                ],
            ],
            'caseStatus' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'label' => 'Case Status',
                    ],
                ],
            ],
            'cases' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => 'Case(If terminated reason for termination)',
                    ],
                ],
            ],
            'remarks' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => 'Remarks',
                    ],
                ],
            ],
        ];

        return $details;
    }

    private function behaviouralProblem()
    {
        $problems = [
            'stealing' => 'Stealing',
            'runningAway' => 'RunningAway',
            'hyperreactivities' => 'Hyperreactivities',
            'agression' => 'Agression',
            'alcohol' => 'Alchol/Substance Use/Abuse',
            'soiling' => 'Soiling(Enuresis)',
            'uncontrolledFromOther' => 'Uncontrolled From Other',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Behavioural Problems',
                    'wrapper' => ['class' => 'form-group complainForm severe-complain small-form'],
                ],
            ],
        ];
        $fields = array_merge($inputs, $this->getFormInputs($problems),
            $this->otherProblem());

        return $fields;
    }

    private function developmentProblem()
    {
        $problems = [
            'mentalRetardation' => 'Mental Retardation',
            'learningDifficulties' => 'Learning Difficulties',
            'supressedFeeling' => 'Backwardness (Supressed Feeling)',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Development Problems',
                    'wrapper' => ['class' => 'form-group complainForm small-form'],
                ],
            ]
        ];
        $fields = array_merge($inputs, $this->getFormInputs($problems),
            $this->otherProblem());

        return $fields;
    }

    private function depressiveProblem()
    {
        $problems = [
            'sadness' => 'Sadness',
            'suicidal' => 'Suicidal Feeling/Thought/Attempted',
            'sleepingDisturbance' => 'Sleeping Disturbance',
            'somaticProblem' => 'Somatic Problem',
            'poorMindConcentration' => 'Poor Mind Concentration',
            'guiltBlamingOwnSelf' => 'Guilt/Blaming Own Self',
            'lossOfEnergy' => 'Loss of Appetite/Energy',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Depressive Problems',
                    'wrapper' => ['class' => 'form-group complainForm small-form    '],
                ],
            ]
        ];
        $fields = array_merge($inputs, $this->getFormInputs($problems),
            $this->otherProblem());

        return $fields;
    }

    private function anxietyProblem()
    {
        $problems = [
            'fear' => 'Fear',
            'nervousness' => 'Nervousness',
            'heartPounding' => 'Heart Pounding',
            'fainting' => 'Fainting',
            'restlessness' => 'Restlessness',
            'worry' => 'Worry',
            'sweating' => 'Sweating',
            'tremblingBody' => 'Trembling Body',
            'separationAnxiety' => 'Separation Anxiety',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Anxiety/Stress Related Problems',
                    'wrapper' => ['class' => 'form-group complainForm small-form'],
                ],
            ],
        ];
        $fields = array_merge($inputs, $this->getFormInputs($problems),
            $this->otherProblem());

        return $fields;
    }

    private function socialProblem()
    {
        $problems = [
            'familyConflict' => 'Family Conflict',
            'neglectFromOther' => 'Neglect From Other',
            'poorDailyActivities' => 'Poor Daily Activities',
            'socialWithdrawal' => 'Social Withdrawal',
            'victimofViolence' => 'Victim of Violence',
        ];
        $problemsInputs['parent'] = [
            'fieldType' => 'subForm',
            'options' => [
                'label' => 'Social Problems',
                'wrapper' => ['class' => 'form-group complainForm small-form'],
            ],
        ];
        $otherProblems = [
            'descrimination' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => false,
                        'wrapper' => ['class' => 'form-group full-width'],
                    ],
                ],
                'descriminationLabel' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Discrimination '
                        ],
                    ],
                ],
                'descriminationValue' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'empty_value' => 'Select',
                            'choices' => $this->serverityLevel(),
                            'wrapper' => ['class' => 'form-group hidden-label small-form-group'],
                        ],
                    ],
                ],
            ],
            'poorRelationship' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => false,
                        'wrapper' => ['class' => 'form-group full-width'],
                    ],
                ],
                'poorRelationshipLabel' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Poor Relationship with '
                        ],
                    ],
                ],
                'poorRelationshipValue' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'choices' => $this->serverityLevel(),
                            'empty_value' => 'Select',
                            'wrapper' => ['class' => 'form-group hidden-label small-form-group'],
                        ],
                    ],
                ],
            ],
        ];
        $inputs = array_merge($this->getFormInputs($problems),
            $otherProblems);
        $problemsInputs = array_merge($problemsInputs, $inputs);

        return $problemsInputs;
    }

    private function physicalProblem()
    {
        $problems = [
            'headache' => 'Headache',
            'stomach' => 'Stomach',
            'fits' => 'Fits(Non Epileptic)',
            'hivAidsRelated' => 'HIV/AIDS Related',
            'restlessness' => 'Restlessness',
            'shakingBody' => 'Shaking Body',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Physical Problems',
                    'wrapper' => ['class' => 'form-group complainForm small-form'],
                ],
            ],
        ];
        $otherProblem = $this->otherProblem();
        $gynechology = $this->gynechologyProblem();
        $fields = array_merge($inputs, $this->getFormInputs($problems),
            $otherProblem, ['gynechologyProblem' => $gynechology]);

        return $fields;
    }

    private function gynechologyProblem()
    {
        $problems = [
            'abortion' => 'Abortion',
            'miscarriage' => 'Miscarriage',
            'sti' => 'STI',
            'uterusProlapsed' => 'Uterus Prolapsed',
            'irregularMenstruation' => 'Irregular Menstruation Cycle',
            'lowAbdomenPain' => ' Low Abdomen Pain',
            'whiteWaterDischarge' => 'White Water Discharge',
        ];
        $inputs = [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => 'Gynaecology and Reproductive Health Problems',
                    'wrapper' => ['class' => 'form-group complainForm sub-complainForm small-form'],


                ],
            ],
        ];
        $fields = array_merge($inputs, $this->getFormInputs($problems));

        return $fields;
    }

    private function epilepsy()
    {
        return [
            'input' => [
                'fieldType' => 'choice',
                'options' => [
                    'label' => 'Epilepsy',
                    'choices' => ['Yes' => 'Yes', 'No' => 'No'],
                    'expanded' => true,
                    'wrapper' => ['class' => 'form-group complain'],
                ],
            ],
        ];
    }

    private function alcoholUsedDisorder()
    {
        return [
            'input' => [
                'fieldType' => 'choice',
                'options' => [
                    'label' => ' Alcohol Used Disorder',
                    'choices' => ['Yes' => 'Yes', 'No' => 'No'],
                    'expanded' => true,
                    'wrapper' => ['class' => 'form-group complain'],
                ],
            ],
        ];
    }

    private function severeMentalHealthProblems()
    {
        return [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => ' Severe Mental Health Problems',
                    'wrapper' => ['class' => 'form-group complainForm'],
                ],
            ],
            'psychotic' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'label' => 'Psychotic',
                        'choices' => ['Yes' => 'Yes', 'No' => 'No'],
                        'expanded' => true,
                        'wrapper' => ['class' => 'form-group complain'],
                    ],
                ],
            ],
            'pstd' => [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'label' => 'PTSD',
                        'choices' => ['Yes' => 'Yes', 'No' => 'No'],
                        'expanded' => true,
                        'wrapper' => ['class' => 'form-group complain'],
                    ],
                ],
            ],
            'other-1' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'multiple_field' => true,
                        'displayTitle' => false,
                        'wrapper' => [
                            'class' => 'form-group hidden-label other-form add-more-cross',
                            'data-repeat' => 1,
                        ],
                    ],
                ],
                'label' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'attr' => ['placeholder' => 'Other(Specify)...'],
                        ],
                    ],
                ],
                'value' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                        ],
                    ],
                ],
            ],
            'addProblem' => [
                'input' => [
                    'fieldType' => 'button',
                    'options' => [
                        'label' => 'Add Problem',
                        'attr' => ['class' => 'form-group submit-button submit-dash add-more'],
                    ],
                ],
            ],
        ];
    }

    private function mentalHealthProblems()
    {
        return [
            'parent' => [
                'fieldType' => 'subForm',
                'options' => [
                    'label' => ' Causes/Reasons for Psychological/Mental Health Problems',
                    'wrapper' => ['class' => 'form-group complainForm psychological-complain'],
                ],
            ],
            'biological' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => ' Biological',
                    ],
                ],
            ],
            'social' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => 'Social',
                    ],
                ],
            ],
            'physical' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => 'Physical',
                    ],
                ],
            ],
            'others' => [
                'input' => [
                    'fieldType' => 'textarea',
                    'options' => [
                        'label' => 'Others',
                    ],
                ],
            ],
        ];
    }

    private function getFormInputs($elements)
    {
        $formInputs = [];
        foreach ($elements as $name => $label) {
            $formInputs[$name] = [
                'input' => [
                    'fieldType' => 'choice',
                    'options' => [
                        'label' => $label,
                        'empty_value' => 'Select',
                        'choices' => $this->data,
                    ],
                ],
            ];
        }

        return $formInputs;
    }

    private function otherProblem()
    {
        return [
            'other-1' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'multiple_field' => true,
                        'displayTitle' => false,
                        'wrapper' => [
                            'class' => 'form-group hidden-label other-form add-more-cross',
                            'data-repeat' => 1,
                        ],
                    ],
                ],
                'label' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'attr' => ['placeholder' => 'Other(Specify)...'],
                        ],
                    ],
                ],
                'value' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'choices' => $this->data,
                            'empty_value' => 'Select',
                        ],
                    ],
                ],
            ],
            'addProblem' => [
                'input' => [
                    'fieldType' => 'button',
                    'options' => [
                        'label' => 'Add Problem',
                        'attr' => ['class' => 'form-group submit-button submit-dash add-more'],
                    ],
                ],
            ],
        ];
    }
}
