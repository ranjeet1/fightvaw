<?php

namespace App\Fightvaw\TPO\Elements\Cases;

class Cases
{

    public function elements()
    {
        return [
            'reg_no' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Number',
                        'rules' => 'required|unique:cases,reg_no',
                    ],
                ],
            ],
            'case_created' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                        'rules' => 'required',
                    ],
                ],
            ],
            'profile' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => false,
                        'label' => 'Client Profile',
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Client Name',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other',],
                            'expanded' => true,
                            'rules' => 'required',
                        ],
                    ],
                ],
                'ethnicity' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Ethnicity',
                        ],
                    ],
                ],
                'education' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Education',
                        ],
                    ],
                ],
                'occupation' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Occupation',
                        ],
                    ],
                ],
            ],
            // Profile
            'contact' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [

                        'label' => 'Client Contact Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'email' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Email',
                            'rules' => 'email',
                        ],
                    ],
                ],
                'phoneNumber' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Phone Number',
                            'rules' => 'numeric',
                        ],
                    ],
                ],
                'mobileNumber' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Mobile Number',
                            'rules' => 'numeric',
                        ],
                    ],
                ],
                'PermanentAddress' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Permanent Address',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'country' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'attr' => ['id' => 'per-country'],
                                'empty_value' => 'Select country',
                                'label' => 'Country',
                            ],
                        ],
                    ],
                    'nepalAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => " ",
                                    'wrapper' => ['class' => 'nepalAddress form-group'],
                                ],
                            ],    
                          'district' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'empty_value' => 'Select district',
                                        'label' => 'District',
                                    ],
                                ],
                            ],
                            'wardNo' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'Ward Number',
                                    ],
                                ],
                            ], 
                            'vdcMunicipality' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'VDC/Municipality',
                                    ],
                                ],
                            ],
                        ],                                   
                    'otherAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => ' ',
                                    'wrapper' => ['class' => 'otherAddress form-group'],
                                ],
                            ],   
                    
                        'description' => [
                            'input' => [
                                'fieldType' => 'textarea',
                                'options' => [
                                    'label' => 'Address',
                                ],
                            ],
                        ],  
                    ],
                ],
                'TemporaryAddress' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [

                            'label' => 'Temporary Address',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'district' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'empty_value' => 'Select district',
                                'label' => 'District',
                            ],
                        ],
                    ],
                    'wardNo' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Ward Number',
                            ],
                        ],
                    ],
                    'vdcMunicipality' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'VDC/Municipality',
                            ],
                        ],
                        'postalAddress' => [
                            'input' => [
                                'fieldType' => 'textarea',
                                'options' => [
                                    'label' => 'Detail Postal Address',
                                    'rules' => 'required',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'family_info' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [

                        'label' => 'Emergency Contact',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => ' Name',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                        ],
                    ],
                ],
                'relation' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Relation',
                        ],
                    ],
                ],
                'contact' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Contact Number',
                        ],
                    ],
                ],
            ], //end of emergency contact
            'incident' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Incident Details',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'date' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Date',
                            'attr' => ['class' => 'input-name datetimepicker'],
                            'rules' => 'required',
                        ],
                    ],
                ],
                'typeOfViolence' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Type of Incident',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'district' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'empty_value' => 'Select district',
                            'label' => 'District',
                            'rules' => 'required',
                            ],
                        ],
                    ],
                'location' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => ' Location',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'description' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Description of Incident',
                            'rules' => 'required',
                        ],
                    ],
                ],
            ], // incidentDetails
            'perpetrator' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [

                        'label' => 'Main Suspected Perpetrator Details',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Name',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other',],
                            'expanded' => true,
                        ],
                    ],
                ],
                'ethnicity' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Ethnicity',
                        ],
                    ],
                ],
                'relationWithVictim' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Relation with Victim',
                        ],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Other information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'serviceProviderName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Previous Services Provider & Affiliated Organization',
                        ],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => ' Referrer Name',
                        ],
                    ],
                ],
                'organization' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => ' Referrer Organization',
                        ],
                    ],
                ],
                'pychosocialCounselling' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => ' Expectations From Pyschosocial Counselling',
                        ],
                    ],
                ],
            ], //end of OtherInformation
        ];
    }
}
