<?php $details                    = JsonHelper::JsonDecode($service->details) ?>
<div class="title-info">
    <ul>
        <li><label>Psychosocial Service Provider Name</label><span>{{$details->providerName}}</span></li>
        <li><label>Psychosocial Service Provider Designation</label><span>{{$details->providerDesignation}}</span></li>
    </ul>
    <div class="assessment">
        <div class="title">Assessment</div>
        <div class="sub-title">Observed/Reported Problems</div>

        <div class="sub-inner-title">Behavioural Problem</div>
        <?php $behaviouralComplain        = $details->assessment->problems->behaviouralProblem; ?>
        <ul class="rating">
            <li><label>Stealing </label><span>{{"(".$behaviouralComplain->stealing.")"}}</span></li>
            <li><label>Running Away </label><span>{{"(".$behaviouralComplain->runningAway.")"}}</span></li>
            <li><label>Hyperactivities </label><span>{{"(".$behaviouralComplain->hyperreactivities.")"}}</span></li>
            <li><label>Agression </label><span>{{"(".$behaviouralComplain->agression.")"}}</span></li>
            <li><label>Alcohol/Substance Use/Abuse </label><span>{{"(".$behaviouralComplain->alcohol.")"}}</span></li>
            <li><label>Bedwetting/Soiling (enuresis) </label><span>{{"(".$behaviouralComplain->soiling.")"}}</span></li>
            <li><label>Uncontrolled From Other </label><span>{{"(".$behaviouralComplain->uncontrolledFromOther.")"}}</span></li>
            {!!$serviceGenerator->getValue($behaviouralComplain)!!}
        </ul>

        <div class="assessment-toggle">
            <div class="sub-inner-title">Development Problems</div>
            <?php $developmentComplain        = $details->assessment->problems->developmentProblem; ?>
            <ul class="rating">
                <li><label>Mental Retardation </label><span>{{"(".$developmentComplain->mentalRetardation.")"}}</span></li>
                <li><label>Learning Difficulties </label><span>{{"(".$developmentComplain->learningDifficulties.")"}}</span></li>
                <li><label>Backwardness (Supressed Feeling) </label><span>{{"(".$developmentComplain->supressedFeeling.")"}}</span></li>
                {!!$serviceGenerator->getValue($developmentComplain)!!}
            </ul>

            <div class="sub-inner-title">Depressive Problems</div>
            <?php $depressiveComplain         = $details->assessment->problems->depressiveProblem; ?>
            <ul class="rating">
                <li><label>Sadness </label><span>{{"(".$depressiveComplain->sadness.")"}}</span></li>
                <li><label>Poor Mind Concentration </label><span>{{"(".$depressiveComplain->poorMindConcentration.")"}}</span></li>
                <li><label>Suicidal Feeling/Thought/Attempted </label><span>{{"(".$depressiveComplain->suicidal.")"}}</span></li>
                <li><label>Guilt/Blaming Own Self </label><span>{{"(".$depressiveComplain->guiltBlamingOwnSelf.")"}}</span></li>
                <li><label>Sleeping Disturbance </label><span>{{"(".$depressiveComplain->sleepingDisturbance.")"}}</span></li>
                <li><label>Loss of Appetite/Energy </label><span>{{"(".$depressiveComplain->lossOfEnergy.")"}}</span></li>
                <li><label>Somatic Complain </label><span>{{"(".$depressiveComplain->somaticProblem.")"}}</span></li>
                {!!$serviceGenerator->getValue($depressiveComplain)!!}
            </ul>

            <div class="sub-inner-title">Anxiety/Stress Related Problems</div>
            <?php $anxietyStressComplain      = $details->assessment->problems->anxietyProblem; ?>
            <ul class="rating">
                <li><label>Fear </label><span>{{"(".$anxietyStressComplain->fear.")"}}</span></li>
                <li><label>Worry </label><span>{{"(".$anxietyStressComplain->worry.")"}}</span></li>
                <li><label>Nervousness </label><span>{{"(".$anxietyStressComplain->nervousness.")"}}</span></li>
                <li><label>Sweating </label><span>{{"(".$anxietyStressComplain->sweating.")"}}</span></li>
                <li><label>Heart Pounding </label><span>{{"(".$anxietyStressComplain->heartPounding.")"}}</span></li>
                <li><label>Trembling Body </label><span>{{"(".$anxietyStressComplain->tremblingBody.")"}}</span></li>
                <li><label>Fainting </label><span>{{"(".$anxietyStressComplain->fainting.")"}}</span></li>
                <li><label>Separation Anxiety </label><span>{{"(".$anxietyStressComplain->separationAnxiety.")"}}</span></li>
                <li><label>Restlessness </label><span>{{"(".$anxietyStressComplain->restlessness.")"}}</span></li>
                {!!$serviceGenerator->getValue($anxietyStressComplain)!!}
            </ul>

            <div class="sub-inner-title">Social Problems</div>
            <?php $socialComplain             = $details->assessment->problems->socialProblem; ?>
            <ul class="rating">
                <li><label>Family Conflict </label><span>{{"(".$socialComplain->familyConflict.")"}}</span></li>
                <li><label>Social Withdrawal </label><span>{{"(".$socialComplain->socialWithdrawal.")"}}</span></li>
                <li><label>Neglect From Other </label><span>{{"(".$socialComplain->neglectFromOther.")"}}</span></li>
                <li><label>Victim of Violence </label><span>{{"(".$socialComplain->victimofViolence.")"}}</span></li>
                <li><label>Social Withdrawal </label><span>{{"(".$socialComplain->socialWithdrawal.")"}}</span></li>
                @if($socialComplain->poorRelationship->poorRelationshipValue && $socialComplain->poorRelationship->poorRelationshipLabel)
                <li><label>Poor Realationship with  {{$socialComplain->poorRelationship->poorRelationshipLabel}}

                    </label><span>
                         @if($socialComplain->poorRelationship->poorRelationshipValue == 0)
                            Mild
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 1)
                            Moderate
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 2)
                            High
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 3)
                            Severe
                        @endif
                    </span></li>

                @endif
                @if($socialComplain->descrimination->descriminationLabel && $socialComplain->descrimination->descriminationValue)
                <li><label>Discrimination {{"(".$socialComplain->descrimination->descriminationLabel.")"}} </label>
                    <span>
                        @if($socialComplain->poorRelationship->poorRelationshipValue == 0)
                            Mild
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 1)
                            Moderate
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 2)
                            High
                        @elseif($socialComplain->poorRelationship->poorRelationshipValue == 3)
                            Severe
                        @endif
                    </span></li>



                @endif
                {!!$serviceGenerator->getValue($socialComplain)!!}
            </ul>
            <div class="sub-inner-title">Physical Problems</div>
            <?php $physicalComplain           = $details->assessment->problems->physicalProblem; ?>
            <ul class="rating">
                <li><label>Headache </label><span>{{"(".$physicalComplain->headache.")"}}</span></li>
                <li><label>Stomach </label><span>{{"(".$physicalComplain->stomach.")"}}</span></li>
                <li><label>Fits(Non Epileptic) </label><span>{{"(".$physicalComplain->fits.")"}}</span></li>
                <li><label>HIV/AIDS Related </label><span>{{"(".$physicalComplain->hivAidsRelated.")"}}</span></li>
                <li><label>Restlessness </label><span>{{"(".$physicalComplain->restlessness.")"}}</span></li>
                <li><label>Shaking Body </label><span>{{"(".$physicalComplain->shakingBody.")"}}</span></li>
                {!!$serviceGenerator->getValue($physicalComplain)!!}
                <div class="sub-inner-section">
                    <div class="assessment-sub-inner-title">Gynaecology and Reproductive Health Problems</div>
                    <?php $gynaecologyComplain        = $physicalComplain->gynechologyProblem; ?>
                    <li><label>Abortion </label><span>{{"(".$gynaecologyComplain->abortion.")"}}</span></li>
                    <li><label>Miscarriage </label><span>{{"(".$gynaecologyComplain->miscarriage.")"}}</span></li>
                    <li><label>STI </label><span>{{"(".$gynaecologyComplain->sti.")"}}</span></li>
                    <li><label>Uterus Prolapsed </label><span>{{"(".$gynaecologyComplain->uterusProlapsed.")"}}</span></li>
                    <li><label>Irregular Menstruation Cycle </label><span>{{"(".$gynaecologyComplain->irregularMenstruation.")"}}</span></li>
                    <li><label>Low Abdomen Pain </label><span>{{"(".$gynaecologyComplain->lowAbdomenPain.")"}}</span></li>
                    <li><label>White Water Discharge </label><span>{{"(".$gynaecologyComplain->whiteWaterDischarge.")"}}</span></li>
                </div>
            </ul>

            <div class="sub-inner-title line-ans-title">Epilepsy</div><div class="line-ans">{{(isset($details->assessment->problems->epilepsy))?($details->assessment->problems->epilepsy):('')}}</div>

            <div class="sub-inner-title line-ans-title">Alcohol Used Disorder</div><div class="line-ans">{{(isset($details->assessment->problems->alcoholUsedDisorder))?($details->assessment->problems->alcoholUsedDisorder):('')}}</div>

            <div class="sub-inner-title sub-complain">Severe Mental Health Problems</div>
            <?php $severeMentalHealthComplain = $details->assessment->problems->severeMentalHealthProblems; ?>
            <ul class="complain-info">
                <li><label>Psychotic </label><span>{{(isset($severeMentalHealthComplain->psychotic))?($severeMentalHealthComplain->psychotic):('')}}</span></li>
                <li><label>PTSD </label><span>{{(isset($severeMentalHealthComplain->ptsd))?($severeMentalHealthComplain->ptsd):('')}}</span></li>
                {!!$serviceGenerator->getValue($severeMentalHealthComplain)!!}
            </ul>

            <div class="sub-inner-title">Causes/Reasons for Psychological/Mental Health Problems</div>
            <?php $reasons = $details->assessment->problems->mentalHealthProblems; ?>
            <ul>
                <li class="more-description"><label>Biological </label><span>{!!nl2br($reasons->biological)!!}</span></li>
                <li class="more-description"><label>Social </label><span>{!!nl2br($reasons->social)!!}</span></li>
                <li class="more-description"><label>Physical </label><span>{!!nl2br($reasons->physical)!!}</span></li>
                <li class="more-description"><label>Other </label><span>{!!nl2br($reasons->others)!!}</span></li>
            </ul>

            <div class="title">Follow Up</div>
            <?php 
            $interventions=[]; 
            $tools=[]; 
            if (isset($details->followUp->psychosocialIntervention))
                $interventions=$details->followUp->psychosocialIntervention; 
            if (isset($details->followUp->toolsTechniques))
                $tools = $details->followUp->toolsTechniques;
            ?>
            <ul>    
                <li class="more-description"><label>Session Number</label><span>{{$details->followUp->sessionNumber}}</span></li>
                <li class="more-description"><label>Psychosocial Intervention</label><span> @foreach ($interventions as $intervention)<ul> <li>{{$intervention}}</li></ul> @endforeach</span></li>
                <li class="more-description"><label>Tools and Techniques</label> <span>@foreach ($tools as $tool)<ul><li>{{$tool}}</li></ul>@endforeach</span></li>
                <li class="more-description"><label>Impact of Intervention</label><span>{!!nl2br($details->followUp->ImpactOfIntervention)!!}</span></li>
                <li class="more-description"><label>Case Status</label><span> {{$details->followUp->caseStatus}}</span></li>
                <li class="more-description"><label>Case</label><span> {!!nl2br($details->followUp->cases)!!}</span></li>
                <li class="more-description"><label>Remarks</label><span> {!!nl2br($details->followUp->remarks)!!}</span><li>
            </ul>

        </div>
        <span class="more">View More...</span>
        <span class="less">View Less...</span>
    </div>
</div>