<?php

namespace App\Fightvaw\CWIN\Organization;

class Organization
{
    const REGISTRATION_NUMBER = '102';
    const IS_FOLLOW_UP_ENABLE = true;

    public function registrationNumber()
    {
        return self::REGISTRATION_NUMBER;
    }

    public function followup()
    {
        return self::IS_FOLLOW_UP_ENABLE;
    }

    public function JsonCaseFields()
    {
        return [
            'case_info', 'profile', 'contact', 'perpetrator', 'family_info', 'incident',
            'details',
        ];
    }

    public function services()
    {
        return [
            'EmgergencyRescue' => [
                'label' => 'Emgergency Rescue Service',
                'followUpTemplate' => false,
            ],
            'EmergencyShelter' => [
                'label' => 'Emergency Shelter Service',
                'followUpTemplate' => false,
            ],
            'AmbulanceVehicle' => [
                'label' => 'Ambulance/Vehicle Service',
                'followUpTemplate' => false,
            ],
            'MedicalFirstAid' => [
                'label' => 'Medical First Aid Service',
                'followUpTemplate' => false,
            ],
            'TreatmentInHospital' => [
                'label' => 'Treatment in Hospital  Service',
                'followUpTemplate' => false,
            ],
            'EmergencySupport' => [
                'label' => 'Emergency Support  Service',
                'followUpTemplate' => false,
            ],
            'Counseling' => [
                'label' => 'Counseling Service',
                'followUpTemplate' => false,
            ],
            'Legal' => [
                'label' => 'Legal Service',
                'followUpTemplate' => false,
            ],
            'FamilyReintegration' => [
                'label' => 'Family Reintegration Service',
                'followUpTemplate' => false,
            ],
            'SponsorshipEducation' => [
                'label' => 'Sponsorship (Education) Service',
                'followUpTemplate' => false,
            ],
            'SponsorshipTraining' => [
                'label' => 'Sponsorship (Training) Service',
                'followUpTemplate' => false,
            ],
            'InitiationForMissingChildren' => [
                'label' => 'Initiation For Missing Children Service',
                'followUpTemplate' => false,
            ],
            'FamilyTracing' => [
                'label' => 'Family Tracing Service',
                'followUpTemplate' => false,
            ],
            'Monitoring' => [
                'label' => 'Monitoring Service',
            ],
            'FactFinding' => [
                'label' => 'Fact finding Service',
            ],
            'Referred' => [
                'label' => 'Referral Service',
                'followUpTemplate' => false,
            ],
            'FollowUp' => [
                'label' => 'Follow up',
            ],
        ];
    }
}
