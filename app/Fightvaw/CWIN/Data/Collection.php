<?php

namespace App\Fightvaw\CWIN\Data;

class Collection extends ServiceCollection
{

    public function getData($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        } else {
            return [];
        }
    }

    private function mediumInfo()
    {
        $mediumInfo = [
            '1098 Telephone', 'Direct Helpline', 'Field Visit', 'Letter', 'Radio/FM/TV',
            'E-Mail', 'Print Media', 'Fax', 'Facebook', 'SMS',
        ];
        asort($mediumInfo);
        array_push($mediumInfo, 'Others');
        return $mediumInfo;
    }

    private function sourceCase()
    {
        $sourceCase = [
            'Direct Helpline', 'School(Teacher)', 'Police', 'Community', 'Child Clubs',
            'Child Friends', 'CWIN Contact Center', 'Labour Office', 'CHN-Kathmandu',
            'CHN-Morang', 'CHN-Makwanpur', 'CHN-Banke', 'CHN-Kaski', 'CHN-Kailai',
            'NGO', 'INGO', 'DCPC', 'VCPC', 'DCWB', 'CCWB', 'Khoj Talash', 'Adolescent Forum',
            'CBOs', 'Court', 'Legal Organization', 'MCPC', 'WCPC', 'WCO', 'Child Line India',
        ];
        asort($sourceCase);
        array_push($sourceCase, 'Other Child Helplines', 'Others');
        return $sourceCase;
    }

    private function registrationHelpLineDistrict()
    {
        $registrationHelpLineDistrict = [
            'Kathmandu', 'Morang', 'Makwanpur', 'Banke', 'Kaski', 'Kailali',
        ];
        asort($registrationHelpLineDistrict);
        return $registrationHelpLineDistrict;
    }

    private function cwinProjectInvolvement()
    {
        $cwinProjectInvolvement = [
            'PRECLOR (Saphlaneer)', 'CCFE for QE (Intervita)', 'Girl Power Program (ICDI)',
            'Child Marriage (ICDI)', 'Empowerment of Street Children(Comic Relief)',
        ];
        asort($cwinProjectInvolvement);
        array_push($cwinProjectInvolvement, 'Others');
        return $cwinProjectInvolvement;
    }

    private function childBackground()
    {
        $childBackground = [
            'Student', 'Domestic Worker', 'Hotel Worker', 'Agriculture Labor', 'Carpet labour',
            'Transportation Worker', 'Brick Kline', 'Porter', 'Stone Quarry', 'Street Children',
            'Slum', 'Shepherd', 'Construction Work', 'Embroidery', 'Circus Worker',
            'Beggar', 'Young Kid(Below age 3)', 'Factory Worker', 'Entertainment Worker',
            'Children used for illegal activity', 'Factory Worker', 'Entertainment Worker',
            'Children used for illegal activity', 'Nothing Specific', 'Unidentified',
        ];
        asort($childBackground);
        array_push($childBackground, 'Others');
        return $childBackground;
    }

    private function parentCondition()
    {
        return [
            'Only Father', 'Only Mother', 'Both', 'No Parents', 'Unidentified',
        ];
    }

    private function abandonedBy()
    {
        return [
            'Father', 'Mother', 'Both',
        ];
    }

    private function stepParents()
    {
        return [
            'Step Mother', 'Step Father', 'Both Step Parents',
        ];
    }

    private function parentalBackground()
    {
        $parentalBackground = [
            'Agriculturist', 'Direct Wage Labor', 'Business (Small)', 'Business (Medium/Big)',
            'Government Service (Officer Level)', 'Government Service (non- officer level)',
            'Private / Public Service (Officer Level)', 'Private / Public Service (non-officer level)',
            'Foreign Employment', 'Transportation Worker', 'Unemployed', 'Unidentified',
            'Porter', 'No Parents',
        ];
        asort($parentalBackground);
        array_push($parentalBackground, 'Others');
        return $parentalBackground;
    }

    private function typeOfSexualAbuse()
    {
        $typeOfSexualAbuse = [
            'Rape', 'Attempt to Rape', 'Sexual Harassment',
        ];
        asort($typeOfSexualAbuse);
        return $typeOfSexualAbuse;
    }

    private function reasonBehindDeath()
    {
        $reasonBehindDeath = [
            'Sick', 'Nature Calamities', 'Suicide', 'Murder', 'Accident', 'Suspense Death',
        ];
        asort($reasonBehindDeath);
        array_push($reasonBehindDeath, 'Others');
        return $reasonBehindDeath;
    }

    private function typeOfSickness()
    {
        $typeOfSickness = [
            'Viral Fiver', 'Diarrhea', 'Vomiting', 'Cough and Cold', 'Headache',
            'Malnutrition', 'Cancer', 'TB', 'Heart Diseases', 'Epilepsy', 'Food Poisoning',
            'Kidney Problem', 'ENT', 'Stomach Ache', 'polio',
        ];
        asort($typeOfSickness);
        return $typeOfSickness;
    }

    private function typeOfAccident()
    {
        $typeOfAccident = [
            'Road Accident', 'Burns', 'Fall down From house/school', 'Fall down From height',
            'Disater (Flood/Landslide/Earthquake/Lightening/Fire)', 'Mine Blasting',
        ];
        asort($typeOfAccident);
        array_push($typeOfAccident, 'Others');
        return $typeOfAccident;
    }

    private function typeOfChildMarriage()
    {
        $typeOfChildMarriage = [
            'Force Child Marriage With Adult', 'Force Child Marriage With Child',
            'Child Love Marriage With Adult', 'Child Love Marriage With Child ',
        ];
        asort($typeOfChildMarriage);
        return $typeOfChildMarriage;
    }

    private function reasonBehindTrafficking()
    {
        $reasonBehindTrafficking = [
            'Child Labor', 'Sexual Abuse', 'Entertainment Labor',
        ];
        asort($reasonBehindTrafficking);
        array_push($reasonBehindTrafficking, 'Others');
        return $reasonBehindTrafficking;
    }

    private function govAuthoritiesInvolved()
    {
        $govAuthoritiesInvolved = [
            'Police/Women Cell', 'DCWB', 'WCO', 'Labor Office', 'NHRC', 'VDC', 'VCPC',
            'MCPC', 'DDC', 'School', 'Hospital', 'CCWB', 'WCPC', 'MOWCSW', 'National Women Commission',
        ];
        asort($govAuthoritiesInvolved);
        array_push($govAuthoritiesInvolved, 'Others');
        return $govAuthoritiesInvolved;
    }

    public function education()
    {
        return [
            'Illiterate', 'Nonformal Education', 'Grade 1', 'Grade 2', 'Grade 3',
            'Grade 4', 'Grade 5', 'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10',
            'Grade 11', 'Grade 12', 'Bachelor', 'Master & above',
        ];
    }

    public function mainChildProblem()
    {
        $mainChildProblem = [
            'Corporal Punishment', 'Labour Exploitation', 'Lost', 'Sponser',
            'Sponser', 'Neglect', 'Orphaned', 'Child Delinquency', 'Psycho-Social Problem',
            'Family Coflict', 'Parents in Jail', 'Bullying', 'Kidnapping', 'Financial Crisis',
            'Drug Used', 'Careless to Children', 'HIV/AIDS', 'Physical Disable',
            'Mentally ill', 'Sexual Abuse', 'Death', 'Child Sickness', 'Accident',
            'Child Marriage', 'Trafficking', 'Mentally Challenged', 'Glue Sniffing',
            'Telephone Abuse', 'Online Abuse', 'Displaced', 'Birth Registration',
            'Citizenship Certificate', 'Birth Registration', 'Citizen Certificate',
            'Request for Shelter', 'Sponsor(ES)', 'Sponsor(Training)', 'Physical Abuse(Torture)',
        ];
        asort($mainChildProblem);
        array_push($mainChildProblem, 'Others');
        return $mainChildProblem;
    }
}
