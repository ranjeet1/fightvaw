<?php

namespace App\Fightvaw\CWIN\Data;

class ServiceCollection
{

    protected function tracingStatus()
    {
        return [
            'Family Traced', 'Family Still not Traced',
        ];
    }

    protected function currentStatus()
    {
        return [
            'Found', 'Still not Found',
        ];
    }

    protected function sponsorshipFor()
    {
        $sponsorshipFor = [
            'Tuition Fee', 'Stationeries', 'Uniform',
        ];
        asort($sponsorshipFor);
        return $sponsorshipFor;
    }

    protected function caseFiledIn()
    {
        $caseFiledIn = [
            'Police(FIR)', 'DAO/DCWB', 'Labour Office', 'Court', 'WCO', 'CCWB', 'NHRC',
        ];
        asort($caseFiledIn);
        array_push($caseFiledIn, 'Others');
        return $caseFiledIn;
    }

    protected function caseEndedThrough()
    {
        $caseEndedThrough = [
            'Mediation', 'Negotiation', 'Court', 'Case Hostile', 'Still in process',
        ];
        asort($caseEndedThrough);
        array_push($caseEndedThrough, 'Others');
        return $caseEndedThrough;
    }

    protected function typeOfCounseling()
    {
        $typeOfCounseling = [
            'Individual Psycho-social Counseling', 'Motivational Counseling to Child',
            'Motivational Counseling to Family', 'Group Counseling', 'Family Counseling',
            'Legal Counseling', 'Telephone Counseling',
        ];
        asort($typeOfCounseling);
        return $typeOfCounseling;
    }

    protected function resultOfCounseling()
    {
        return [
            'Not Healed', 'Partially Healed', 'Fully Healed',
        ];
    }

    protected function currentSituation()
    {
        $currentSituation = [
            'Reintegrated with Family', 'Still at Center', 'Referred', 'Left Helpline Taking Permission',
            'Left Helpline Without Taking Permission',
        ];
        asort($currentSituation);
        return $currentSituation;
    }

    protected function referredTo()
    {
        $referredTo = [
            'CWIN Balika Peace Home', 'CWIN ESP Department', 'CWIN Self Reliance Center',
            'Rehabilitation Center', 'Long Term Shelter Homes', 'Legal Organizations',
            'CHN – Kathmandu', 'CHN – Kaski', 'CHN – Banke', 'CHN – Makwanpur', 'CHN – Morang',
            'CHN – Kailali', 'Police', 'CCWB', 'DCWB', 'VDC', 'VCPC', 'Hospital',
            'MCPC', 'NHRC', 'WCO',
        ];
        asort($referredTo);
        array_push($referredTo, 'Others');
        return $referredTo;
    }
}
