<?php

namespace App\Fightvaw\CWIN\Elements\Cases;

use App\Fightvaw\CWIN\Data\Collection;

class Cases extends Collection
{

    public function elements()
    {
        return [
            'reg_no' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Number',
                        'rules' => 'required|unique:cases,reg_no',
                    ],
                ],
            ],
            'case_created' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Registration Date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                        'rules' => 'required',
                    ],
                ],
            ],
            'case_info' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Case',
                        'displayTitle' => false,
                        'wrapper' => ['class' => 'form-group form-groups-wrapper'],
                    ],
                ],
                'counsellor' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Form Filled up by',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'registrationHelpLineDistrict' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'optionsFrom' => 'sdsa',
                            'label' => 'Registration Helpline',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'cwinProjectInvolvement' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Cwin Project Involvement',
                            'otherOptions' => true,
                        ],
                    ],
                ],
                'networkAndCoordination' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Networking And Coordination',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'govAuthoritiesInvolved' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'label' => 'Goverment Authorities Involvement',
                                'otherOptions' => true,
                            ],
                        ],
                    ],
                    'childRightAndGov' => [
                        'input' => [
                            'fieldType' => 'textarea',
                            'options' => [
                                'label' => 'Child Rights Organizations and Other Organizations Involvement',
                            ],
                        ],
                    ],
                ],
            ],
            'profile' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Child Profile',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => ' Name',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other',],
                            'expanded' => true,
                            'rules' => 'required',
                        ],
                    ],
                ],
                'nationality' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Nationality',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'ethnicity' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Ethnicity',
                            'otherOptions' => true,
                        ],
                    ],
                ],
                'education' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Education',
                            'choices' => array_combine($this->education(),
                                $this->education()),
                        ],
                    ],
                ],
                'childBackground' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Background',
                            'otherOptions' => true,
                        ],
                    ],
                ],
            ], // Profile
            'contact' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => ' Contact Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'childContact' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Child Contact Information',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'email' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Email',
                                'rules' => 'email',
                            ],
                        ],
                    ],
                    'phoneNumber' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Phone Number',
                                'rules' => 'numeric',
                            ],
                        ],
                    ],
                    'mobileNumber' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Mobile Number',
                                'rules' => 'numeric',
                            ],
                        ],
                    ],
                    'PermanentAddress' => [
                        'parent' => [
                            'fieldType' => 'subForm',
                            'options' => [
                                'label' => 'Permanent Address',
                                'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                            ],
                        ],
                       'country' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'attr' => ['id' => 'per-country'],
                                'empty_value' => 'Select country',
                                'label' => 'Country',
                            ],
                        ],
                    ],
                    'nepalAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => " ",
                                    'wrapper' => ['class' => 'nepalAddress form-group'],
                                ],
                            ],    
                          'district' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'empty_value' => 'Select district',
                                        'label' => 'District',
                                    ],
                                ],
                            ],
                            'wardNo' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'Ward Number',
                                    ],
                                ],
                            ], 
                            'vdcMunicipality' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'VDC/Municipality',
                                    ],
                                ],
                            ],
                        ],                                   
                    'otherAddress' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => ' ',
                                    'wrapper' => ['class' => 'otherAddress form-group'],
                                ],
                            ],   
                    
                        'description' => [
                            'input' => [
                                'fieldType' => 'textarea',
                                'options' => [
                                    'label' => 'Address',
                                ],
                            ],
                        ],  
                    ],
                        'PermanentContact' => [
                            'input' => [
                                'fieldType' => 'text',
                                'options' => [
                                    'label' => 'Contact Number',
                                ],
                            ],
                        ],
                    ],
                    'TemporaryAddress' => [
                        'parent' => [
                            'fieldType' => 'subForm',
                            'options' => [
                                'label' => 'Temporary Address',
                                'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                            ],
                        ],
                        'district' => [
                            'input' => [
                                'fieldType' => 'choice',
                                'options' => [
                                    'empty_value' => 'Select district',
                                    'label' => 'District',
                                ],
                            ],
                        ],
                        'wardNo' => [
                            'input' => [
                                'fieldType' => 'text',
                                'options' => [
                                    'label' => 'Ward Number',
                                ],
                            ],
                        ],
                        'vdcMunicipality' => [
                            'input' => [
                                'fieldType' => 'text',
                                'options' => [
                                    'label' => 'VDC/Municipality',
                                ],
                            ],
                        ],
                        'temporaryContact' => [
                            'input' => [
                                'fieldType' => 'text',
                                'options' => [
                                    'label' => 'Contact Number',
                                ],
                            ],
                        ],
                    ],
                ],
                'repoterPerson' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => "Reporter Contact Information",
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'name' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Name',
                            ],
                        ],
                    ],
                    'address' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Address',
                            ],
                        ],
                    ],
                    'relation' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Relation',
                            ],
                        ],
                    ],
                    'contact' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Contact Number',
                            ],
                        ],
                    ],
                ],
                'contactPerson' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => "Contact Person's Contact Information",
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'name' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Name',
                            ],
                        ],
                    ],
                    'address' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Address',
                            ],
                        ],
                    ],
                    'relation' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Relation',
                            ],
                        ],
                    ],
                    'contact' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Contact Number',
                            ],
                        ],
                    ],
                ],
            ],
            'family_info' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Parental Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'parentCondition' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Parental Condition',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'abandonedBy' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Abandoned By',
                        ],
                    ],
                ],
                'stepParents' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Step Parents',
                        ],
                    ],
                ],
                'fatherName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Father Name',
                        ],
                    ],
                ],
                'motherName' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Mother Name',
                        ],
                    ],
                ],
                'parentalBackground' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Parental Background',
                            'otherOptions' => true,
                        ],
                    ],
                ],
                'TemporaryAddress' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Parental Temporary Address',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'district' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'empty_value' => 'Select district',
                                'label' => 'District',
                            ],
                        ],
                    ],
                    'wardNo' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Ward Number',
                            ],
                        ],
                    ],
                    'vdcMunicipality' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'VDC/Municipality',
                            ],
                        ],
                    ],
                    'temporaryContact' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Contact Number',
                            ],
                        ],
                    ],
                ],
            ], //end of parentalInfo
//Perpetrator information begains
            'perpetrator' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Perpetrator Information',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'name' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Name',
                            'rules' => 'required',
                        ],
                    ],
                ],
                'age' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Age',
                        ],
                    ],
                ],
                'gender' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Gender',
                            'choices' => ['Male' => 'Male', 'Female' => 'Female',
                                'Transgender' => 'Transgender', 'Other' => 'Other',],
                            'expanded' => true,
                        ],
                    ],
                ],
                'occupation' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Occupation',
                        ],
                    ],
                ],
                'relationWithVictim' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Relation with Victim',
                        ],
                    ],
                ],
            ],
            'incident' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Incident',
                        'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                    ],
                ],
                'date' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Date',
                            'attr' => ['class' => 'input-name datetimepicker'],
                        ],
                    ],
                ],
                'location' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Location',
                        ],
                    ],
                ],
                'typeOfViolence' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Type of Incident',
                            'multiple' => true,
                        ],
                    ],
                ],
                'description' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Description of Incident',
                            'rules' => 'required',
                        ],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => false,
                        'label' => 'Details',
                    ],
                ],
                'problemScanning' => [
                    'parent' => [
                        'fieldType' => 'subForm',
                        'options' => [
                            'label' => 'Problem Scanning',
                            'wrapper' => ['class' => 'form-group parentForm contact-form dash-class'],
                        ],
                    ],
                    'name' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Name',
                                'help-text' => 'Who was child with before coming to helpline',
                            ],
                        ],
                    ],
                    'relation' => [
                        'input' => [
                            'fieldType' => 'text',
                            'options' => [
                                'label' => 'Relation',
                                'help-text' => 'Who was child with before coming to helpline',
                            ],
                        ],
                    ],
                    'sourceCase' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'label' => 'Source of the Case',
                                'otherOptions' => true,
                            ],
                        ],
                    ],
                    'mediumInfo' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'label' => 'Medium of Information',
                                'otherOptions' => true,
                            ],
                        ],
                    ],
                    'mainChildProblem' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'rules' => 'required',
                                'label' => 'Main Problem of Child',
                            ],
                        ],
                    ],
                    'associatedChildProblems' => [
                        'input' => [
                            'fieldType' => 'choice',
                            'options' => [
                                'attr' => ['id' => 'associatedChildProblems'],
                                'choices' => array_combine($this->mainChildProblem(),
                                    $this->mainChildProblem()),
                                'label' => 'Other Associated Problems',
                                'multiple' => true,
                            ],
                        ],
                    ],
                    'otherProblems' => [
                        'parent' => [
                            'fieldType' => 'subForm',
                            'options' => [
                                'displayTitle' => false,
                                'label' => 'Other Associated Problems',
                            ],
                        ],
                        'sexualAbuseCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Sexual Abuse Cases',
                                    'wrapper' => ['id' => 'Sexual-Abuse', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'typeOfSexualAbuse' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'label' => 'Type of Sexual Abuse',
                                    ],
                                ],
                            ],
                            'actionTakenAganistSexualAbuse' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'Action Taken Aganist Sexual Abuse',
                                    ],
                                ],
                            ],
                        ], // Sexual Abuse Cases
                        'deathCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Death Cases',
                                    'wrapper' => ['id' => 'Death', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'reasonBehindDeath' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'label' => 'Reason Behind Death',
                                        'otherOptions' => true,
                                    ],
                                ],
                            ],
                        ], // Death Cases
                        'childSicknessCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Child Sickness Cases',
                                    'wrapper' => ['id' => 'Child-Sickness', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'typeOfSickness' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'multiple' => true,
                                        'label' => 'Type of Sickness',
                                    ],
                                ],
                            ],
                            'typeOfSicknessOther' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'Type of Sickness Other(Specify)',
                                    ],
                                ],
                            ],
                        ], // Child Sickness Cases
                        'accidentCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Accident Cases',
                                    'wrapper' => ['id' => 'Accident', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'typeOfAccident' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'otherOptions' => true,
                                        'label' => 'Type of Accident',
                                    ],
                                ],
                            ],
                        ], // Accident Cases
                        'childMarriageCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Child Marriage Cases',
                                    'wrapper' => ['id' => 'Child-Marriage', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'typeOfChildMarriage' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'label' => 'Type of Child Marriage',
                                    ],
                                ],
                            ],
                        ], //Child Marriage Cases
                        'traffickingCases' => [
                            'parent' => [
                                'fieldType' => 'subForm',
                                'options' => [
                                    'label' => 'Trafficking Cases',
                                    'wrapper' => ['id' => 'Trafficking', 'class' => 'form-group parentForm contact-form dash-class hide-section'],
                                ],
                            ],
                            'reasonBehindTrafficking' => [
                                'input' => [
                                    'fieldType' => 'choice',
                                    'options' => [
                                        'otherOptions' => true,
                                        'label' => 'Reason Behind Trafficking',
                                    ],
                                ],
                            ],
                            'areaTrafficking' => [
                                'input' => [
                                    'fieldType' => 'textarea',
                                    'options' => [
                                        'attr' => ['Placeholder' => 'From -To(District and Place)'],
                                        'label' => 'Area of Trafficking',
                                    ],
                                ],
                            ],
                            'relationWithTrafficking' => [
                                'input' => [
                                    'fieldType' => 'text',
                                    'options' => [
                                        'label' => 'RelationShip of Victim with the Trafficker',
                                    ],
                                ],
                            ],
                            'actionTakenAganistTrafficking' => [
                                'input' => [
                                    'fieldType' => 'textarea',
                                    'options' => [
                                        'label' => 'Action Taken Aganist Trafficking',
                                    ],
                                ],
                            ],
                        ], // Trafficking Cases
                    ], // Other Associated Problems
                ], //end of Problem Scanning
            ],
        ];
    }
}
