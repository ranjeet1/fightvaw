<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class FamilyReintegration
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'district' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'District',
                        ],
                    ],
                ],
                'place' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Place',
                        ],
                    ],
                ],
                'reintegratedWith' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Reintegrated with (Name and Relationship)',
                        ],
                    ],
                ],
            ],
        ];
    }
}
