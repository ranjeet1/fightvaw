<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class EmergencyShelter
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'currentSituation' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Current Situation',
                        ],
                    ],
                ],
                'durationInShelter' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Duration of Stay in Emergency Shelter (in days)',
                        ],
                    ],
                ],
            ],
        ];
    }
}
