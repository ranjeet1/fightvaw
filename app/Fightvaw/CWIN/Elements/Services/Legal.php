<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class Legal
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'caseFiledIn' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => "Case Filed In",
                            'otherOptions' => true,
                        ],
                    ],
                ],
                'caseEndedThrough' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => "Case Ended Through",
                            'otherOptions' => true,
                        ],
                    ],
                ],
                'compensationReceived' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'expanded' => true,
                            'choices' => ['Yes' => 'Yes', 'No' => 'No'],
                            'label' => "Compensation Received",
                            'rules' => '',
                        ],
                    ],
                ],
                'otherCompensation' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Please mention any other compensation other than money',
                            'rules' => '',
                        ],
                    ],
                ],
            ],
        ];
    }
}
