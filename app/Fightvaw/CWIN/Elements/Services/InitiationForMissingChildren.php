<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class InitiationForMissingChildren
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'currentStatus' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'expanded' => true,
                            'label' => 'Current Status',
                        ],
                    ],
                ],
                'date' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Found Date',
                            'attr' => ['class' => 'input-name datetimepicker'],
                        ],
                    ],
                ],
            ],
        ];
    }
}
