<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class SponsorshipEducation
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'school' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'School',
                        ],
                    ],
                ],
                'grade' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Grade',
                        ],
                    ],
                ],
                'sponsorshipFor' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Sponsorship For',
                        ],
                    ],
                ],
                'sponsorshipForOther' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Sponsorship Other(Specify)',
                        ],
                    ],
                ],
                'cwinProject' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => "CWIN's Project",
                        ],
                    ],
                ],
            ],
        ];
    }
}
