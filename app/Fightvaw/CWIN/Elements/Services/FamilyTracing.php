<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class FamilyTracing
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'tracingStatus' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'expanded' => true,
                            'label' => 'Status',
                        ],
                    ],
                ],
            ],
        ];
    }
}
