<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class Counseling
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'typeOfCounseling' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'multiple' => true,
                            'label' => 'Type of Counseling',
                        ],
                    ],
                ],
                'typeOfCounselingOther' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Type of Counseling Other(Specify)',
                        ],
                    ],
                ],
                'resultOfCounseling' => [
                    'input' => [
                        'fieldType' => 'choice',
                        'options' => [
                            'label' => 'Result of Counseling',
                        ],
                    ],
                ],
            ],
        ];
    }
}
