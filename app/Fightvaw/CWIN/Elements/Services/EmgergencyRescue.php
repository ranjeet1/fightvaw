<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class EmgergencyRescue
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Rescue Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'rescuePlace' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Rescued From(Place)',
                        ],
                    ],
                ],
                'organizationInvolved' => [
                    'input' => [
                        'fieldType' => 'text',
                        'options' => [
                            'label' => 'Organizations Involved',
                        ],
                    ],
                ],
            ],
        ];
    }
}
