<?php

namespace App\Fightvaw\CWIN\Elements\Services;

class TreatmentInHospital
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'label' => 'Details',
                        'displayTitle' => false,
                    ],
                ],
                'treatmentFor' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Treatment For',
                        ],
                    ],
                ],
                'result' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => 'Result',
                        ],
                    ],
                ],
            ],
        ];
    }
}
