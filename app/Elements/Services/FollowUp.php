<?php

namespace App\Elements\Services;

class FollowUp
{

    public function elements()
    {
        return [
            'date' => [
                'input' => [
                    'fieldType' => 'text',
                    'options' => [
                        'label' => 'Date',
                        'rules' => 'required|date',
                        'attr' => ['class' => 'input-name datetimepicker'],
                    ],
                ],
            ],
            'details' => [
                'parent' => [
                    'fieldType' => 'subForm',
                    'options' => [
                        'displayTitle' => false,
                        'label' => 'Details',
                    ],
                ],
                'description' => [
                    'input' => [
                        'fieldType' => 'textarea',
                        'options' => [
                            'label' => "Description",
                            'rules' => 'required',
                        ],
                    ],
                ],
            ],
            'files' => [
                'input' => [
                    'fieldType' => 'file',
                    'options' => [
                        'label' => "Upload File",
                        'attr' => ['class' => 'input-name', 'multiple' => true, 'id' => 'file_upload'],
                        'rules' => '',
                    ],
                ],
            ],
        ];
    }
}
