<?php

namespace App\Elements\Services;

class File
{

    public function elements()
    {
        return [
            'files' => [
                'input' => [
                    'fieldType' => 'file',
                    'options' => [
                        'label' => "Upload File",
                        'attr' => ['class' => 'input-name', 'multiple' => true, 'id' => 'file_upload'],
                    ],
                ],
            ],
        ];
    }
}
