<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Core\Form\FormHelper;

class FormBuilderServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFormHelper();
    }

    protected function registerFormHelper()
    {
        $this->app->bindShared('laravel-form-helper', function ($app) {
            $configuration = [
                'defaults' => [
                    'wrapper_class' => 'form-group',
                    'wrapper_error_class' => 'has-error',
                    'label_class' => 'control-label',
                    'field_class' => 'form-control',
                    'error_class' => 'text-danger'
                ],
                // Templates
                'form' => 'laravel-form-builder::form',
                'text' => 'laravel-form-builder::text',
                'textarea' => 'laravel-form-builder::textarea',
                'button' => 'laravel-form-builder::button',
                'radio' => 'laravel-form-builder::radio',
                'checkbox' => 'laravel-form-builder::checkbox',
                'select' => 'laravel-form-builder::select',
                'choice' => 'laravel-form-builder::choice',
                'repeated' => 'laravel-form-builder::repeated',
                'child_form' => 'laravel-form-builder::child_form',
                'sub_form' => 'laravel-form-builder::sub_form',
                'custom_fields' => [
                // 'datetime' => 'App\Forms\Fields\Datetime'
                ]
            ];
            return new FormHelper($app['view'], $app['request'], $configuration);
        });
        $this->app->alias('laravel-form-helper', 'App\Core\Form\FormHelper');
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../Core/Form/views', 'laravel-form-builder');
        $organizationName = env('ORGANIZATION_NAME');
        $this->loadViewsFrom(__DIR__ . "/../Fightvaw/$organizationName/Views", 'Fightvaw');
    }
}
