<?php

namespace App\Helpers;

use App;
use \Illuminate\Contracts\Logging\Log;

class FileHelper
{

    public function storeFiles($object, $files)
    {
        $uploadDestination = public_path().'/uploads/files/';
        foreach ($files as $file) {
            if ($file) {
                $fileName       = time().$file->getClientOriginalName();
                $upload_success = $file->move($uploadDestination, $fileName);
                if ($upload_success) {
                    try {
                        $file                  = App::make('App\Models\File');
                        $data['file_name']     = $file->getClientOriginalName();
                        $data['file_path']     = '/uploads/files/'.$fileName;
                        $data['fileable_id']   = $object->id;
                        $data['fileable_key']  = $object->getFileKey();
                        $data['fileable_type'] = get_class($object);
                        $file->saveData($data);
                    } catch (\ErrorException $e) {
                        Log::error($e->getMessage());
                    }
                }
            }
        }

        return true;
    }

    public function moveFiles($object, $files)
    {
        foreach ($files as $fileName) {
            $oldDestination = public_path().'/uploads/temp/'.md5($fileName)."_".$fileName;
            $filePath       = '/uploads/files/';
            $newDestination = public_path().$filePath;
            try {
                move_uploaded_file($oldDestination, $newDestination);
                $file                  = App::make('App\Models\File');
                $data['file_name']     = $fileName;
                $data['file_path']     = $filePath;
                $data['fileable_id']   = $object->id;
                $data['fileable_key']  = $object->getFileKey();
                $data['fileable_type'] = get_class($object);
                $file->saveData($data);
            } catch (\ErrorException $e) {
                Log::error($e->getMessage());
            }
        }

        return true;
    }
}