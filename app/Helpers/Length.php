<?php

namespace App\Helpers;

class Length
{

    public static function getStringSplit($string)
    {
        $filterString          = strip_tags($string);
        $splitString['first']  = $filterString;
        $splitString['second'] = '';
        if (strlen($filterString) > 500) {
            // truncate string
            $stringCut             = substr($filterString, 0, 500);
            // make sure it ends in a word so assassinate doesn't become ass...
            $splitString['first']  = substr($stringCut, 0,
                    strrpos($stringCut, ' ')).'<span class="hide-dot"> ...</span> <span class="more">View More...</span>';
            $splitString['second'] = '<span class="remain-description">'.substr($filterString,
                    strrpos($stringCut, ' ')).'</span><span class="less">View less...</span>';
        }

        return $splitString;
    }
}
