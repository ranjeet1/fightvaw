<?php

namespace App\Helpers;

use Illuminate\Support\Facades\View;

class ViewHelper
{

    public static function viewFileExist($filePath)
    {
        if (!View::exists($filePath)) {
            $filePath = str_replace("Fightvaw::", "", $filePath);
        }

        return $filePath;
    }

    public static function getLabel($data)
    {
        if (isset($data['input']['options']['label'])) {
            return $data['input']['options']['label'];
        }

        return 'No label';
    }

    public static function getValue($data, $value)
    {
        if (isset($data['input']['options']['multiple'])) {
            if (is_array($value)) {
                return implode(", ", $value);
            }
        }
        if (str_contains('Date', ViewHelper::getLabel($data)) && $value) {
            return ViewHelper::formatDate($value);
        }

        return $value;
    }

    public static function formatDate($date)
    {
        return date('j F Y', strtotime($date));
    }
}
