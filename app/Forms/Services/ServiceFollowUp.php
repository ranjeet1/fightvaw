<?php

namespace App\Forms\Services;

use App\Core\Form\Form;

class ServiceFollowUp extends Form
{

    public function buildForm()
    {
        $this->add('date', 'text',
                [
                'attr' => ['class' => 'input-name datetimepicker'],
                'label' => 'Date',
                'rules' => 'required',
            ])
            ->add('description', 'textarea',
                [
                'attr' => ['class' => 'input-name'],
                'label' => 'Description',
                'rules' => 'required',
            ])
            ->add('Save', 'submit',
                [
                'attr' => ['class' => 'submit-button submit-dash'],
            ])
            ->add('Cancel', 'button',
                [
                'attr' => ['class' => 'submit-button submit-dash', 'onclick' => "redirectFunction('".route('cases.show',
                        $this->model->service->case_id)."')"],
        ]);
    }
}
