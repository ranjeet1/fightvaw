<?php

namespace App\Http\Controllers\Services;

use App\Core\Organization\Organization;
use App;
use App\Helpers\JsonHelper;

use App\Http\Requests\Service\CreateServiceRequest;
use App\Models\Service\Service;
use Illuminate\Routing\Controller;
use App\Models\Cases\Cases;
use App\Helpers\FileHelper;
use App\Models\File as ServiceFile;
use Illuminate\Support\Facades\Response;

/**
 * Class ServiceController
 * @package App\Http\Controllers\Services
 */
class ServiceController extends Controller
{

    public function __construct(Organization $organization, Cases $case, Service $service)
    {
        $this->organization = $organization;
        $this->formBuilderObj = App::make('FormBuilder');
        $this->case = $case;
        $this->service = $service;
        $this->fileHelper = new FileHelper;
        $this->serviceFile = new ServiceFile();
    }

    public function create($caseId, $type)
    {
        $this->service->case_id = $caseId;
        $this->service->service = $type;
        $this->organization->setSection($type);
        $this->service->title   = $this->organization->getServiceTitle();
        $formPath               = $this->organization->getFormPath();
        $form                   = $this->formBuilderObj->create($formPath,
            [
            'method' => 'POST',
            'url' => route('cases.services.store', $caseId),
            'model' => $this->service,
            'data' => ['caseId' => $caseId, 'organizationObj' => $this->organization],
            'class' => 'form-class file-upload-form ajax-validate',
        ]);
        $form->add('service', 'hidden', ['default_value' => $this->service->service]);
        $service = $this->service;
        //dd($form);
        return view("services.create", compact('form', 'service'));
    }

    public function store(CreateServiceRequest $request, $caseId)
    {
        $service = $this->service;
        $service->case_id = $caseId;
        $service->user_id = \Auth::user()->id;
        $service->fill($request->only('service', 'date'));
        $service->details = JsonHelper::JsonEncode($request->get('details'));
        $service->save();
        $this->organization->setSection($service->service);
        $service->title = $this->organization->getServiceTitle();
        $service->activityLogForCreate();
        if ($request->get('files')) {
            $this->fileHelper->moveFiles($service, $request->get('files'));
        }
        if ($service->service == "FollowUp") {
            $flashMessage = "Follow up of Case #" . $service->cases->reg_no . " is added successfully";
        } else {
            $flashMessage = $service->title." is added successfully to Case #".$service->cases->reg_no;
        }
        \Flash::success($flashMessage);

        return Response::json([
                'url' => route('cases.show', $caseId),
        ]);
    }

    public function show($caseId, $type)
    {
        $service = $this->service->whereCaseId($caseId)->whereService($type)->firstOrFail();
        $service->details = JsonHelper::JsonDecode($service->details);
        return view("services." . $type . "_show", compact('service'));
    }

    public function edit($serviceId)
    {
        $service          = $this->service->findOrFail($serviceId);
        $service->details = JsonHelper::JsonDecode($service->details);
        $this->organization->setSection($service->service);
        $formPath         = $this->organization->getFormPath();
        $service->user_id = \Auth::user()->id;
        $this->organization->setSection($service->service);
        $service->title   = $this->organization->getServiceTitle();
        $form             = $this->formBuilderObj->create($formPath,
            [
            'method' => 'PUT',
            'url' => route('services.update', $serviceId),
            'model' => $service,
            'data' => ['caseId' => $service->case_id, 'organizationObj' => $this->organization],
            'class' => 'form-class file-upload-form ajax-validate',
        ]);
        $form->add('service', 'hidden', ['default_value' => $service->service]);

        return view("services.create", compact('form', 'service'));
    }

    public function update(CreateServiceRequest $request, $serviceId)
    {
        $service = $this->service->findOrFail($serviceId);
        $service->fill($request->only('service', 'date'));
        $service->details = JsonHelper::JsonEncode($request->get('details'));
        $this->updateFiles($request->get('uploaded_files'), $service); 
        if ($request->get('files')) {
            $this->fileHelper->moveFiles($service, $request->get('files'));
        }
        $service->save();
        $this->organization->setSection($service->service);
        $service->title = $this->organization->getServiceTitle();
        $service->activityLogForUpdate();
        if ($service->service == "FollowUp") {
            $flashMessage = "Follow up of Case #" . $service->cases->reg_no . " is edited successfully";
        } else {
            $flashMessage = $service->title . " is edited successfully of Case #".$service->cases->reg_no;
            \Flash::success($flashMessage);
        }

        return Response::json([
                'url' => route('cases.show', $service->case_id),
        ]);
    }

    public function delete($serviceId)
    {
        $service = $this->service;
        if (\Auth::user()->can(['delete-own-services'])) {
            $service = $service->where('user_id', '=', \Auth::user()->id);
        }
        $service = $service->findOrFail($serviceId);
        $service->deleteService();        
        $this->organization->setSection($service->service);
        $service->title = $this->organization->getServiceTitle();
        $service->activityLogForDelete();
        $flashMessage = "$service->title is deleted successfully of Case #".$service->cases->reg_no;
        \Flash::success($flashMessage);

        return redirect()->route('cases.show', $service->case_id);
    }

    private function updateFiles($uploadedFiles = [], $service)
    {
        $files = $service->files;
        if (empty($uploadedFiles)) {
            foreach ($files as $file) {
                $this->removeFiles($file);
            }

            return true;
        }
        foreach ($files as $file) {
            if (!in_array($file->file_name, $uploadedFiles)) {
                $this->removeFiles($file);
            }
        }
    }

    private function removeFiles($file)
    {
        \File::delete(public_path().$file->file_path);
        $this->serviceFile->find($file->id)->delete();
    }
}
