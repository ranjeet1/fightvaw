<?php

namespace App\Http\Controllers\Services;

use App\Core\Organization\Organization;
use App\Http\Requests\Service\FollowUpRequest;
use App\Models\Service\Service;
use App\Models\Service\ServiceFollowUp;
use Illuminate\Routing\Controller;
use App\Helpers\FileHelper;
use App\Core\Form\FormBuilder;
use App;

class ServiceFollowUpController extends Controller
{

    public function __construct(Organization $organization, Service $service,
                                ServiceFollowUp $followUp)
    {
        $this->organization = $organization;
        $this->service      = $service;
        $this->followup     = $followUp;
        $this->fileHelper   = new FileHelper();
    }

    public function create($serviceId)
    {
        $this->followup->service_id     = $serviceId;
        $formBuilder                    = App::make('FormBuilder');
        $form                           = $formBuilder->create('App\Forms\Services\ServiceFollowUp',
            [
            'method' => 'POST',
            'url' => route('services.followup.store', $serviceId),
            'model' => $this->followup,
            'data' => ['organizationObj' => $this->organization],
            'class' => 'form-class',
        ]);
        $followup                       = $this->followup;
        $this->organization->setSection($this->followup->service->service);
        $this->followup->service->title = $this->organization->getServiceTitle();

        return view('services.follow_up_create', compact('form', 'followup'));
    }

    public function store(FollowUpRequest $request, $serviceId)
    {
        $followup             = $this->followup;
        $followup->fill($request->only('description', 'date'));
        $followup->service_id = $serviceId;
        $followup->user_id    = \Auth::user()->id;
        $followup->save();
        if ($request->file('files')) {
            $this->fileHelper->storeFiles($followup, $request->file('files'));
        }
        $this->saveToServices($followup);
        $this->organization->setSection($this->followup->service->service);
        $followup->service->title = $this->organization->getServiceTitle();
        $followup->activityLogForCreate();
        \Flash::success("Follow up for ".$followup->service->title." of Case #".$followup->service->cases->reg_no."  is added successfully.");

        return redirect()->route('cases.show', $followup->service->case_id);
    }

    public function edit($serviceId, $followupId)
    {
        $followup    = $this->followup->findOrFail($followupId);
        $formBuilder = new FormBuilder();
        $form        = $formBuilder->create('App\Forms\Services\ServiceFollowUp',
            [
            'method' => 'PUT',
            'url' => route('services.followup.update',
                [$serviceId, $followup->id]),
            'model' => $followup,
            'data' => ['organizationObj' => $this->organization],
            'class' => 'form-class',
        ]);

        return view('services.follow_up_create', compact('form', 'followup'));
    }

    public function update(FollowUpRequest $request, $serviceId, $followupId)
    {
        $followup = $this->followup->findOrFail($followupId);
        $followup->fill($request->only('description', 'date'));
        $followup->save();
        if ($request->file('files')) {
            $this->fileHelper->storeFiles($this->followup,
                $request->file('files'));
        }
        $this->updateToServices($followup);
        $this->organization->setSection($followup->service->service);
        $followup->service->title = $this->organization->getServiceTitle();
        $followup->activityLogForUpdate();
        \Flash::success("Follow up for " .$followup->service->title. " of Case #" . $followup->service->cases->reg_no . "  is edited successfully.");

        return redirect()->route('cases.show', $followup->service->case_id);
    }

    private function saveToServices($followup)
    {
        $this->service->follow_up_id = $followup->id;
        $this->service->date         = $followup->date;
        $this->service->case_id      = $followup->service->case_id;
        $this->service->user_id      = \Auth::user()->id;
        $this->service->service      = "service_followup";
        $this->service->save();

        return true;
    }

    private function updateToServices($followup)
    {
        $this->service       = $this->service->findOrFail($followup->service_id);
        $this->service->date = $followup->date;
        $this->service->save();

        return true;
    }

    public function delete($followUpId)
    {
        $service = $this->service->findOrFail($serviceId);
        foreach ($service->files as $servicefiles) {
            \File::delete($servicefiles->file_path);
        }
        foreach ($service->followups as $followup) {
            foreach ($followup->files as $followUpFiles) {
                \File::delete($followUpFiles->file_path);
            }
        }
        $service->files()->delete();
        $service->followups()->delete();
        $service->followup()->delete();
        $service->delete();
        \Flash::success("Service deleted successfully");

        return redirect()->route('cases.show', $service->case_id);
    }
}
