<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\RoleRequest;
use App\Models\Users\Role;

class RoleController extends Controller
{

    public function __construct(Role $role)
    {
        $this->role  = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::latest('created_at')->paginate(5);
        return view('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('role.create')->with(['role' => $this->role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RoleRequest $request)
    {
        //dd($request->all());
        $role = $this->role->create($request->all());
        $role->perms()->attach($request->get('permission'));
        \Flash::success("Role is added successfully.");

        return redirect()->route('role.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->role->findOrFail($id);

        return view('role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(RoleRequest $request, $id)
    {
        $role = $this->role->findOrFail($id);
        $role->where('id', $id)->update($request->except('_method','_token','permission'));
        /* permission attach alias */
        $role->perms()->detach();
        $role->perms()->attach($request->get('permission'));
        \Flash::success("Role is edited successfully.");
        
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {dd($id);
        $role = $this->role->findOrFail($id);
        //$role->perms()->detach();
        $this->role->destroy($id);
        \Flash::success("Deleted successfully.");

        return redirect()->route('role.index');
    }
}