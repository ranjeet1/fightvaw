<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Users\Users;
use App\Http\Requests\Users\UsersRequest;
use App\Models\Users\Role;

class UsersController extends Controller
{

    public function __construct(Users $users, Role $role)
    {
        $this->users = $users;
        $this->role  = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = Users::latest('created_at')->paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->users->is_active = 'true';
        return view('users.create')->with(['users' => $this->users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UsersRequest $request)
    {
        $this->users->fill($request->except('password'));
        $this->users->password = bcrypt($request->get('password'));
        $this->users->save();
        $this->users->roles()->attach($request->get('role'));
        \Flash::success("User is added successfully.");

        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->users->findOrFail($id);
        return view('users.edit', compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UsersRequest $request, $id)
    {   
        $users = $this->users->findOrFail($id);
        $users->fill($request->except('_method','_token','role','password'));
        $password = bcrypt($request->get('password'));
        if($users->password != $password)
        {
            $users->password = $password;
        }
        if(!$request->is_active){
            $users->is_active = False;
        }
        $users->save(); 
        /* role attach alias */
        $users->roles()->detach();
        $users->roles()->attach($request->get('role'));
        \Flash::success("User is edited successfully.");
        
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->users->roles()->detach();
        $this->users->destroy($id);
        \Flash::success("Deleted successfully.");

        return redirect()->route('users.index');
    }
}