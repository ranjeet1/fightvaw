<?php namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;

class FileController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | File Controller
    |--------------------------------------------------------------------------
    |
    | This controller manipulates all the file request made on the application
    |
    */

    public function upload(FileRequest $request)
    {
        $uploadDestination = public_path().'/uploads/temp/';

        $file = $request->file('Filedata')[0];

        $filename = preg_replace('/\s+/', '', $file->getClientOriginalName());


        $fileName = md5($filename)."_".$filename;

        if ($file->move($uploadDestination, $fileName)) {
            return "success";
        }
        return "fail";
    }

    public function remove(FileRequest $request)
    {
        if (\File::delete(public_path().'/uploads/temp/'.md5($request->get('name'))."_".$request->get('name'))) {
            return "success";
        }
        return "fail";
    }
}
