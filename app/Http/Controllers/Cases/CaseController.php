<?php

namespace App\Http\Controllers\Cases;

use App\Core\Organization\Organization;
use App\Helpers\JsonHelper;
use App;
use App\Http\Requests\Cases\CaseFileRequest;
use App\Models\Cases\CaseFile;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Cases\Cases;
use App\Http\Requests\Cases\CreateCaseRequest;
use Illuminate\Support\Facades\Response;

class CaseController extends Controller
{
    public function __construct(Organization $organization, Cases $case, CaseFile $files)
    {
        $this->organization = $organization;
        $this->organization->setSection('Cases');
        $this->jsonCaseFields = $this->organization->getJsonCaseFields();
        $this->case = $case;
        $this->files = $files;
    }

    public function index(Request $request)
    {
        $cases = \DB::table('cases')
            ->select('cases.*')
            ->leftjoin('services', 'cases.id', '=', 'services.case_id')
            ->where(function ($query) use ($request) {
                $searchKeys = $this->organization->getSearchFields();
                $additionalKeys = $this->organization->getAddSearchFields();
                $search = $request->get('search');
                foreach ($searchKeys as $searchKey => $searchValue) {
                    $input = $request->get($searchKey);
                    if ($input) {
//                        dd($input);
                        if (gettype($input) != "array") {

                            $input = [$input];
                        }
                        if ($searchValue['type'] == "json") {

                            $query->where(function ($query) use ($input, $searchValue) {

                                foreach ($input as $item) {

                                    if(is_array($searchValue['path']))
                                    {
                                        foreach($searchValue['path'] as $path)
                                        {
                                            foreach($searchValue['field'] as $field){
                                                $query->orWhereRaw($field."::json#>>"." ' ".$path." ' ilike "."'%".$item."%'");
                                            }
                                        }

                                    }else{
                                        $query->orWhereRaw($searchValue['field']."::json#>>"." ' ".$searchValue['path']." ' ilike "."'%".$item."%'");
                                    }

                                }
                            });
                        } else {
                            $query->where(function ($query) use ($input, $searchValue) {
                                foreach ($input as $item) {
                                    $query->orWhereRaw($searchValue['field']." ilike "."'%".$item."%'");
                                }
                            });
                        }
                    }
                }
                if ($search) {

                    $query->where(function ($query) use ($search, $additionalKeys, $searchKeys) {
                        foreach ($additionalKeys as $additionalKey => $additionalValue) {
                            if ($additionalValue['type'] == "json") {
                                $query->orWhereRaw($additionalValue['field']."::json#>>"."'".$additionalValue['path']."' ilike '%".$search."%'");
                            } else {
                                $query->orWhereRaw($additionalValue['field']." ilike "."'%".$search."%'");
                            }
                        }
                        foreach ($searchKeys as $searchKeys => $searchValue) {

                            if ($searchValue['type'] == "json") {
                                if(is_array($searchValue['path'])) {
                                    foreach ($searchValue['path'] as $path) {

                                        foreach ($searchValue['field'] as $field) {
                                            $query->orWhereRaw($field . "::json#>>" . "'" . $path . "' ilike '%" . $search . "%'");
                                        }
                                    }
                                }else{
                                    $query->orWhereRaw($searchValue['field'] . "::json#>>" . "'" . $searchValue['path']  . "' ilike '%" . $search . "%'");
                                }
                            } else {
                                $query->orWhereRaw($searchValue['field']." ilike "."'%".$search."%'");
                            }
                        }
                    });

                }
            });
        $from = "2000/12/12 00:00:00";
        $to = date("Y/m/d")." 23:59:59";
        if ($request->get('from')) {
            $from = $request->get('from')." 00:00:00";
        }
        if ($request->get('to')) {
            $to = $request->get('to')." 23:59:59";
        }

        $cases->whereRaw("case_created between '".$from."'"." and '".$to."'");

        if ((\Auth::user()->can(['manage-own-cases'])) && (\Auth::user()->can(['view-all-cases'])) == null  && (\Auth::user()->can(['manage-all-cases'])) == null) {

            $cases->whereRaw("cases.user_id = ".\Auth::user()->id);
        }

        $cases->groupBy('cases.id');
        $cases->orderBy('case_created', 'DESC');
        $cases = $cases->get();

        $inputs        = array_filter($request->all());
        $searchOptions = $this->organization->getSearchFields();
        $data = App::make('App\Core\Data\DataHandler');

        return view('cases.index', compact('cases', 'inputs', 'searchOptions', 'data'));
    }

    public function create()
    {
        $formPath = $this->organization->getFormPath();
        $formBuilder = App::make('FormBuilder');
        $form        = $formBuilder->create($formPath,
            [
            'method' => 'POST',
            'url' => route('cases.store'),
            'model' => $this->case,
            'class' => 'form-class ajax-validate',
            'data' => ['caseId' => $this->case->id, 'organizationObj' => $this->organization],
        ]);

        return view('cases.create', compact('form'));
    }

    public function store(CreateCaseRequest $request)
    {
        $this->case->fill($request->only('reg_no', 'status', 'case_created'));
        $this->case = JsonHelper::Encode($this->case, $request, $this->jsonCaseFields);
        $this->case->code = $this->getCaseCode();
        $this->case->user_id = \Auth::user()->id;
        $this->case->public_key = substr(md5(time()), 1, 15);
        $this->case->save();
        $this->case->activityLogForCreate();
        \Flash::success("Case #".$this->case->reg_no." is added successfully.");

        return Response::json([
                'url' => route('cases.show', $this->case->id),
        ]);
    }

    public function show($id)
    {
        $case = $this->case->with(array('services' => function ($query) {
                    if (\Auth::user()->can(['manage-own-services']) && \Auth::user()->can(['manage-all-services']) == null && \Auth::user()->can(['view-all-services']) == null) {
                        $query->whereRaw("services.user_id = ".\Auth::user()->id);
                    }
                    $query->orderBy('date', 'desc');
                },
                    'services.followups' => function ($query) {
                    $query->orderBy('date', 'desc');
                },
                ))->where('cases.id', '=', $id);

        $case             = $case->firstOrFail();
        $intakeGenerator  = App::make('App\Core\Views\IntakeGenerator');
        $serviceGenerator = App::make('App\Core\Views\ServiceGenerator');
        $organizationObj = $this->organization;
        return view('cases.show', compact('case', 'organizationObj', 'intakeGenerator', 'serviceGenerator'));
    }

    public function edit($caseId)
    {
        $case = $this->case->findOrFail($caseId);
        $case = JsonHelper::Decode($case, $this->jsonCaseFields);
        $formPath = $this->organization->getFormPath();
        $formBuilder = App::make('FormBuilder');
        $form        = $formBuilder->create($formPath,
            [
            'method' => 'PUT',
            'url' => route('cases.update', $caseId),
            'model' => $case,
            'class' => 'form-class ajax-validate',
            'data' => ['caseId' => $case->id, 'organizationObj' => $this->organization],
        ]);
        $form->add('case_id', 'hidden', ['default_value' => $caseId]);

        return view('cases.edit', compact('form', 'case'));
    }

    public function update(CreateCaseRequest $request, $caseId)
    {
        $case = $this->case->findOrFail($caseId);
        $case->fill($request->only('reg_no', 'status', 'case_created'));
        $case = JsonHelper::Encode($case, $request, $this->jsonCaseFields);
        $this->case->user_id = \Auth::user()->id;
        $case->save();
        $case->activityLogForUpdate();
        \Flash::success("Case #$case->reg_no is edited successfully.");

        return Response::json([
                'url' => route('cases.show', $caseId),
        ]);
    }

    public function addFile($caseId)
    {
        $caseFile = new CaseFile();
        $case = $this->case->findOrFail($caseId);
        $form = \FormBuilder::create('App\Forms\Cases\FileForm', [
                    'method' => 'POST',
                    'url' => route('cases.file.store', $caseId),
                    'model' => $caseFile,
                    'class' => 'form-class',
        ]);

        return view('cases.create_file', compact('form', 'case'));
    }

    public function postFile(CaseFileRequest $request, $caseId)
    {
        $uploadDestination = public_path().'/uploads/cases/';
        $file = $request->file('file_name');

        // TO DO: Multiple file uploads using foreach

        $upload_success = $file->move($uploadDestination, $file->getClientOriginalName());
        if ($upload_success) {
            $caseFile = new CaseFile();
            $caseFile->file_name = $file->getClientOriginalName();
            $caseFile->file_path = '/uploads/cases/'.$file->getClientOriginalName();
            $caseFile->case_id = $caseId;
            $caseFile->description = $request->get('description');
            $caseFile->save();
            \Flash::success('File is successfully uploaded');

            return redirect()->route('cases.show', $caseId);
        }
        \Flash::error('Error in uploading file');

        return redirect()->back();
    }

    public function files($caseId)
    {
        $files = $this->files->whereCaseId($caseId)->get();

        return view('cases.files', compact('files'));
    }

    private function getCaseCode()
    {
        $count = Cases::count();
        $count += 1000;
        $caseCode = $this->organization->getRegistrationNumber().date('Y').$count;

        return $caseCode;
    }

    public function publicView($caseId)
    {
        $case = $this->case->whereId($caseId)->wherePublicKey(\Input::get('key'))->first();
        if ($case) {
            return view('cases.show', compact('case'));
        } else {
            return "Bad Key";
        }
    }

    public function delete($caseId)
    {
        $case = $this->case;

        $case        = $case->findOrFail($caseId);
        $case = $this->case->findOrFail($caseId);
        $case->deleteCase();
        \Flash::success('Case is deleted successfully');

        return redirect()->route('cases.index');
    }

    public function restore($caseId)
    {
        $case = $this->case->withTrashed()->whereId($caseId)->first();
        $case->restore();
        \Flash::success('Case is successfully restored');

        return redirect()->route('cases.show', $caseId);
    }
}
