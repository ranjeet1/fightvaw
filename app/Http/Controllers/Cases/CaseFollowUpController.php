<?php namespace App\Http\Controllers\Cases;

use App\Http\Requests\Cases\FollowUpRequest;
use App\Models\Cases\Cases;
use Illuminate\Routing\Controller;
use App\Models\Cases\CaseFollowUp;



class CaseFollowUpController extends Controller {

    protected $caseFollowUp;

    protected $case;

    public function __construct(CaseFollowUp $caseFollowUp, Cases $case){
        $this->caseFollowUp = $caseFollowUp;
        $this->case = $case;
    }

    public function create($caseId){
        $case = $this->case->findOrFail($caseId);
        $form = \FormBuilder::create('App\Forms\Cases\FollowUpForm', [
            'method' => 'POST',
            'url' => route('cases.followup.store',$caseId),
            'model' =>$this->caseFollowUp
        ]);

        return view('cases.create_follow_up', compact('form','case'));
    }

    public function edit(){
        
    }

    public function store(FollowUpRequest $request, $caseId){
        $this->caseFollowUp->fill($request->only('description'));
        $this->caseFollowUp->case_id = $caseId;
        $this->caseFollowUp->user_id = \Auth::user()->id;
        $this->caseFollowUp->save();
        \Flash::success("Case Followed up successfully");

        return redirect()->route('cases.show',$caseId);
    }

    public function update(){

    }

}