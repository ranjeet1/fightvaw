<?php

namespace App\Http\Controllers\Log;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Regulus\ActivityLog\Models\Activity;

class ActivityLogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $from = "2000-12-12 00:00:00";
        $to   = date("Y-m-d")." 23:59:59";
        if ($request->get('from')) {
            $from = $request->get('from')." 00:00:00";
        }
        if ($request->get('to')) {
            $to = $request->get('to')." 23:59:59";
        }
        $logs = Activity::whereRaw("created_at between '".$from."'"." and '".$to."'")
            ->latest('created_at')
            ->paginate(10);

        return view('log.index', compact('logs'));
    }
}
