<?php

namespace App\Http\Requests\Service;

use App\Core\Requests\FormRequests;
use Illuminate\Foundation\Http\FormRequest;

class CreateServiceRequest extends FormRequest
{
    protected $formRequests;

    public function rules()
    {
        $this->setFormRequestType();
        $rules = $this->formRequests->getRules();

        return $rules;
    }

    public function getAtrributeLabel()
    {
        $this->setFormRequestType();
        $attributesLabel = $this->formRequests->getAttributesLabel();

        return $attributesLabel;
    }

    public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else
        return Response::make('Permission denied foo!', 403);
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->setAttributeNames($this->getAtrributeLabel());

        return $validator;
    }

    public function setFormRequestType()
    {
        $type               = $this->get('service');
        $this->formRequests = new FormRequests($type);
    }
}
