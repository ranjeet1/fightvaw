<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => "required|email|max:255|unique:users,email",
            'password' => 'required|min:6',
            'phone_number' => 'required|numeric',
            'address' => 'required',
            'role' => 'required'
        ];
        if ($this->get('id')) {
            $rules['email'] .= ','.$this->get('id');
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}