<?php namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest {


   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255|unique:roles,name',
            'description' => 'required',
            'permission' => 'required',
        ];
        if ($this->get('id')) {
            $rules['name'] .= ','.$this->get('id');
        }
        return $rules;
    }

    /**
     * Define attributes label
     * @return array
     */
    public function getAtrributeLabel()
    {
        return [
            'name' => 'name of the role',
        ];
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->setAttributeNames($this->getAtrributeLabel());

        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
