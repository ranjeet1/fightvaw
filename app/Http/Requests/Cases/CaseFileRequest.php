<?php namespace App\Http\Requests\Cases;

use Illuminate\Foundation\Http\FormRequest;

class CaseFileRequest extends FormRequest {

    public function rules(){
        return [
            'file_name' => 'required'
        ];
    }
    
    public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        // Optionally, send a custom response on authorize failure 
        // (default is to just redirect to initial page with errors)
        // 
        // Can return a response, a view, a redirect, or whatever else
        return Response::make('Permission denied foo!', 403);
    }
}
