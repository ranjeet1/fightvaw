<?php

namespace App\Http\Requests\Cases;

use App\Core\Requests\FormRequests;
use Illuminate\Foundation\Http\FormRequest;

class CreateCaseRequest extends FormRequest
{
    protected $formRequests;

    public function __construct()
    {
        $this->formRequests = new FormRequests('Cases');
    }

    public function rules()
    {
        $rules = $this->formRequests->getRules();
        if ($this->get('case_id')) {
            $rules['reg_no'] .= ','.$this->get('case_id');
        }

        return $rules;
    }

    public function getAtrributeLabel()
    {
        $attributesLabel = $this->formRequests->getAttributesLabel();

        return $attributesLabel;
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->setAttributeNames($this->getAtrributeLabel());

        return $validator;
    }

    public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else
        return Response::make('Permission denied foo!', 403);
    }
}
