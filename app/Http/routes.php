<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


//$router->resource('services.followup', 'Services\ServiceFollowUpController');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


$router->get('/', function () {

    return redirect('/auth/login');
});

$router->get('/home', function () {
    return redirect()->route('cases.index');
});


$router->get('download/{filename}',function($filename){
    $filePath = public_path().'/uploads/temp/'.$filename;
    if (file_exists($filePath))
    {
        // Send Download
        return Response::download($filePath, $filename, [
            'Content-Length: '. filesize($filePath)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
});
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);



$router->group(['middleware' => ['auth', 'acl']],
    function ($router) {

        $router->group(['namespace' => 'Users', 'permissions' => ['manage-all', 'manage-users']],
        function ($router) {
        $router->resource('users', 'UsersController');
    });

    $router->group(['namespace' => 'Users', 'permissions' => ['manage-all', 'manage-roles']],
        function ($router) {
        $router->resource('role', 'RoleController');
    });

    $router->group(['namespace' => 'Cases'],
        function ($router) {
        $router->get('cases/delete/{caseId}',
            [
            'as' => 'cases.delete',
            'uses' => 'CaseController@delete',
        ]);
        $router->resource('cases', 'CaseController');
    });

    $router->group(['namespace' => 'Services'],
        function ($router) {
        $router->get('/cases/{caseId}/service/{type}/create',
            [
            'as' => 'services.create',
            'uses' => 'ServiceController@create',
        ]);
        $router->get('/cases/{caseId}/service/{type}',
            [
            'as' => 'services.show',
            'uses' => 'ServiceController@show',
        ]);
        $router->get('/service/{serviceId}/edit',
            [
            'as' => 'services.edit',
            'uses' => 'ServiceController@edit',
        ]);
        $router->put('/service/{serviceId}',
            [
            'as' => 'services.update',
            'uses' => 'ServiceController@update',
        ]);
        $router->resource('cases.services', 'ServiceController');
         $router->resource('services.followup', 'ServiceFollowUpController');


            $router->get('service/{serviceId}/delete',
            [
            'as' => 'services.delete',
            'uses' => 'ServiceController@delete',
        ]);
    });
    $router->group(['namespace' => 'Log', 'permissions' => ['manage-all', 'manage-activity-log']],
        function($router) {
        $router->resource('log', 'ActivityLogController');
    });


    $router->post('file/uploads',
        [
        'as' => 'file.uploads',
        'uses' => 'FileController@upload',
    ]);
    $router->post('file/remove',
        [
        'as' => 'file.remove',
        'uses' => 'FileController@remove',
    ]);

//    $router->get('download/{filename}',[
//        'uses' => 'FileController@downloadFile'
//    ]);

});


