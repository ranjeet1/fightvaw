<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class CheckPermission implements Middleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->userHasAccessTo($request)) {
            view()->share('currentUser', $request->user());

            return $next($request);
        }

        return redirect('/auth/login');
    }
    /*
      |--------------------------------------------------------------------------
      | Additional helper methods for the handle method
      |--------------------------------------------------------------------------
     */

    /**
     * Checks if user has access to this requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean                  true if has permission otherwise false
     */
    protected function userHasAccessTo($request)
    {
        return $this->hasPermission($request);
    }

    /**
     * hasPermission Check if user has requested route permimssion
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean                  true if has permission otherwise false
     */
    protected function hasPermission($request)
    {
        $required = $this->requiredPermission($request);

        return !$this->forbiddenRoute($request) && $request->user()->can($required);
    }

    /**
     * Extract required permission from requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @return String                   permission_slug connected to the Route
     */
    protected function requiredPermission($request)
    {
        $permissions = $this->getControllerAction($request);

        return isset($permissions) ? $permissions : [];
    }

    /**
     * Extract controller@action
     * Extract permissions list
     *
     * @param  \Illuminate\Http\Request $request
     * @return array                    $permissions
     */
    protected function getControllerAction($request)
    {
        $action = $request->route()->getAction();
        if (isset($action['permissions'])) {
            return $action['permissions'];
        }
        $controllerAction = str_replace($action['namespace']."\\", '',
            $action['controller']);
        $permissions      = $this->getPermissions($controllerAction);

        return $permissions;
    }

    /**
     * Extract permission list based on controller and action
     *
     * @param  string $permissions
     * @return array  $permissionList
     */
    protected function getPermissions($controllerAction)
    {
        $permissionList   = [];
        $controllerAction = $this->getClusterAction($controllerAction);
        switch ($controllerAction) {
            case 'Cases-show';
                $permissionList = ['manage-all-cases',
                    'manage-own-cases', 'view-all-cases', 'manage-all-services',
                    'manage-own-services',
                    'view-all-services', 'delete-all-cases', 'delete-own-cases',
                    'delete-all-services',
                    'delete-own-services',
                    'manage-all',];
                break;

            case 'Cases-Add-Update':
                $permissionList = ['manage-all-cases', 'manage-own-cases', 'manage-all'];
                break;

            case 'CaseController@delete':
                $permissionList = ['manage-all', 'delete-all-cases', 'delete-own-cases'];
                break;

            case 'CaseController@delete':
                $permissionList = ['manage-all', 'delete-all-cases', 'delete-own-cases'];
                break;

            case 'Services-Add-Update':
                $permissionList = ['manage-all-services', 'manage-own-services',
                    'manage-all',];
                break;

            case 'ServiceController@delete':
                $permissionList = ['manage-all', 'delete-all-services', 'delete-own-services'];
                break;

            case 'ServiceController@index':
                $permissionList = ['manage-all', 'delete-all-services', 'delete-own-services',
                    'manage-all-services', 'manage-own-services', 'view-all-services',];
                break;

            default:
                break;
        }

        return $permissionList;
    }

    /**
     * Cluster controller action
     *
     * @param  string $controllerAction
     * @return string $controllerAction;
     */
    protected function getClusterAction($controllerAction)
    {
        if (in_array($controllerAction,
                ['CaseController@create', 'CaseController@store', 'CaseController@update',
                'CaseController@edit', ])) {
            $controllerAction = 'Cases-Add-Update';
        } elseif (in_array($controllerAction,
                ['ServiceController@create', 'ServiceController@store', 'ServiceController@update',
                'ServiceController@edit', 'FileController@remove', 'FileController@upload', 'ServiceFollowUpController@create',
                'ServiceFollowUpController@store', 'ServiceFollowUpController@edit', 'ServiceFollowUpController@update', ])) {
            $controllerAction = 'Services-Add-Update';
        } elseif (in_array($controllerAction,
                ['CaseController@index', 'CaseController@show'])) {
            $controllerAction = 'Cases-show';
        }

        return $controllerAction;
    }

    /**
     * Check if current route is hidden to current user role
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean                  true/false
     */
    protected function forbiddenRoute($request)
    {
        $action = $request->route()->getAction();

        if (isset($action['except'])) {
            return $action['except'] == $request->user()->role->role_slug;
        }

        return false;
    }
}
