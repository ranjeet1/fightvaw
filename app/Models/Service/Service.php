<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;
use Regulus\ActivityLog\Models\Activity;

class Service extends Model
{
    protected $fileable_key = "service";
    protected $table        = "services";
    protected $fillable     = ['details', 'service', 'case_id', 'user_id', 'date'];

    public function followups()
    {
        return $this->hasMany('App\Models\Service\ServiceFollowUp', 'service_id');
    }

    public function cases()
    {
        return $this->belongsTo('App\Models\Cases\Cases', 'case_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function followup()
    {
        return $this->belongsTo('App\Models\Service\ServiceFollowUp',
                'follow_up_id');
    }

    public function getFileKey()
    {
        return $this->fileable_key;
    }
 public function deleteService()
    {
        foreach ($this->files as $servicefiles) {
            \File::delete($servicefiles->file_path);
        }
        foreach ($this->followups as $followup) {
            $serviceFollowup = $this->where('follow_up_id', $followup->id)->delete();
            foreach ($followup->files as $followUpFiles) {
                \File::delete($followUpFiles->file_path);
            }
        }
        if ($this->follow_up_id) {
            ServiceFollowUp::where('id', $this->follow_up_id)->delete();
        }
        $this->files()->delete();
        $this->followups()->delete();
        $this->delete();
    }

    public function activityLogForCreate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => $this->service,
            'action' => 'Store',
            'description' => "$userName added '$this->title' for Case #".$this->cases->reg_no,
            'details' => "$userName added '$this->title' for Case #".$this->cases->reg_no,
            'updated' => '',
        ]);
    }

    public function activityLogForUpdate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => $this->service,
            'action' => 'Update',
            'description' => "$userName updated '$this->title' of Case #".$this->cases->reg_no,
            'details' => "$userName updated '$this->title' of Case #".$this->cases->reg_no,
            'updated' => '',
        ]);
    }

    public function activityLogForDelete()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => $this->service,
            'action' => 'Delete',
            'description' => "$userName deleted '$this->title' of Case #".$this->cases->reg_no,
            'details' => "$userName deleted '$this->title' of Case #".$this->cases->reg_no,
            'updated' => '',
        ]);
    }
}