<?php namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class ServiceFile extends Model {

	protected $table = "service_files";
	protected $fillable = ['service_id','file_name','file_path'];

	public function cases(){
		return $this->belongsTo('App\Models\Service\Service');
	}

}
