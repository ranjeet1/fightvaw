<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;
use Regulus\ActivityLog\Models\Activity;

class ServiceFollowUp extends Model
{
    protected $fileable_key = "service_follow_up";
    protected $table        = "service_follow_up";
    protected $fillable     = ['user_id', 'case_id', 'description', 'date'];

    public function service()
    {
        return $this->belongsTo('\App\Models\Service\Service', 'service_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function getFileKey()
    {
        return $this->fileable_key;
    }

    public function activityLogForCreate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => $this->service->service,
            'action' => 'Store',
            'description' => "$userName added 'Follow up' for ".$this->service->title." of Case #".$this->service->cases->reg_no,
            'details' => "$userName added 'Follow up' for ".$this->service->title." of Case #".$this->service->cases->reg_no,
            'updated' => '',
        ]);
    }

    public function activityLogForUpdate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => $this->service->service,
            'action' => 'Update',
            'description' => "$userName updated 'Follow up' for ".$this->service->title." of Case #".$this->service->cases->reg_no,
            'details' => "$userName updated 'Follow up' for ".$this->service->title." of Case #".$this->service->cases->reg_no,
            'updated' => '',
        ]);
    }
}
