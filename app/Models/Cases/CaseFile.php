<?php namespace App\Models\Cases;

use Illuminate\Database\Eloquent\Model;

class CaseFile extends Model {

	protected $fileable_key = "cases";
	protected $table = "case_files";
	protected $fillable = ['case_id','file_name','file_path','description'];

	public function cases(){
		return $this->belongsTo('App\Models\Cases\Case');
	}

}
