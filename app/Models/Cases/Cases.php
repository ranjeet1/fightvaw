<?php

namespace App\Models\Cases;

use Illuminate\Database\Eloquent\Model;
use Regulus\ActivityLog\Models\Activity;

class Cases extends Model
{
    protected $fileable_key = "case";
    protected $dates        = ['deleted_at'];
    protected $table        = "cases";
    protected $fillable     = ['user_id', 'code', 'reg_no', 'case_created', 'status',
        'case_info',
        'profile', 'contact', 'family_info', 'incident', 'perpetrator', 'details',];

    public function followups()
    {
        return $this->hasMany('App\Models\Cases\CaseFollowUp', 'case_id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service\Service', 'case_id');
    }

    public function servicefiles()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function getFileKey()
    {
        return $this->fileable_key;
    }

    public function deleteCase()
    {
        foreach ($this->services as $service) {
            $service->deleteService();
        }
        $this->delete();
        $this->activityLogForDelete();
    }

    public function activityLogForCreate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => 'Case',
            'action' => 'Store',
            'description' => "$userName created a Case #$this->reg_no",
            'details' => "$userName created a Case #$this->reg_no",
            'updated' => '',
        ]);
    }

    public function activityLogForUpdate()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => 'Case',
            'action' => 'Update',
            'description' => "$userName updated a Case #$this->reg_no",
            'details' => "$userName updated a Case #$this->reg_no",
            'updated' => '',
        ]);
    }

    public function activityLogForDelete()
    {
        $userName = \Auth::user()->name;
        Activity::log([
            'contentId' => $this->id,
            'contentType' => 'Case',
            'action' => 'Delete',
            'description' => "$userName deleted a Case #$this->reg_no",
            'details' => "$userName deleted a Case #$this->reg_no",
            'updated' => '',
        ]);
    }
}