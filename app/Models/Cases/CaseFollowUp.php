<?php namespace App\Models\Cases;

use Illuminate\Database\Eloquent\Model;

class CaseFollowUp extends Model {
	
	protected $table = "case_follow_up";
	protected $fillable = ['user_id','description'];

	public function files(){
		return $this->hasMany('App\Models\Cases\CaseFollowUp','follow_up_id');
	}

}
