<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['file_type', 'file_id', 'file_path', 'file_name'];

    public function fileable()
    {
        return $this->morphTo();
    }

    public function saveData($data)
    {
        $this->file_name     = $data['file_name'];
        $this->file_path     = $data['file_path'];
        $this->fileable_id   = $data['fileable_id'];
        $this->fileable_key  = $data['fileable_key'];
        $this->fileable_type = $data['fileable_type'];
        $this->save();
    }
}
