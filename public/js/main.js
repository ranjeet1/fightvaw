$(document).ready(function () {

    $('.datetimepicker').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        maxDate: new Date(),
    });
    $('#dateFrom').datetimepicker({
        timepicker: false,
        format: 'Y/m/d',
        maxDate: new Date(),
    });
    $('#dateTo').datetimepicker({
        timepicker: false,
        format: 'Y/m/d',
        maxDate: new Date(),
    });
    $('#dateFrom').on("change", function () {
        $('#dateTo').datetimepicker({
            minDate: $(this).val(),
        });
    });
    $('#dateTo').on("change", function () {
        $('#dateFrom').datetimepicker({
            maxDate: $(this).val(),
        });
    });
    $('#date-from').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        maxDate: new Date(),
    });
    $('#date-to').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        maxDate: new Date(),
    });

    //This marks the start of the codes for date range functions from http://xdsoft.net/jqplugins/datetimepicker/
    jQuery('#date-from').datetimepicker({
        format:'Y/m/d',
        onShow:function( ct ){
            this.setOptions({
                maxDate:jQuery('#date-to').val()?jQuery('#date-to').val():0
            })
        },
        timepicker:false
    });

    jQuery('#date-to').datetimepicker({
        format:'Y/m/d',
        onShow:function( ct ){
            this.setOptions({
                minDate:jQuery('#date-from').val()?jQuery('#date-from').val():false
            })
        },
        timepicker:false
 });
    //This marks the end of the codes for date range functions from http://xdsoft.net/jqplugins/datetimepicker/

    $('.multiple-input').select2({allowClear: true});


    function goToByScroll(id) {
        // Reove "link" from the ID
        id = id.replace("link", "");
        // Scroll
        $('html,body').animate({
            scrollTop: $("#" + id).offset().top - 60},
        'slow');
    }

    $(".followup-heading .title a").click(function (e) {
        // Prevent a page reload when a link is pressed
        e.preventDefault();
        // Call the scroll function
        goToByScroll($(this).attr("id"));

    });

    $('.wrapper-dropdown-3').click(function (e) {
        e.stopPropagation();
        $('.dropdown').toggle();
        $('.create-row .services-top-heading h2').toggleClass('toggle-margin');
        $('.add-list').css('margin', '0');
    });

    $(".close").click(function () {
        $(".alert").hide();
    });
     $(".alert alert-danger").click(function () {
        $(".alert alert-danger").hide();
    });

    $(document).click(function () {
        $('.dropdown').hide();
        $('.create-row .services-top-heading h2').toggleClass('toggle-margin');
    });

    $('.basic-info').readmore({
        afterToggle: function (trigger, element, expanded) {
            if (!expanded) {

                $('html, body').animate({scrollTop: $('.basic-info').offset().top}, {duration: 100});
            }

        }});

    $('.assessment-toggle').hide();
    $('.assessment .more').click(function () {
        $(this).siblings('.assessment-toggle').slideToggle();
        $(this).toggle();
        $(this).siblings('.less').toggle();
        $('html,body').animate({
            scrollTop: $(this).parent().offset().top
        }, 500);
    });

    $('.assessment .less').on('click', function () {
        $(this).siblings('.assessment-toggle').slideToggle();
        $(this).toggle();
        $(this).siblings('.more').toggle();
        $('html,body').animate({
            scrollTop: $(this).parent().offset().top
        }, 1000);
    });

    /**
     * Add class has-error to input, select to highlight
     */
    $('.form-class').children().each(function(){
        if($(this).children().hasClass('text-danger')){
            $(this).children().addClass('has-error');
        }
    });

    $('.form-group.upload-files-section').prev().css("margin-bottom", "0");


    if ($(".search-filter").is(':visible')) {
        $('html,body').animate({
            scrollTop: $('.slide-upward').offset().top}, 1000);
    }

    $('.form-group.parentForm > label:first-child').click(function () {
        $(this).siblings('.form-group').toggle(function () {
            $('.other-form').css("display", 'none!important');
        }, function () {

        });
        $(this).toggleClass('togglearrow');

    });

    $('.form-group.complainForm > label:first-child').click(function () {
        $(this).siblings('.form-group').toggle();
        $(this).toggleClass('togglearrow');

    });

    $('.xdsoft_datetimepicker').on('change', function () {
        $(this).hide();
    });


    $('.more').on('click', function () {
        $(this).hide();
        $(this).siblings().show();
        $('.hide-dot').css('display', 'none');
    });

    $('.login-wrap').css({
        position: 'absolute',
        left: ($(window).width() - $('.login-wrap').outerWidth()) / 2,
        top: ($(window).height() - $('.login-wrap').outerHeight()) / 2 - 10
    });

    $('.less').hide();
    $('.less').on('click', function () {
        $(this).hide();
        $('.hide-dot').css('display', 'inline');
        $('.remain-description').hide();
        $(this).siblings('.more').show();
    });
    //console.log('form-group.parentForm :has(div.text-danger)');
    if ($('.form-group.parentForm :has(div.text-danger)')) {
        $('.form-group.parentForm :has(div.text-danger)').parent().children('.form-group').show();
    }

    $('.otherOptions').on('change', function () {
        if ($(this).val() == 'Others') {
            var id = $(this).attr('id');
            var name = $(this).attr('name');
            var inputText = "<input id='" + id + "[Others]' class='form-control others-specify' type='text' name='" + name + "[Others]' placeholder='Others(Specify)'>";
            $(this).after(inputText);
        } else {
            $(this).next().remove();
            $(this).parent().next().find('.hide-label').parent().remove();
        }
    });

    // Add More
    $('.add-more-cross').after().append("<span class ='remove-more'></span>");
    $('.add-more').on("click", function () {
        //console.log($(this));
        var index = $(this).index();
        //console.log(index);
        var html1 = $(this).parent().children(':nth-child(' + index + ')').clone();
        inputText = html1.find('input');
        var previous = parseInt(inputText[0].name.split("-")[1][0])

        //console.log(previous)
        //var previous = $(this).parent().children(':nth-child(' + index + ')').data('repeat');

        //console.log("previous"+previous);
        var next = parseInt(previous) + 1;
        //console.log('next'+next);
        html1.removeClass('other-form');
        html1.css('display', 'block', 'important');
        html1.attr('data-repeat', next);
        html1.find('input').each(function () {
            $(this).val('');

            //var test = $(this).attr('name');
            //console.log(test);
            //console.log('test next'+next);
            var newName = $(this).attr('name').replace(previous, next).replace("[" + previous + "]", "[" + next + "]");
            $(this).attr('name', newName);
        });
        html1.find('select').each(function () {
            $(this).val('');
            var newName = $(this).attr('name').replace(previous, next).replace("[" + previous + "]", "[" + next + "]");
            $(this).attr('name', newName);
        });
        $(this).before(html1);
        $('.remove-more').on('click', function () {
            $(this).parent().remove();
        });
    });
    $('.remove-more').on('click', function () {
        $(this).parent().remove();
    });
    // search form

    $('.search-wrapper input').focus(function () {
        if ($(".search-filter").is(':visible')) {
            $('.search-form button.search-buttton').hide();
        } else {
            $('.search-form button.search-buttton').css('display', 'block');
        }
    });
    $(".advance-search").click(function () {
        $('.search-form button.search-buttton').hide();
        $(".search-filter").toggle();
        $(".search-child").toggleClass("search-child-toggle");
        $(".search-header").toggleClass("search-radius");
    });
    $(".add").click(function () {
        $(".remove").toggle();
        $(".add").hide();
    });
    $(".remove").click(function () {
        $(".add").toggle();
        $(".remove").hide();
    });
    $(".cancel-search").click(function () {
        $(".search-child").toggleClass("search-child-toggle");
        $(".search-filter").hide();
    });
    if ($(".search-filter").is(':visible')) {
        $(".search-header").addClass("search-radius");
    }
    else
    {
        $(".search-header").removeClass("search-radius");
    }
    if ($(".remove").is(':visible')) {
        $(".search-header").removeClass("search-radius");
    }
    $(".reset-button").click(function (e) {
        $(':input').val('');
        $('#dateFrom').val('');
        $("select").each(function () {
            $(this).attr("value", '');
            $(this).select2("val", "");
        });
        e.preventDefault();
    });

    $("select#associatedChildProblems :selected").map(function (i, el) {
        var obj = $("select#associatedChildProblems").parent().next();
        var value = $(el).val();
        var idValue = value.replace(" ", "-").replace("/", "").replace("(", "").replace(")", "");
        if (idValue) {
            obj.find('div#' + idValue).find(':input').removeAttr('disabled');
        }
        if (idValue && obj.find('div#' + idValue).hasClass('hide-section')) {
            obj.find('div#' + idValue).removeClass('hide-section');
        }
    });

    $('select#associatedChildProblems').on('change', function () {
        var obj = $(this).parent().next();
        var selectList = $(this).val();
        if (selectList) {
            $.each(selectList, function (i, val) {
                var id = val.replace(" ", "-").replace("/", "").replace("(", "").replace(")", "");
                obj.find('div#' + id).removeClass('hide-section');
                obj.find('div#' + id).find(':input').removeAttr('disabled');

            });
        }
        $("select#associatedChildProblems option:not(:selected)").map(function (i, el) {
            var value = $(el).val();
            var idValue = value.replace(" ", "-").replace("/", "").replace("(", "").replace(")", "");

            if (idValue) {
                obj.find('div#' + idValue).find(':input').attr('disabled', 'disabled');
            }

            if (idValue && !obj.parent().next().find('div#' + idValue).hasClass('hide-section')) {
                obj.find('div#' + idValue).addClass('hide-section');
            }
        });
    });
    
    $('.nepalAddress .form-group').hide();
    $('.otherAddress .form-group').hide();

    var selected_option = $('#per-country option:selected').val();


    // checking condition of coutry value for tpo

    if ($('select[name="contact[PermanentAddress][country]"]').val() != 0) {
          if(selected_option != "Nepal") {
            $('.otherAddress .form-group').css("display","block");
            $('.nepalAddress .form-group').css("display","none");
            
        } else {
             $('.nepalAddress .form-group').css("display","block");
             $('.otherAddress .form-group').css("display","none");
        } 
    }
        else{
            $('.nepalAddress .form-group').hide();
            $('.otherAddress .form-group').hide();
        } 

    // checking condition of coutry for CWIN
    if ($('select[name="contact[childContact][PermanentAddress][country]"]').val() != 0) {

        if(selected_option != "Nepal") {

            $('.otherAddress .form-group').css("display","block");
            $('.nepalAddress .form-group').css("display","none");
            
        } else {

             $('.nepalAddress .form-group').css("display","block");
             $('.otherAddress .form-group').css("display","none");
        } 
    }
        else{
            $('.nepalAddress .form-group').hide();
            $('.otherAddress .form-group').hide();
        }    
        
 $('select[name="contact[PermanentAddress][country]"]').on('change',function () {
        if ($(this).val() === 'Nepal') {
            $('select[name="contact[PermanentAddress][nepalAddress][district]"]').val('');
            $('input[name="contact[PermanentAddress][nepalAddress][wardNo]"]').val('');
            $('input[name="contact[PermanentAddress][nepalAddress][vdcMunicipality]"]').val('');
            $('.nepalAddress .form-group').css("display","block");
            $('.otherAddress .form-group').css("display","none");
        } else {
            $('textarea[name="contact[PermanentAddress][otherAddress][description]"]').val('');
            $('.otherAddress .form-group').css("display","block");
            $('.nepalAddress .form-group').css("display","none");
        }  
    });

 $('select[name="contact[childContact][PermanentAddress][country]"]').on('change',function () {

        if ($(this).val() === 'Nepal') {

            $('select[name="contact[childContact][PermanentAddress][nepalAddress][district]"]').val('');
            $('input[name="contact[childContact][PermanentAddress][nepalAddress][wardNo]"]').val('');
            $('input[name="contact[childContact][PermanentAddress][nepalAddress][vdcMunicipality]"]').val('');
            $('.nepalAddress .form-group').css("display","block");
            $('.otherAddress .form-group').css("display","none");
        } else {
            $('textarea[name="contact[childContact][PermanentAddress][otherAddress][description]"]').val('');
            $('.otherAddress .form-group').css("display","block");
            $('.nepalAddress .form-group').css("display","none");
        }  
    });

    $(".ajax-validate #Save").on("click", function () {
        var form_val_serialized = $("form.ajax-validate").serialize();
        var postUrl = $("form").attr('action');
        var jsRetVal = false;

        $('input').removeClass('has-error');
        $('select').removeClass('has-error');

        $.ajax({
            async: false,
            type: "POST",
            url: postUrl,
            data: form_val_serialized + "&ajax=true",
            dataType: 'json',
            success: function (json) {
                window.location.href = json.url;
            },
            error: function (errors) {
                jsRetVal = false;
                $('div.text-danger').remove();
                $('div.alert.alert-danger').remove();
                var errorString = '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><ul>';
                
                $.each(errors.responseJSON, function (fieldName, error)
                {
                    var fieldNameArray = fieldName.split('.');
                    var inputName = '';
                    $.each(fieldNameArray, function (index, value)
                    {
                        inputName += "\\[" + value + "\\]";
                    });
                    inputName = inputName.replace('\\[', '').replace('\\]', '');
                    $('#' + inputName + '').addClass('has-error');
                    $('#' + inputName + '').after('<div class="text-danger">' + error[0] + '</div>');
                    errorString += '<li>' + error[0] + '</li>';

                });
                errorString += '</ul></div>';
                $('.form-info').before(errorString);
                $("html, body").animate({scrollTop: 0}, "");
                if ($('.form-group.parentForm :has(div.text-danger)')) {
                    $('.form-group.parentForm :has(div.text-danger)').parent().children('.form-group').show();
                }

                $(".close").click(function () {
                    $(".alert").hide();
                });
            }

        });
        return jsRetVal;
    });
});

function redirectFunction($url)
{
    window.location.replace($url);
}
