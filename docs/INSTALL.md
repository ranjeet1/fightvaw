## Install

FightVAW Custom can be cloned after having access to the gitlab repository and follow the given below procedure to install:

1. Clone the code from git@gitlab.yipl.com.np:web-apps/fightvaw_custom.git in your web root folder.
2. Add `127.0.0.1 fightvaw_custom.dev` to your /etc/hosts file
3. Change your apache host file
4. Restart apache server

## Run

1. Inside project directory, install the application dependencies using command: `composer install`.
2. Copy .env.example to .env.
3. Make the require changes and update your database configurations in .env file.
4. In .env file, provide the name of particular organization to access it.
5. Run migration using: `php artisan migrate`.
6. Seed dummy data using: `php artisan db:seed`.
7. Give read/write permission to the storage folder using command: `chmod -R 777 storage`.
8. Create a directory uploads inside public folder and, files and temp directory inside the uploads directory.
9. Give read/write permission to uploads directory recurrsively using: `chmod 777 -R uploads/`.
10. Access `http://fightvaw_custom.dev`.
11. Login access: **Email**: yipl@fightvaw.org **Password**: #yipl@fightvaw#
    