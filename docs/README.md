#FightVAW Custom Description

FightVAW (Fight Violence Against Women) is an initiative to fight violence against women in Nepal using ICT (Information and Communications Technology). This is a followup from the VawHack (hackathon on VAW) conducted on Jun 16, 2013. Learn more about it from `http://fightvaw.org/about`.

FightVAW Custom is developed for different organizations working for welfare of women. Recently, FightVAW Custom has integrated four Organizations: Lacc, CWIN, TPO, and Saathi.

##System Structure

The system is developed using php framework [Laravel](http://laravel.com) `5.0`. Various packages has been used as per the requirement.

### Tools and packages

This application uses many tools and packages. Those packages can
be seen in the [composer.json](http://gitlab.yipl.com.np/web-apps/agentcis/blob/master/compos...).

Some major PHP packages used are listed below:
1. [regulus/activity-log](https://packagist.org/packages/regulus/activity-log)- for monitoring user activity on a website or web application.
2. [zizaco/entrust](https://packagist.org/packages/zizaco/entrust) - for user roles and permission.
3. [rap2hpoutre/laravel-log-viewer](https://packagist.org/packages/rap2hpoutre/laravel-log-viewer) - for log view.

## Check code quality

We follow [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted<...) for coding standard.

# Structure for a particular Organization
Fightvaw directory is the repository that contains the require blocks for an organization. Block consists of Organization, Data, Element, and View.
## Organization
Covers the details about an organization. Required informations for an organization, to build up are fetch from this.

## Data
Collects the data require for an organization to build case intake forms and various service's form

##Element
Collects the fields defination requires to build up case intake forms and various service's form

##View
Own view layouts can be defined here to override the basic templates for case intake and for various services of an organization.

