# The server-based syntax can be used to override options:
# ------------------------------------
server "ENV['org'].fightvaw.org",
    roles: %w{web app},
    ssh_options: {
        user: 'fightvaw', # overrides user setting above
        keys: %w(/home/fightvaw/.ssh/id_rsa),
        forward_agent: true,
        auth_methods: %w(publickey password)
    }

# Directory to deploy 
# ===================
    set :deploy_to, "/home/fightvaw/subdomains/ENV['org']"

# Branch
# ======
    set :branch, "Fightvaw-ENV['org']"