# config valid only for current version of Capistrano
    lock '3.4.0'

#set :application, 'my_app_name'
    set :repo_url, 'git@gitlab.yipl.com.np:web-apps/fightvaw_custom.git'

# Default value for :scm is :git
    set :scm, :git

# Default value for :pty is false
    set :pty, true

# Default tmp folder
    set :tmp_dir, "/home/fightvaw/fightvaw_tmp"

namespace :deploy do
     
    desc "Build"
    after :updated, :build do
        on roles(:app) do
            within release_path  do
                 execute :tar, xf  components/vendor.tar.gz
                 execute :cp, .env.example .env 
                 execute :chmod, "-R 777 storage/               
            end
        end
    end
 
end
