<div class="search-form">
    {!! Form::open(['method' => 'get']) !!}
    <div class="field-wrapper">
        {!! Form::label('from','From')!!}
        {!! Form::text('from',null,['class'=>'','placeholder'=>'YYYY-MM-DD','id'=>'date-from'])!!}
    </div>
    <div class="field-wrapper">
        {!! Form::label('To','To')!!}
        {!! Form::text('to',null,['class'=>'','placeholder'=>'YYYY-MM-DD','id'=>'date-to'])!!}
    </div>
    <div class="action-btn">
        {!! Form::button('Search',['class'=>'move search-btn', 'name'=>'advancedSearch','type'=>'submits']) !!}
    </div>
    {!! Form::close() !!}
</div>
