@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row add-row">
        <div class="wrapper case-wrapper">
            <div class="case-heading-wrapper">
                <h1>Activity Logs</h1>
            </div>
            <div class="search-wrap">
                @include("log.partials._search")
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                            @forelse($logs as $log)
                            <tr>
                                <td>{{$log->description." on ".ViewHelper::formatDate($log->created_at)}}</td>
                            </tr>
                            @empty
                            <tr><td><p class="no-list">No logs yet!</p></td></tr>
                            @endforelse
                    </table>
                    <div class="pagination">
                        {!! $logs->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
