{!! Form::hidden('id') !!}

<div class="form-group">
    {!! HTML::decode(Form::label('name',"Name<span class='required'>*</span>")) !!}
    {!! Form::text('name') !!}
    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! HTML::decode(Form::label('email',"Email<span class='required'>*</span>")) !!}
    {!! Form::text('email') !!}
    {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! HTML::decode(Form::label('phone_number',"Phone Number<span class='required'>*</span>")) !!}
    {!! Form::text('phone_number') !!}
    {!! $errors->first('phone_number', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! HTML::decode(Form::label('address',"Address<span class='required'>*</span>")) !!}
    {!! Form::textarea('address') !!}
    {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
</div>


<div class="form-group">
    {!! HTML::decode(Form::label('password',"Password<span class='required'>*</span>")) !!}
    {!! Form::input('password', 'password', $users->password) !!}
    {!! $errors->first('password_new', '<div class="text-danger">:message</div>') !!}
</div>


<div class="form-group">
    {!! HTML::decode(Form::label('role',"Role<span class='required'>*</span>")) !!}
    {!! Form::select('role[]',App\Models\Users\Role::lists('name','id'),$users->roles()->getRelatedIds(),['class'=>'multiple-input input-name','multiple'=>true,'placeholder'=>'Select Role']) !!}
    {!! $errors->first('role', '<div class="text-danger">:message</div>') !!}
</div>


<div class="form-group">
    {!! HTML::decode(Form::label('is_active',"Is Active")) !!}
    {!! Form::checkbox('is_active') !!}
    {!! $errors->first('is_active', '<div class="text-danger">:message</div>') !!}
</div>

<div class="button-wrap">
    {!! Form::button('Save',['type' => 'submit']) !!}
    {!! Form::button('Cancel',['onclick' => "redirectFunction('".route('users.index')."')"]) !!}
</div>