@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row">
        <div class="wrapper">
            <h1> Add User</h1>
            <span class="form-info">Fields with <i> * </i>are required.</span>
            @include('partials.errors.basic')
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($users, ['route' => 'users.store','class' => 'form-class']) !!}

                    @include('users.partials._userForm')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
