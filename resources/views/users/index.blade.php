@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row add-row">
        <div class="wrapper case-wrapper">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="add-case-link">
                            {!! HTML::decode(link_to_route('users.create', "<span>+</span>Add User")) !!}
                    </div>
                    <table class="table table-striped multi-col">
                        <thead>
                            <tr>
                                <th class="name">Name</th>
                                <th class="email">Email</th>
                                <th class="phone">Phone Number</th>
                                <th class="role">Role</th>
                                <th class="isactive">Is Active</th>
                                <th class="action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone_number}}</td>
                                <td>{{implode(", ",$user->roles->lists('name'))}}</td>
                                <td>{{ ($user->is_active)?'Yes':'No' }}</td>
                                <td>
                                    {!! link_to_route('users.edit','Edit',['id'=>$user->id]) !!}
                                </td>
                            </tr>
                                @empty
                                <tr><td><p class="no-list">No users yet!</p></td></tr>
                            @endforelse
                        </tbody>
                    </table>

                    <div class="pagination">
                        {!! $users->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop