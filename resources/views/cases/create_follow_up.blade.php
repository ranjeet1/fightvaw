@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row">
        <div class="wrapper">
            <h1>Add Follow Up for Case {{$case->id}}</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
