@extends('layouts.app')

@section('content')
    <?php $services = $organizationObj->getServices(); ?>
    <div class="container">
        <div class="row create-row">
            <div class="wrapper">
                <div class="top-heading">
                    <h1>Case #{{$case->reg_no}}</h1>
                </div>
                <div class="panel panel-default case-wrap">
                    @if(Entrust::can('manage-all') || Entrust::can('delete-all-cases') || (Entrust::can('delete-own-cases') && \Auth::user()->id == $case->user_id ) )
                        <div class="delete-btn">
                            {!! link_to_route('cases.delete', "Delete Case",$case->id) !!}
                        </div>
                    @endif


                    @if( Entrust::can('manage-all') || (Entrust::can('manage-own-cases') && \Auth::user()->id == $case->user_id) || Entrust::can('manage-all-cases'))
                        <div class="edit-btn">
                            {!! link_to_route('cases.edit', "Edit Case Information",$case->id) !!}
                        </div>
                    @endif

                    <div class="panel-body">
                        <div class="info-wrap">
                            <div class="basic-info">
                                <div class="title">Basic Information</div>
                                <ul>
                                    <li>
                                        <label>Date</label><span>{{ ViewHelper::formatDate($case->case_created) }}</span>
                                    </li>
                                </ul>
                                <?php $jsonFields = $organizationObj->getJsonCaseFields(); ?>
                                <?php
                                $key = array_search('incident', $jsonFields);
                                if (isset($jsonFields[$key])) {
                                    unset($jsonFields[$key]);
                                }
                                ?>
                                @foreach($jsonFields as $fields)
                                    <div class="basic-more-info">
                                        <?php $filePath
                                                = ViewHelper::viewFileExist("Fightvaw::cases.partials._$fields");
                                        ?>
                                        @if(str_contains($filePath, "Fightvaw::"))
                                            @include($filePath)
                                        @else
                                            <?php
                                                echo nl2br($intakeGenerator->generate($case, 'Cases', $fields));
                                            ?>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <?php $incidentFilePath = ViewHelper::viewFileExist("Fightvaw::cases.partials._incident"); ?>
                            @include($incidentFilePath)
                        </div>
                        @if(\Auth::user()->can(['manage-all','manage-all-services', 'manage-own-services', 'view-all-services']))
                            @include("services.index")
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
