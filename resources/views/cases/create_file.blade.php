@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row">
        <div class="col-sm-8 col-sm-offset-2">
            <h1>Add Files for Case {{$case->id}}</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
