@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row">
        <div class="wrapper">
            <h1> Add a New Case</h1>
             <span class="form-info">Fields with <i> * </i>are required.</span>
            @include('partials.errors.basic')
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop