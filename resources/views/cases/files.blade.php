@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Case files </span></div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>File Name</th>
                                <th>Description</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($files as $file)
                            <tr>
                                <td>
                                    <a href="{{ asset($file->file_path) }}" target="_blank">{{$file->file_name}}</a>
                                </td>
                                <td>
                                    {{$file->description}}
                                </td>
                                <td>
                                    <a href="#">Remove</a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3"> No Files Found</td>
                            </tr>
                            @endforelse
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

