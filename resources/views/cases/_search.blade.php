<div class="search-form">
    {!! Form::open(['method' => 'get']) !!}
        <div class="search-header">
        <div class="field-wrapper search-wrapper">
            {!! Form::text('search',isset($inputs['search'])?$inputs['search']:null,['placeholder'=>'Search Cases ...'])!!}
        </div>
        </div>
        <?php unset($inputs['search']); ?>
        <div class="search-child @if(count($inputs)) search-child-toggle  @endif">
            {!! Form::button('Search',['class'=>'search-buttton hide-toggle','name'=>'advancedSearch','type'=>'submits']) !!}
            <div class="advance-search">
                <span class="add">Advanced Search</span>
                <span class="remove">Advanced Search</span>
            </div>
        </div>
        
        <div class="search-filter" @if(count($inputs)) style="display: block"  @endif>
        <div class="field-wrapper">
            {!! Form::label('from','From')!!}
            {!! Form::text('from',isset($inputs['from'])?$inputs['from']:null,['class'=>'','placeholder'=>'YYYY/MM/DD','id'=>'dateFrom'])!!}
        </div>
        <div class="field-wrapper">
            {!! Form::label('To','To')!!}
            {!! Form::text('to',isset($inputs['to'])?$inputs['to']:null,['class'=>'','placeholder'=>'YYYY/MM/DD','id'=>'dateTo'])!!}
        </div>

        @foreach($searchOptions as $searchKey => $searchValue)
                <div class="field-wrapper">
                    {!! Form::label($searchKey,$searchValue['label'])!!}
                    @if($searchValue['field_type'] =="text")
                        {!! Form::text($searchKey,isset($inputs[$searchKey])?$inputs[$searchKey]:null,isset($searchValue['attrs'])?$searchValue['attrs']:[]) !!}
                    @elseif($searchValue['field_type'] =="select")
                        {!! Form::select((isset($searchValue['attrs']['multiple']) && $searchValue['attrs']['multiple'])? $searchKey."[]" : $searchKey, $data->getData($searchKey), isset($inputs[$searchKey])?$inputs[$searchKey]:[],isset($searchValue['attrs'])?$searchValue['attrs']:null) !!}
                    @endif
                </div>
        @endforeach

        <div class="action-btn">
            {!! Form::button('Search',['class'=>'move search-btn', 'name'=>'advancedSearch','type'=>'submits']) !!}
            {!! Form::button('Reset',['class'=>'reset-button']) !!}
            {!! Form::button('Cancel',['class'=>'cancel-search', 'onclick'=>"redirectFunction('".route('cases.index')."')"]) !!}
        </div>

    </div>
    {!! Form::close() !!}
</div>
