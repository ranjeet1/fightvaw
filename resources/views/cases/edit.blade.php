@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row">
        <div class="wrapper edit-form">
            <h1>Edit Case #{{$case->reg_no}}</h1>
            <span class="last-update">(Last Updated on {{date('d F Y',strtotime($case->created_at))}})</span>
            <span class="form-info">Fields with <i>*</i> are required.</span>
            @include('partials.errors.basic')
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop