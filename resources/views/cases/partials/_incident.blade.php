<?php

use App\Core\Elements\ElementHandler;

$elementhandler = new ElementHandler('Cases');
$elements = $elementhandler->getElements('incident');
?>
<div class="incident">
    <div class="title">Incident</div>
    <?php $incident = JsonHelper::JsonDecode($case->incident); ?>
    <ul>
        @foreach($elements as $key => $element)
        @if($key != 'description')
        <li><label for="">{{ViewHelper::getLabel($element)}}</label>
            <?php $value = (isset($incident->$key))?$incident->$key:''; ?>
            {{ViewHelper::getValue($element, $value)}}
        </li>
        @else
        <li>
            <label for="">Description</label>
            <p>
                <?php $splitDescription = \App\Helpers\Length::getStringSplit($incident->description); ?>
                {!!nl2br($splitDescription['first'])!!}
                {!!nl2br($splitDescription['second'])!!}
            </p>
        </li>
        @endif
        @endforeach
    </ul>
</div>
