@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row add-row">
        <div class="wrapper case-wrapper">
            <div class="case-heading-wrapper">
                @if (Session::has('status'))
                    <div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('status') }}
                    </div>
                @endif
                <div id="map" class="map"></div>
                <h1>Cases</h1>
                <div class="add-case-link">

                    @if(Entrust::can('manage-all') || Entrust::can('manage-own-cases') || Entrust::can('manage-all-cases'))
                    {!! HTML::decode(link_to_route('cases.create',"<span>+</span>Add New Case")) !!}
                    @endif
                </div>
            </div>

            @include('cases._search')
            @if(count($inputs) && count($cases))
            <div class="result-title slide-upward">
                Your Search Results:
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <tbody>
                            @forelse($cases as $case)

                            <tr>
                                <td>
                                    <div class="reg-wrap">
                                        {!! link_to_route('cases.show',$case->reg_no,[$case->id],['class'=>'reg-num'])!!}

                                        @if( Entrust::can('manage-all') ||( (Entrust::can('manage-own-cases') ) && \Auth::user()->id == $case->user_id) || Entrust::can('manage-all-cases'))
                                            {!! link_to_route('cases.edit','Edit',[$case->id],['class'=>'case-edit-icon'])!!}
                                        @endif


                                    </div>
                                    <p>
                                        <?php $incident = json_decode($case->incident);?>
                                        @if(isset($incident->description))
                                            <?php $splitDescription = \App\Helpers\Length::getStringSplit($incident->description); ?>
                                            {!!$splitDescription['first']!!}
                                            {!!$splitDescription['second']!!}
                                        @endif
                                    </p>
                                </td>
                                <td>
                                    <span>{{ ViewHelper::formatDate($case->case_created) }}</span>
                                </td>
                            </tr>
                            @empty
                            <tr class="negative-result slide-upward">
                                <td colspan="3">Cases Not Found</td>
                            </tr>
                            @endforelse
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
