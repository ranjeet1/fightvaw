@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row create-row add-row">
        <div class="wrapper case-wrapper">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="add-case-link">
                            {!! HTML::decode(link_to_route('role.create', "<span>+</span>Add Role")) !!}
                    </div>
                    <table class="table table-striped multi-col">
                        <thead>
                            <tr>
                                <th class="name">Name</th>
                                <th class="description">Description</th>
                                <th class="permission">Permission</th>
                                <th class="action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($roles as $role)
                            <tr>
                                <td>{{$role->name}}</td>
                                <td>{{$role->description}}</td>
                                <td>{{implode(", ",$role->perms->lists('display_name'))}}</td>
                                <td>
                                    {!! link_to_route('role.edit','Edit',['id'=>$role->id]) !!}
                                </td>
                            </tr>
                                @empty
                                <tr><td><p class="no-list">No role define yet!</p></td></tr>
                            @endforelse
                        </tbody>
                    </table>

                    <div class="pagination">
                        {!! $roles->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop