{!! Form::hidden('id') !!}

<div class="form-group">
    {!! HTML::decode(Form::label('name',"Name of the Role<span class='required'>*</span>")) !!}
    {!! Form::text('name') !!}
    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! HTML::decode(Form::label('description',"Description<span class='required'>*</span>")) !!}
    {!! Form::textarea('description') !!}
    {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
</div>


<div class="form-group">
    {!! HTML::decode(Form::label('permission',"Permission<span class='required'>*</span>")) !!}

<div class="form-group sub-options" id="test">
<?php
    $relatedId = $role->perms()->getRelatedIds();
    $permissionList =    App\Models\Users\Permission::lists('display_name','id');
?>
   <div class="form-group">     
    <label class="sub-title">Cases</label>
    <div class="choice-wrap"> 
        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',3,(in_array(3,$relatedId))?true:null,['id'=>'permission3'])  !!}
        <label for="permission3">{{$permissionList[3]}} </label>
        </div>
        
        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',4,(in_array(4,$relatedId))?true:null,['id'=>'permission4'])  !!}
        <label for='permission4'>{{$permissionList[4]}} </label>
        </div>
        
        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',5,(in_array(5,$relatedId))?true:null,['id'=>'permission5'])  !!}
        <label for='permission5'>{{$permissionList[5]}}</label> 
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',13,(in_array(13,$relatedId))?true:null,['id'=>'permission13','disabled'])  !!}
        <label for='permission13'>{{$permissionList[13]}} </label>
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',12,(in_array(12,$relatedId))?true:null,['id'=>'permission12','disabled'])  !!}
        <label for='permission12'>{{$permissionList[12]}} </label>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label class="sub-title">Services</label>
    <div class="choice-wrap">
        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',6,(in_array(6,$relatedId))?true:null,['id'=>'permission6','disabled'])  !!}
        <label for='permission6'>{{$permissionList[6]}}</label> 
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',7,(in_array(7,$relatedId))?true:null,['id'=>'permission7','disabled'])  !!}
        <label for='permission7'>{{$permissionList[7]}}</label> 
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',8,(in_array(8,$relatedId))?true:null,['id'=>'permission8','disabled'])  !!}
        <label for='permission8'>{{$permissionList[8]}}</label> 
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',11,(in_array(11,$relatedId))?true:null,['id'=>'permission11','disabled'])  !!}
        <label for='permission11'>{{$permissionList[11]}}</label>
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',10,(in_array(10,$relatedId))?true:null,['id'=>'permission10','disabled'])  !!}
        <label for='permission10'>{{$permissionList[10]}}</label>
        </div>
    </div>
    </div>
    <label class="sub-title">Users / Roles / Activity Log</label>
    <div class="choice-wrap">
        <div class="choice-row">
        {!! Form::checkbox('permission[]',2,(in_array(2,$relatedId))?true:null,['id'=>'permission2','disabled'])  !!}
        <label for='permission2'>{{$permissionList[2]}}</label>
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',9,(in_array(9,$relatedId))?true:null,['id'=>'permission9','disabled'])  !!}
        <label for='permission9'>{{$permissionList[9]}} </label>
        </div>

        <div class="choice-row"> 
        {!! Form::checkbox('permission[]',14,(in_array(14,$relatedId))?true:null,['id'=>'permission14','disabled'])  !!}
        <label for='permission14'>{{$permissionList[14]}}</label>
        </div> 
    </div>
    <label class="sub-title">All</label>
    <div class="choice-wrap">
        <div class="choice-row">  
        {!! Form::checkbox('permission[]',1,(in_array(1,$relatedId))?true:null,['id'=>'permission1'])  !!}
        <label for='permission1'>{{$permissionList[1]}}</label>
        </div> 
    </div>
    {!! $errors->first('permission', '<div class="text-danger">:message</div>') !!}
</div>
</div>
<div class="button-wrap">
    {!! Form::button('Save',['type' => 'submit']) !!}
    {!! Form::button('Cancel',['onclick' => "redirectFunction('".route('role.index')."')"]) !!}
</div>

<script>
    "use strict";

    var permission1 = document.getElementById('permission1');
    var permission2 = document.getElementById('permission2');
    var permission3 = document.getElementById('permission3');
    var permission4 = document.getElementById('permission4');
    var permission5 = document.getElementById('permission5');
    var permission6 = document.getElementById('permission6');
    var permission7 = document.getElementById('permission7');
    var permission8 = document.getElementById('permission8');
    var permission9 = document.getElementById('permission9');
    var permission10 = document.getElementById('permission10');
    var permission11 = document.getElementById('permission11');
    var permission12 = document.getElementById('permission12');
    var permission13 = document.getElementById('permission13');
    var permission14 = document.getElementById('permission14');

    function enableIfPermission3IsChecked(){
        permission13.removeAttribute('disabled');
        permission12.removeAttribute('disabled');
        permission6.removeAttribute('disabled');
        permission7.removeAttribute('disabled');
        permission8.removeAttribute('disabled');
        permission2.removeAttribute('disabled');
        permission9.removeAttribute('disabled');
        permission14.removeAttribute('disabled');
    }

    function enableIfPermission4IsChecked()
    {
        permission12.removeAttribute('disabled');
        permission7.removeAttribute('disabled');
        permission6.removeAttribute('disabled');
        permission2.removeAttribute('disabled');
        permission9.removeAttribute('disabled');
        permission14.removeAttribute('disabled');
    }

    function enableIfPermission5IsChecked()
    {
        permission7.removeAttribute('disabled');
        permission8.removeAttribute('disabled');
        permission6.removeAttribute('disabled');
        permission2.removeAttribute('disabled');
        permission9.removeAttribute('disabled');
        permission14.removeAttribute('disabled');
    }
    if(permission3.checked){
        enableIfPermission3IsChecked();
    }

    if(permission4.checked){
        enableIfPermission4IsChecked();
    }

    if(permission5.checked){
        enableIfPermission5IsChecked();
    }

    if(permission6.checked && (permission3.checked || permission5.checked)){
        permission11.removeAttribute('disabled');
    }

    if(permission6.checked && (permission4.checked || permission5.checked))
    {
        permission10.removeAttribute('disabled');
    }

    if(permission7.checked && (permission3.checked || permission5.checked ||  permission4.checked)){
        permission10.removeAttribute('disabled');
    }


    document.getElementById('test').onchange = function(e){

        if( e.target.value == '3' && e.target.checked)
        {
            enableIfPermission3IsChecked();
        }
        if(e.target.value == '3' && !e.target.checked) {
            permission13.checked = false;
            permission13.setAttribute('disabled','disabled');

            permission12.checked = false;
            permission12.setAttribute('disabled','disabled');

            permission6.checked = false;
            permission6.setAttribute('disabled','disabled');

            permission7.checked = false;
            permission7.setAttribute('disabled','disabled');

            permission8.checked = false;
            permission8.setAttribute('disabled','disabled');

            permission11.checked = false;
            permission11.setAttribute('disabled','disabled');

            permission10.checked = false;
            permission10.setAttribute('disabled','disabled');

            permission2.checked = false;
            permission2.setAttribute('disabled','disabled');

            permission9.checked = false;
            permission9.setAttribute('disabled','disabled');

            permission14.checked = false;
            permission14.setAttribute('disabled','disabled');

            if(permission4.checked){
                permission7.removeAttribute('disabled');
                permission12.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
            if(permission5.checked){
                permission7.removeAttribute('disabled');
                permission8.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
        }

        if(e.target.value == '4' && e.target.checked)
        {
            enableIfPermission4IsChecked();
        }

        if(e.target.value == '4' && !e.target.checked)
        {
            permission12.checked = false;
            permission12.setAttribute('disabled','disabled');
            permission10.checked = false;
            permission10.setAttribute('disabled','disabled');

            permission7.checked = false;
            permission7.setAttribute('disabled','disabled');

            permission6.checked = false;
            permission6.setAttribute('disabled','disabled');

            permission2.checked = false;
            permission2.setAttribute('disabled','disabled');

            permission9.checked = false;
            permission9.setAttribute('disabled','disabled');

            permission14.checked = false;
            permission14.setAttribute('disabled','disabled');

            if(permission3.checked){
                permission7.removeAttribute('disabled');
                permission12.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
            if(permission5.checked ){
                permission7.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
        }

        if(e.target.value == '5' && e.target.checked){
            enableIfPermission5IsChecked();
        }

        if(e.target.value == '5' && !e.target.checked){
            permission7.checked = false;
            permission7.setAttribute('disabled','disabled');
            permission10.checked = false;
            permission10.setAttribute('disabled','disabled');
            permission8.checked = false;
            permission8.setAttribute('disabled','disabled');

            permission6.checked = false;
            permission6.setAttribute('disabled','disabled');

            permission2.checked = false;
            permission2.setAttribute('disabled','disabled');

            permission9.checked = false;
            permission9.setAttribute('disabled','disabled');

            permission14.checked = false;
            permission14.setAttribute('disabled','disabled');
            if(permission3.checked){
                permission7.removeAttribute('disabled');
                permission8.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
            if(permission4.checked){
                permission7.removeAttribute('disabled');
                permission6.removeAttribute('disabled');
                permission2.removeAttribute('disabled');
                permission9.removeAttribute('disabled');
                permission14.removeAttribute('disabled');
            }
        }



        if(permission6.checked && (permission3.checked || permission5.checked)){
           permission11.removeAttribute('disabled');


        }

        if(e.target.value == '7' && (permission3.checked || permission5.checked || permission4.checked)){
            permission10.removeAttribute('disabled');
        }

        if(e.target.value == '6' && (permission4.checked || permission5.checked)){
            permission10.removeAttribute('disabled');
        }
        if(permission7.checked && (permission3.checked || permission5.checked ||  permission4.checked)){
            permission10.removeAttribute('disabled');
        }

        if(e.target.value == '7' && !e.target.checked){
            permission10.checked = false;
            permission10.setAttribute('disabled','disabled');
        }

        if(e.target.value == '6' && !e.target.checked){
            permission10.checked = false;
            permission10.setAttribute('disabled','disabled');
        }


        if(!permission6.checked){
            permission11.checked = false;
            permission11.setAttribute('disabled','disabled');

        }
 };

</script>