<div class="services-and-followup">
    <div class="services-top-heading">
        <h2>Services</h2>
        @if(Entrust::can('manage-all')||Entrust::can('manage-all-services') || ( (Entrust::can('manage-own-services') && \Auth::user()->id == $case->user_id) || Entrust::can('view-all-cases') || Entrust::can('manage-all-cases') ) )
            <div class="add-buttons">
                <div class="add-list add-all">
                    @if($organizationObj->isFollowupEnable)
                        <div class="add-case-link">{!! link_to_route('services.create', "Add Follow
                            up",[$case->id,'FollowUp']) !!}
                        </div>
                    @endif
                    <div class="wrapper-dropdown-3" tabindex="1">
                        <span>Add Services</span>
                    </div>
                </div>
            </div>
        @endif
        <ul class="dropdown">

            @foreach($services as $service => $serviceProperty)
                @if($service != 'FollowUp')
                    <li>{!! link_to_route('services.create', $serviceProperty['label'],[$case->id, $service]) !!}</li>
                @endif
            @endforeach
        </ul>

        <div class="service-inner-wrap">

            @forelse($case->services as $service)

                <div class="follow-up">
                    <div class="left-followup-wrap">
                        <div class="date">{{ViewHelper::formatDate($service->date)}}</div>
                        <div class="<?php ($service->service == 'FollowUp' || $service->service == 'service_followup') ? (print 'followup-icon') : (print 'service-icon') ?>">
                            icon
                        </div>
                    </div>
                    <div class="right-followup-wrap">
                        <div class="inner-right-followup-wrap">
                            @if($service->service == 'service_followup')

                                @include("services.partials._service_followup")
                            @else
                                @include("services.partials._services")
                            @endif
                        </div>
                    </div>
                </div>
            @empty
                <div class="panel-body">
                    <div class="no-list">No Service and follow up</div>
                </div>
            @endforelse
        </div>
    </div>
</div>
