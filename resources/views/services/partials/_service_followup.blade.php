@if($service->followup)

    <div class="followup-heading">
        <div class="title">
            Follow up <a
                    id="{{$service->followup->service->id}}link">( {{$services[$service->followup->service->service]['label']}}
                )</a>
        </div>
        <div class="button-block">
            @if( Entrust::can('manage-all') || Entrust::can('manage-all-services') || (Entrust::can('manage-own-services') && \Auth::user()->id == $service->followup->service->user_id ))
                {!! link_to_route('services.followup.edit',
                "Edit",[$service->followup->service->id,$service->followup->id],['class'=>'edit-btn']) !!}
                @if(\Auth::user()->can(['manage-all', 'delete-all-services']) || (\Auth::user()->can('delete-own-services') && \Auth::user()->id == $service->followup->service->user_id ))
                    {!! link_to_route('services.delete',
                    "Delete",$service->id,['class'=>'delete-btn','data-method'=>'delete']) !!}
                    @if(Entrust::can('delete-all-services') && Entrust::can('delete-own-services'))
                        {!! link_to_route('services.delete',
                        "Delete",$service->id,['class'=>'delete-btn','data-method'=>'delete']) !!}
                    @endif
                @endif
             @endif
        </div>
    </div>
    <p>{!!nl2br($service->followup->description)!!}</p>
    @if(count($service->followup->files))
        <ul class="follow-list">
            <li>
                <label for="">Files</label>
                @foreach($service->followup->files as $serviceFile)
                    <div class="followup-description">
                        <a href="{{asset($serviceFile->file_path)}}" target="_blank">{{$serviceFile->file_name}}</a>
                    </div>
                @endforeach
            </li>
        </ul>
    @endif
@endif

