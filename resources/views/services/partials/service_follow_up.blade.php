@if(count($service->followups) > 0)
<ul class="follow-list">
    <li>
        <label for="">Follow up</label>
        @foreach($service->followups as $followup)
            <div class="followup-description">
                <div class="date">{{ViewHelper::formatDate($followup->date)}}</div>
                {!!nl2br($followup->description)!!}

                @if(count($followup->files))
                    <ul class="follow-list">
                        <li>
                            <label for="">Files</label>
                            @foreach($followup->files as $serviceFile)
                                <div class="followup-description followup-files">
                                    <a href="{{asset($serviceFile->file_path)}}" target="_blank">{{$serviceFile->file_name}}</a>
                                </div>
                            @endforeach
                        </li>
                    </ul>
                @endif
                
            </div>
        @endforeach
    </li>
</ul>
@endif