<div class="followup-heading">
    <div class="title" id="{{$service->id}}">{{$services[$service->service]['label']}}</div>
    <div class="button-block">
        @if($service->service != 'FollowUp' && !isset($services[$service->service]['followup']) && ! \Auth::user()->can('view-all-services'))
        {!! link_to_route('services.followup.create', "Follow up",$service->id,['class'=>'followup-btn']) !!}
        @endif

        @if(\Auth::user()->can('manage-all') || \Auth::user()->can('manage-all-services') || (\Auth::user()->can('manage-own-services') && \Auth::user()->id == $service->user_id ) )
            {!! link_to_route('services.edit', "Edit",$service->id,['class'=>'edit-btn']) !!}
        @if(\Auth::user()->can(['manage-all', 'delete-all-services']) || (\Auth::user()->can('delete-own-services') && \Auth::user()->id == $service->user_id ))
        {!! link_to_route('services.delete', "Delete",$service->id,['class'=>'delete-btn','data-method'=>'delete']) !!}
        @endif
        @endif
    </div>
</div>


@if(isset($services[$service->service]['followUpTemplate']) && !$services[$service->service]['followUpTemplate'])
<?php  $viewFileExist = View::exists("Fightvaw::Services.partials._".$service->service); ?>
@if($viewFileExist)
@include("Fightvaw::Services.partials._".$service->service)
@else
@include("services.partials._serviceDetails")
@endif
@else
@include("services.partials._followUp")
@endif

<!-- Service Followup -->
@if($service->service != 'FollowUp')
@include("services.partials.service_follow_up")
@endif

@if(count($service->files))
<!-- Service Files -->
<ul class="follow-list file-lists">
    <li>
        <label>Files</label>
        @foreach($service->files as $file)
        <div class="followup-description">
            <a href="{{ url('download',[md5($file->file_name)."_".$file->file_name])}}" target="_blank"> @if(strlen($file->file_name) >= 20) {{substr($file->file_name,0,20)."... ".substr($file->file_name,-5)}} @else {{ $file->file_name }} @endif</a>
        </div>
        @endforeach
    </li>
</ul>
@endif
