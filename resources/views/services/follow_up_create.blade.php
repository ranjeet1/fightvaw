@extends('layouts.app')

@section('content')

<?php
    $title = ($followup->id)?('Edit '):('Add ');
    $title .= "Follow up for ".$followup->service->title." of Case #".$followup->service->cases->reg_no;
?>
<div class="container">
    <div class="row create-row">
        <div class="wrapper">
            <h1>{{$title}}</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
