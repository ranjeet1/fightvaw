@extends('layouts.app')

@section('content')
<?php
    $title = ($service->id)?('Edit '):('Add ');
    $title .= $service->title." for Case #".$service->cases->reg_no;
?>

    <div class="container">
        <div class="row create-row">
            <div class="wrapper">
                <h1>{{$title}}</h1>
                <span class="form-info">Fields with <i> * </i>are required.</span>
                @include('partials.errors.basic')      
                <div class="panel panel-default">
                    <div class="panel-body service-form">
                        {!! form($form) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="uploaded-files">

    </div>
@stop
@section('script')
    @include("scripts.file_upload")
@stop
