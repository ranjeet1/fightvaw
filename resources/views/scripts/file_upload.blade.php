<script type="text/javascript" src="{{url('/js/jquery.uploadify.min.js')}}"></script>
<script>
    $(function() {
        $('#file_upload').uploadifive({
            'fileSizeLimit' : 8000,
            'fileType'     : [
                'image/jpeg',
                'image/png',
                'application/pdf',
                'text/csv',
                'text/plain',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel',
                'application/vnd.ms-powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'application/msword',
                'application/csv',
                'application/excel'
            ],
            'uploadScript' : '{{url('file/uploads')}}',
            'onUploadFile' : function(file){

                if(file.name.length >= 20 )
                    $(".uploadifive-queue div:last-child").find(".filename").text(file.name.substr(0,20)+'...'+file.name.substr(-5));

            },
            'formData'     : {
                '_token'     : '{{csrf_token()}}'
            },
            'onInit': addFiles
        });

    });

    function updateForm(file,task){
        fileForm = $('.file-upload-form');
        (task == "add") ?
            fileForm.append("<input type='hidden'  name='files[]' value="+file.name.replace(/\s+/g, '')+" data-name="+formatFileName(file.name)+" />"):
            removeFromServer(file.name);

    };
    function removeFromServer(filename){


        $("[data-name="+formatFileName(filename)+"]").detach();
        $.ajax({
            url: '{{url('file/remove')}}',
            data: {
                'name': filename,
                '_token': '{{csrf_token()}}'
            },
            error: function() {
                console.log("remove failure");
            },
            success: function(data) {

            },
            type: 'POST'
        });
    };
    var addFiles = function(){
        var serviceFiles = [];
        fileForm = $('.file-upload-form');
        @if(count($service->files))
            @foreach($service->files as $file)
                fileName = '{{$file->file_name}}';
                var fileHtml = '<div class="uploadifive-queue-item">\
                    <a class="close remove-file" data-name="'+formatFileName(fileName)+'" >X</a>\
                    <div>\
                        <span class="filename">'+' @if(strlen($file->file_name) >= 20) {{substr($file->file_name,0,20)."... ".substr($file->file_name,-5)}} @else {{ $file->file_name }} @endif'+'</span>\
                    <span class="fileinfo"></span></div>\
                    </div>';

                $("#uploadifive-file_upload-queue").before(fileHtml);
                fileForm.append("<input type='hidden'  name='uploaded_files[]' value="+fileName+" data-file="+formatFileName(fileName)+" />");

        @endforeach
    @endif
    }

    var formatFileName = function(fileName){
        return fileName.replace(".", "-").replace(/[()]/g, "").replace(/\,/g,"").replace(/\s+/g, '');
    }
    $(document).ready(function() {
        $('.remove-file').on("click",function(){
            $("[data-file="+$(this).data('name')+"]").detach();
            $(this).parent().detach();

        });

    });
</script>