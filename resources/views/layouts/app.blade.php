<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Application Title -->
        <title>FightVAW</title>

        <!-- Bootstrap CSS -->
        <link href="{{url('/css/app.css')}}" rel="stylesheet">

        <link href="{{url('/css/main.css')}}" rel="stylesheet">
        <!--Select2 Css-->
        <link href="{{url('/js/select2/select2.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/jquery.datetimepicker.css')}}" />

        <link rel="stylesheet" href="{{url('/css/uploadify.css')}}" />
    </head>
    <body>
        <div class="main-wrapper">
            <!-- Static navbar -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="inner-nav">
                    <div class="navbar-header">
                        <?php $orgName = env('ORGANIZATION_NAME'); ?>
                        <div class="{{$orgName}}-logo logo"><img src='{{asset("/images/".$orgName."_logo.png")}}' alt=""></div>
                        <a class="navbar-brand" href="{{url('/')}}">Case Management System</a>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse navbar-right">
                        @if (Auth::check())
                        <ul>
                            <li class="all-cases"><a href="{{url('/cases')}}">Dashboard</a></li>
                            @if(Entrust::can('manage-all') || Entrust::can('manage-own-cases') || Entrust::can('manage-all-cases'))
                            <li class="all-cases"><a href="{{ url('/cases/create') }}">Add New Case</a></li>
                            @endif
                            @if(Entrust::can(['manage-all', 'manage-users', 'manage-roles','manage-activity-log']))
                            <li class="administrator"><a href="#">Manage</a>
                                <ul>
                                    @if(Entrust::can(['manage-all', 'manage-users']))
                                    <li><a href="{{url('/users')}}">Users</a></li>
                                    @endif
                                    @if(Entrust::can(['manage-all','manage-roles']))
                                    <li><a href="{{url('/role')}}">Roles</a></li>
                                    @endif
                                    @if(Entrust::can(['manage-all','manage-activity-log']))
                                    <li><a href="{{url('/log')}}">Activity Logs</a></li>
                                    @endif
                                </ul>
                            </li>
                            @endif
                            <li class="administrator"><a href="#">{{Auth::user()->name}}</a>
                                <ul>
                                    <li><a href="{{url('/auth/logout')}}">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        @endif
                    </div>
                </div>
            </nav>

            <div class="main-container">
                <div class="container wrapper">
                    @include('flash::message')
                </div>
                @yield('content')
            </div>
            <div class="footer">
                <div class="inner-footer">
                    <div class="fightvaw-logo"><img src="{{asset('/images/fightvaw-logo.png')}}" alt=""></div>
                </div>
            </div>
            <!-- Bootstrap JavaScript -->
            <script src="{{url('/js/jquery.js')}}"></script>

            <!--Select-->
            <script src="{{url('/js/select2/select2.js')}}"></script>
            <script type="text/javascript" src="{{url('/js/jquery.datetimepicker.js')}}"></script>

            <!-- Readmore jquery -->
            <script src="{{url('/js/readmore.js')}}"></script>

            <!--Main JavaScript-->
            <script src="{{url('/js/main.js')}}"></script>

            @yield('script')
        </div>
    </body>
</html>
