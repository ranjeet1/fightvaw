@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="wrapper login-wrap">
            @if (Session::has('status'))
            <div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('status') }}
            </div>
            @endif
            <div class="panel-heading">Password Reset<p>We will email you instructions on how to reset your password.</p></div>
            <div class="panel panel-default login-wrapper">
    
                
                <div class="panel-body">  

                    <form class="form-horizontal" role="form" method="POST" action="{{url('/password/email')}}" id="forgot-password-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group input-wrapper">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" id="email" name="email" class="form-control <?php if ($errors->first('email')) print "error-box"; ?>" autocapitalize="off" value="{{ old('email') }}">
                                <div class="error">{{ $errors->first('email') }}</div>
                            </div>
                        </div>
                        <div class="form-group input-wrapper button-wrapper">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@stop
