@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
	<div class="wrapper login-wrap">
		<div class="panel-heading">Login</div>
		<div class="panel panel-default login-wrapper">
			
			<div class="panel-body">

<!--				@include('partials.errors.basic')-->

				<form class="form-horizontal" role="form" method="POST" action="{{url('/auth/login')}}" id="login-form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group input-wrapper">
						<label for="email" class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6">
							<input type="email" id="email" name="email" class="form-control <?php if($errors->first('email')) print "error-box"; ?>" autocapitalize="off" value="{{ old('email') }}">
							<div class="error">{{ $errors->first('email') }}</div>
						</div>
					</div>
					<div class="form-group input-wrapper password-wrapper">
						<label for="password" class="col-sm-3 control-label">Password</label>
						<div class="col-sm-6">
							<input class=" form-control <?php if($errors->first('password')) print "error-box"; ?>" type="password" name="password">
							<div class="error">{{ $errors->first('password') }}</div>
						</div>
					</div>
					<div class="form-group button-wrapper">
							<button type="submit" class="btn btn-primary">Login</button>	
					</div>
					<div class="form-group input-wrapper forgot-pwd-wrapper">
						<div class="col-sm-offset-3 col-sm-6">
							<!--
<label>
<input type="checkbox" name="remember"> Remember Me
</label>
-->
							<div id="forgot-password-link" class="text-right"><a href="{{url('/password/email')}}" class="forgot-password">Forgot Password?</a></div>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
</div>
@stop

