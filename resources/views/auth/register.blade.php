@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="wrapper register-wrap">
            <div class="panel-heading">Add User</div>
            <div class="panel panel-default">
                <div class="panel-body">

                    {!! Form::open(array('url' => '/auth/register')) !!}

                    {!! Form::text('name', @$name) !!}

                    {!! Form::label('name') !!}
                    {!! Form::text('name', @$name) !!}

                    {!! Form::label('email') !!}
                    {!! Form::text('email') !!}

                    {!! Form::label('phone_number') !!}
                    {!! Form::text('phone_number') !!}

                    {!! Form::label('address') !!}
                    {!! Form::textarea('address') !!}

                    {!! Form::label('password') !!}
                    {!! Form::password('password') !!}

                    {!! Form::label('role') !!}
                    {!! Form::select('role',App\Models\Users\Role::lists('name','id')) !!}

                    {!! Form::button('Create',['type'=>'submit']) !!}

                    {!! Form::button('Cancel',['onclick'=>"redirectFunction()"]) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
