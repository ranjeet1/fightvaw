@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="wrapper">
            <div class="panel panel-default login-wrapper">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{url('/password/reset')}}" id="login-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group input-wrapper">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" id="email" name="email" class="form-control <?php if ($errors->first('email')) print "error-box"; ?>" autocapitalize="off" value="{{ old('email') }}">
                                <div class="error">{{ $errors->first('email') }}</div>
                            </div>
                        </div>
                        <div class="form-group input-wrapper password-wrapper">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-6">
                                <input class=" form-control <?php if ($errors->first('password')) print "error-box"; ?>" type="password" name="password">
                                <div class="error">{{ $errors->first('password') }}</div>
                            </div>
                        </div>
                        <div class="form-group input-wrapper password-wrapper">
                            <label for="password" class="col-sm-3 control-label">Confirm Password</label>
                            <div class="col-sm-6">
                                <input type="password" name="password_confirmation" class="form-control <?php if ($errors->first('password_confirmation')) print "error-box"; ?>">
                                <div class="error">{{ $errors->first('password_confirmation') }}</div>
                            </div>
                        </div>
                        <div class="form-group button-wrapper">
                            <button type="submit" class="btn btn-primary">Reset</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@stop

