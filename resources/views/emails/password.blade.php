Dear {{$user->name}},<br><br>
Click the link below or copy/ paste it in your browser to reset your password: <br><br>{{ url('password/reset/'.$token) }}<br><br>
Regards,<br>
FightVAW