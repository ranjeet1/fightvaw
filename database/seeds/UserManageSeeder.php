<?php

use App\Models\Users\Role;
use App\Models\Users\Users;
use Illuminate\Database\Seeder;
use App\Models\Users\Permission;
use Illuminate\Support\Facades\DB;

class UserManageSeeder extends Seeder {

    public function run()
    {
        /**
         * Create Role
         */
        $role = Role::create([
            'name'  => 'Superadmin',
            'display_name' => 'Superadmin',
            'description' => 'Manage All',
            ]);
        $manageAllPermsId = Permission::where('name','manage-all')->get()->first()->id;
        $role->perms()->sync([$manageAllPermsId]);

        /**
         * Create User
         */
        $user = Users::create([
            'name'  => 'Yipl',
            'email' => 'yipl@fightvaw.org',
            'phone_number' => '1234567898',
            'address' => 'Kumaripati, Lalitpur',
            'password' => bcrypt('#yipl@fightvaw#'),
            'is_active' => '1',
            ]);
        $lastRoleId = Role::all()->last()->id;
        $user->roles()->sync([$lastRoleId]);

    }

}