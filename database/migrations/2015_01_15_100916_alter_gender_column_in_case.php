<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenderColumnInCase extends Migration {

	/**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropColumn('gender');
        });
        Schema::table('cases', function(Blueprint $table)
        {
            $table->enum('gender', ['Male', 'Female', 'Other'])->nullable();
        });       
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropColumn('gender');
        });
        Schema::table('cases', function(Blueprint $table)
        {
            $table->enum('gender', ['Male', 'Female', 'Third gender'])->nullable();

        });
    }

}
