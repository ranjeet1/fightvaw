<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVarcharLengthOfStatusToCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{       
            DB::statement('ALTER TABLE cases ALTER COLUMN status TYPE character(100)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            DB::statement('ALTER TABLE cases ALTER COLUMN status TYPE character(50)');
	}

}
