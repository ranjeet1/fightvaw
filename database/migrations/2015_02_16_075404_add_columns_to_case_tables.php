<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCaseTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            DB::statement('ALTER TABLE cases ADD details json;');
            DB::statement('ALTER TABLE cases ADD address json;');
            DB::statement('ALTER TABLE cases ADD family_info json;');
            DB::statement('ALTER TABLE cases ADD incident json;');
            DB::statement('ALTER TABLE cases ADD perpetrator json;');
            DB::statement('ALTER TABLE cases ADD contact json;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            DB::statement('ALTER TABLE cases Drop details json;');
            DB::statement('ALTER TABLE cases Drop address json;');
            DB::statement('ALTER TABLE cases Drop family_info json;');
            DB::statement('ALTER TABLE cases Drop incident json;');
            DB::statement('ALTER TABLE cases Drop perpetrator json;');
            DB::statement('ALTER TABLE cases Drop contact json;');
	}

}
