<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_follow_up', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('service_id')->unsigned();
			// $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('user_id')->unsigned();
			// $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_follow_up');
	}

}
