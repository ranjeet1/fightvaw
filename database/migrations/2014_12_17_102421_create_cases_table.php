<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			// $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->string('code', 100);
			$table->string('name', 100);
			$table->integer('age')->nullable();
			$table->enum('gender',['Male','Female','Third gender'])->nullable();
			$table->string('ethnicity',100)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('phone_num',50)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cases');
	}

}
