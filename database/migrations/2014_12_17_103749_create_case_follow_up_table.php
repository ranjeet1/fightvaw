<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_follow_up', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('case_id')->unsigned();
			// $table->foreign('case_id')->references('id')->on('cases')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('user_id')->unsigned();
			// $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_follow_up');
	}

}
