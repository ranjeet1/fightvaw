<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Store data in permissions table
        $data = [
            ['name'=>'manage-all','display_name'=>'Manage All','description'=>'Has all previliges to the system'],
            ['name'=>'manage-users','display_name'=>'Manage Users','description'=>'Manage Users'],
            ['name'=>'manage-all-cases','display_name'=>'Manage All Cases','description'=>'Manage All Cases'],
            ['name'=>'manage-own-cases','display_name'=>'Manage Own Cases','description'=>'Manage Own Cases'],
            ['name'=>'view-all-cases','display_name'=>'View All cases','description'=>'View All cases'],
            ['name'=>'manage-all-services','display_name'=>'Manage All Services','description'=>'Manage All Services'],
            ['name'=>'manage-own-services','display_name'=>'Manage Own Services','description'=>'Manage Own Services'],
            ['name'=>'view-all-services','display_name'=>'View All Services','description'=>'View All Services'],
            ['name'=>'manage-roles','display_name'=>'Manage Roles','description'=>'Manage Roles'],            
            ['name'=>'delete-own-services','display_name'=>'Delete Own Services','description'=>'Delete Own Services'],
            ['name'=>'delete-all-services','display_name'=>'Delete All Services','description'=>'Delete All Services'],
            ['name'=>'delete-own-cases','display_name'=>'Delete Own Cases','description'=>'Delete Own Cases'],
            ['name'=>'delete-all-cases','display_name'=>'Delete All Cases','description'=>'Delete All Cases'],
            ['name'=>'manage-activity-log','display_name'=>'Manage Activity Logs','description'=>'Manage Activity Log']
        ];
        foreach($data as $value)
        {
            App\Models\Users\Permission::create($value);
        }

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permission_role');
        Schema::drop('permissions');
        Schema::drop('role_user');
        Schema::drop('roles');
    }
}
